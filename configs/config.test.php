<?php

/**
 * Configuration for your test environment.
 * Only define settings that have to be different from the production environment!
 */

config::set('CONF_MYSQL_DATABASE', 'test_pricebooster');
config::set('SERVICE_URL', 'http://test.eggerd.de/pricebooster');
config::set('RUNTIME_EXEC_TIME', 60000);
config::set('RUNTIME_TIMEOUT', 120);
config::set('PRICE_RISE_TIMEOUT', 180);
config::set('KIOSK_REMOTE_REGISTRATION_KEY', '');

?>
<?php

// ***** ***** MYSQL ***** *****

// Host unter dem die Datenbank zu erreichen ist
config::set('CONF_MYSQL_HOSTNAME', 'localhost');

// Benutzername für die Anmeldung an der Datenbank
config::set(config::secret('CONF_MYSQL_USERNAME'));

// Passwort für die Anmeldung an der Datenbank
config::set(config::secret('CONF_MYSQL_PASSWORD'));

// Name der Datenbank die verwendet werden soll
config::set('CONF_MYSQL_DATABASE', 'pricebooster');



// ***** ***** Allgemein ***** *****

// Titel des Dienstes, der z.B. in der Kopfzeile, in Tabs, in E-Mails usw. verwendet wird
config::set('SERVICE_NAME', 'PriceBooster'); // :string

// Die aktuelle Versionsnummer des PriceBoosters, die unter anderem in der Kopfzeile des Clients angezeigt wird
config::set('SERVICE_VERSION', '2.0.5'); // :string @ JS => [Hauptversionsnummer].[Nebenversionsnummer].[Revisionsnummer]

// Hier wird die dreistellige Versionsnummer um die Buildnummer erweitert. Die hier definierte Versionsnummer wird
// intern zur überprüfung von Aktualisierungen verwendet und identifiziert jede einzelne Version.
config::set('SERVICE_BUILD', config::get('SERVICE_VERSION')['value'].'.6'); // :string

// Unter dem hier genannten URL ist der Dienst erreichbar. Wird unter Anderem in versendeten E-Mails benötigt um
// Grafiken einbinden zu können, da diese eine vollständige PFadangabe benötigen
config::set('SERVICE_URL', 'https://pricebooster.eggerd.de'); // :string

// Die hier definierte E-Mail Adresse wird bei allen versendeten E-Mail Nachrichten als Absender angegeben
config::set('MAIL_SUBMITTER', 'pricebooster@dustin-eckhardt.de'); // :string

// Der hier definierte Key wird für die Registration bei Kiosk Browser Remote verwendet
config::set(config::secret('KIOSK_REMOTE_REGISTRATION_KEY'));



// ***** ***** Runtime ***** *****

// Nach Ablauf der hier definierten Zeit, in Millisekunden, wird die Runtime des Clients erneut ausgeführt, wodurch
// z.B. die Preisabfrage durchgeführt, Broadcasts geladen, Logs gesendet (usw.) werden. Wird beim Client auch für
// den Countdown bis zur nächsten Preisabfrage (wenn die Preise angeglichen sind) verwendet.
config::set('RUNTIME_EXEC_TIME', 300000); // :integer @ JS => [Millisekunden]

// Für die hier definierte Zeit, in Sekunden, wird die Runtime des Clients keine Preisabfragen durchführen, da
// zuvor neue Preise gemeldet wurden. Wird vom Client aber auch für den Countdown, unter den gemeldeten Preisen,
// verwendet.
config::set('RUNTIME_TIMEOUT', 1800); // :integer @ JS => [Sekunden]

// Sollten die für eine Referenz ermittelten Preise älter als die hier angegebene Zeit, in Sekunden, sein, so
// werden diese als veraltet betrachtent und nicht für die Preisabfrage verwendet. In diesem Fall wird ein Fehler
// ausgelöst. Für die Ermittlung des Alters werden die Zeitangaben der Quelle verwendet.
config::set('REFERENCE_DATA_TIMEOUT', 1800); // :integer => [Sekunden]

// Die für eine Referenz ermittelten Preise werden für diese zusätzlich in der Datenbank gespeichert. Sollten die
// dort gespeicherten Preise nicht älter als die hier angegebene Zeit sein, werden diese für die Preisabfrage
// verwendet und somit zunächst nicht neu von der Quelle geladen. Erst nach Ablauf dieser Zeit, werden die Preise
// wieder neu von der Quelle ermittelt. Dies dient der Reduzierung von Anfragen an die Quelle, solange die bekannten
// Preise noch als aktuell betrachtet werden können. Kommt hauptsächlich zum Einsatz, falls mehrere Stationen die
// gleiche Referenz verwenden oder ein Client neu geladen wird.
config::set('DATABASE_REFERENCE_PRICE_DURABILITY', 280); // :integer => [Sekunden]

// Auch die Preise der eigenen Stationen werden in der Datenbank gespeichert, da diese für das Erkennen von
// Preiserhöhungen benötigt werden. Die gespeicherten Preise werden jedoch nur dann verwendet wenn diese nicht
// älter als die hier angegebene Zeit sind.
config::set('DATABASE_STATION_PRICE_DURABILITY', 900); // :integer => [Sekunden]

// Sollte bei einer Station eine Preiserhöhung festgestellt werden, können die Preisabfragen für eine besimmte
// Zeit pausiert werden. Die Unterbrechung der Preisabfragen, bei Erkennung einer Preiserhöhung, kann sich somit
// von der üblichen Pausierungsdauer, wenn neue Preise gemeldet werden, unterscheiden.
config::set('PRICE_RISE_TIMEOUT', 3600); // :integer => [Sekunden]

// Gibt an wie viele Referenzen maximal für eine Station überprüft und gemeldet werden dürfen. Sollte eine Station
// mehr als die hier definierte Anzahl von Referenzen zugewisen bekommen, werden jeweils nur die zuerst geladenen
// Referenzen verwendet und alle nachfolgenden ignoriert!
config::set('MAX_REFERENCE_AMOUNT', 10); // :integer => [Anzahl]

// Aktiviert oder deaktiviert die Verwendung des Benachrichtigungstons, der abgespielt wird sobal dem Client einer
// Station neue Preise gemeldet werden; dient hauptsächlich zu Debug-Zwecken
config::set('PLAY_NOTIFICATION_SOUNDS', true); // :boolean @ JS

// Da Personal normal schon vor den Öffnungszeiten im Laden anwesend sind, kann auch der Client seine Arbeit
// vor den Öffnungszeiten wieder aufnehmen. Hier kann angegeben werden, wie viel früher der Client wieder
// Preisabfragen durchführen soll; die Öffnungszeiten werden dann um die angegebene Zeit vorverlegt.
config::set('OPENING_HOUR_OVERWRITE', 1800); // :integer => [Sekunden]

// Preisabfragen sind kurz vor Ladenschluss weniger Sinnvoll, da diese entweder ohnehin vom Pricing ignoriert werden
// oder die Zeit zum Melden der Preise einfach nicht mehr ausreichen würde. Daher kann hier der Ladenschluss für
// den Dienst um eine gewisse Zeit vorverlegt werden, um somit die Preisabfragen frühzeitig zu pausieren.
config::set('CLOSING_HOUR_OVERWRITE', 2700); // :integer => [Sekunden]

// Bei der Überprüfung für Versäumnismeldungen werden die zuletzt gesendeten Preise, der Counter und einige
// Zeitangaben für die jeweilige Station gespeichert. Diese Daten werden nach Ablauf der hier angegebenen Zeit
// als veraltet betrachtet und somit nicht mehr verwendet, sondern durch neue ersrtzt. Die hier verwendete Zeit
// muss mindestens so groß wie die von RUNTIME_TIMEOUT sein, bzw. sollte diese leicht überschreiten um einen Puffer
// für die Abfragen zu schaffen.
config::set('DERELICTION_DATA_DURABILITY', config::get('RUNTIME_TIMEOUT')['value'] + 300); // :integer => [Sekunden]



// ***** ***** Design & Effekte ***** *****

// Nach Ablauf der hier angegebenen Zeit wird das Menü wieder ausgeblendet, sollte der Benutzer keine Option aus
// dem Menü auswählen
config::set('HIDE_MENU_TIME', 10000); // :integer @ JS => [Millisekunden]



// ***** ***** Debug & Logs ***** *****

// Gibt an ob das Debug-Log beim Laden des Clients ein- (ture) oder ausgeblendet (false) werden soll
config::set('DEBUG_LOG_VISIBLE', false); // :boolean

// Aus Gründen der Performance muss das Debug-Log des Clients regelmäßig geleert werden. Dabei werden nur die
// Einträge gelöscht, die bereits an den Server gesendet und gespeichert wurden. Hier wird also definiert wie
// viele Einträge der Client aufbewahren soll, die bereits gesendet wurden, bevor diese gelöscht werden.
config::set('DEBUG_MAX_ENTRIES', 500); // :integer @ JS => [Anzahl]

// über die hier definierte Anzahl von Klicks, auf das Icon des Dienstes, kann das Debug-Log des Clients
// eingeblendet werden.
config::set('DEBUG_CLICKS_TO_SHOW', 4); // :integer @ JS => [Anzahl]

// In dem hier definiertem Verzeichnis werden die Log-Dateien getrennt nach Stationen abgespeichert.
// Nicht vorhandene (Unter-) Ordner werden angelegt, sollten diese nicht vorhanden sein.
config::set('DEBUG_DIRECTORY', '_logs'); // :string

// Definiert die maximale Gesamtgröße der gespeicherten Log-Dateien, je Station, in MB.
// Wird die hier angegebene Größe erreicht, werden alte Log-Dateien gelöscht, bis die maximale Größe wieder
// unterschritten wurde oder DEBUG_PRESERVE_LOGS erreicht wurde.
config::set('DEBUG_MAX_LOG_SIZE', 10); // :integer => [Megabyte]

// Wird die unter DEBUG_MAX_LOG_SIZE definierte Gesamtgröße der Log-Dateien einer Station erreicht, wird damit
// begonnen alte Logs zu löschen, bis die definierte Gesamtgröße wieder unterschritten ist. Das Löschen wird
// jedoch auch dann beendet, wenn nur noch die hier definierte Anzahl an Logs übrig ist, auch wenn dabei die
// maximale Größe überschritten wird!
config::set('DEBUG_PRESERVE_LOGS', 3); // :integer => [Anzahl]

?>
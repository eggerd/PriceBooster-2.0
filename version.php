<?php

	// Über diese Datei kann einem Client mitgeteilt werden, dass ein Update ausgerollt
	// bzw. vorbereitet wird. Während eines Updates können somit sämtliche Dateien des
	// Servers gelöscht werden, ohne auf dem Client für Fehlermeldungen zu sorgen.
	// Der Client wird diese Datei überprüfen, sollte er nicht in der Lage sein, die
	// aktuelle Versionsnummer (über /javascript/functions/properties.js) zu ermitteln.
	//
	// Wird ein Update durchgeführt, muss lediglich 'success' auf true gesetzt werden
	// und in 'data' die neue Versionsnummer angegeben werden!

	// sollte diese Zeile geändert oder verschoben werden, müssen auch die regulären Ausdrücke
	// in der CI von GitLab angepasst werden, da diese Zeile während Deploys mehrfach geändert wird!
	$update = array('success' => false, 'errors' => null, 'data' => array('update' => '#.#.#.#'));


	if($update['success'])
	{
		$return = $update;
	}
	else
	{
		require_once(realpath(dirname(__FILE__)).'/configs/configure.php');

		$return = array('success' => true, 'errors' => null, 'data' => array('version' => SERVICE_BUILD));
	}

	echo json_encode($return);

?>
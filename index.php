<!DOCTYPE html>
<html>
<?php

    session_start();

    require_once('./configs/configure.php');
    require_once('./includes/config-js.php');
    require_once('./includes/functions/mysql.php');
    require_once('./includes/functions/login.php');
    require_once('./includes/functions/properties.php');

    $authenticated = authenticated(); // überprüfen ob dier Client angemeldet ist

    require_once('./includes/header.php');
?>

<body>
    <div id="header">
        <div id="caption">
            <img src="./images/logo.png" />
            <h1><?php echo SERVICE_NAME; // Name des Diensts ausgeben ?></h1>
            <h4>v<?php echo SERVICE_VERSION; // dreistellige Versionsnummer ausgeben ?></h4>
        </div>
        <div id="navi" <?php if($authenticated['data'] !== true){echo 'style="display: none"';} // wird nicht angezeigt, wenn keine Station angemeldet ?>>
            <img src="./images/menu.png" />
            <h3>
                <?php

                    if($authenticated['data'] === true) // wenn die Station angemeldet ist
                    {
                        $station_info = fetch_station_info($_COOKIE['station_id']); // grundlegende Daten der Station laden (z.B. den Namen)

                        if($station_info['success']) // wenn die Daten geladen werden konnten
                        {
                            if(isset($_COOKIE['support']) && $_COOKIE['support']) // falls es sich um einen Support-Login handelt
                            {
                            	echo '<span id="support">SUPPORT</span> <span id="spacer">/</span> '.$station_info['data']['name']; // speziellen Stationsnamen ausgeben
                            }
                            else // normaler Login einer Station
                            {
                            	echo $station_info['data']['name']; // Stationsname ausgeben
                        	}
                        }
                        else // Daten konnten nicht geladen werden
                        {
                            echo '
                            <script language="javascript" type="text/javascript">
                                $(function()
                                {';

                                    foreach($station_info['errors'] as $code => $message)
                                    {
                                        echo 'create_notification("'.$code.'", "'.$message.'")';
                                    }

                            echo '
                                });
                            </script>';
                        }
                    }

                ?>
            </h3>
        </div>
    </div>
    <div id="menu">
        <div id="menu-triangle"></div>
        <img src="./images/menu/settings.png">
        <img src="./images/menu/contact.png">
        <img src="./images/menu/changelog.png">
        <img src="./images/menu/logout.png">
    </div>

    <noscript>
		<div class="notification error">
            <img src="./images/error.png" />
            <div>
                <h5>Meldung 4e012</h5>
                <p>Der Dienst kann nicht verwendet werden, da die Ausführung von JavaScript deaktiviert ist!</p>
            </div>
        </div>
    </noscript>

    <!-- Content-Overlay für alle zusätzlichen Inhalte, die über das Menü aufgerufen werden können -->
    <div id="overlay-wrapper">
        <div id="overlay">
            <div id="headline">
                <h4 id="title"><!-- hier wird später der Titel des Contents eingefügt --></h4>
                <img id="close" src="./images/overlay/close.png">
                <div id="hint"><!-- hier werden ggf. Hinweise angezeigt --></div>
            </div>
            <div id="context"><!-- hier wird später der jeweilige Content eingefügt --></div>
        </div>
    </div>

    <!-- Haupt-Content des Clients -->
    <div id="content">
        <div id="notifications">
            <div id="broadcasts"><!-- hier werden globale / Stations-Broadcasts eingefügt --></div>
            <!-- hier werden Fehlermeldungen usw. eingefügt -->
        </div>

        <div id="login" <?php if($authenticated['data'] === true){echo 'style="display: none"';} ?>>
            <input type="text" placeholder="Stationsnummer" />
            <input type="password" placeholder="Passwort" />
            <input type="submit" value="Anmelden" />
        </div>

        <div id="references-wrap">
	        <div id="references">
	            <!-- hier werden die Preise der Referenz-Stationen eingefügt -->
	        </div>
        </div>
        <div id="reference-bottom">N&auml;chste &Uuml;berpr&uuml;fung der Wettbewerbspartner in etwa <span id="countdown-references">##:##</span> Minuten</div>
    </div>

    <div id="footer">Dustin Eckhardt &copy; <?php echo date('Y', time()) ?></div> <!-- seit 2013 -->





    <!--========================================================================================================
        nachfolgende Elemente beziehen sich nicht auf das Design, sondern erfüllen eine spezifische Funktion!
    =========================================================================================================-->

    <audio id="audio"><source src="./sounds/attention.mp3" type="audio/mpeg"></audio>

    <!-- die Status-Images werden hier unsichtbar eingebunden, da dies den Browser dazu
    zwingt die Images zu laden und im Cache abzulegen. Somit sind die Images auch im
    Fall von Verbindungsproblemen (Server nicht erreichbar / kein Internet) verfügbar. -->
    <div id="preload">
	    <img src="./images/note.png" />
	    <img src="./images/error.png" />
	    <img src="./images/warning.png" />
	    <img src="./images/success.png" />
	    <img src="./images/cloud_update.png" />
	    <img src="./images/cloud_error.png" />
	    <img src="./images/offline.png" />
    </div>

    <!-- enthält später alle Debug-Einträge die vom Client erzeugt werden -->
    <div id="debug" <?php echo DEBUG_LOG_VISIBLE ? null : 'style="display: none"'; // je nach Einstellung das Debug-Log beim Laden einblenden oder nicht ?>>
    	<div id="selection">
    		<!-- das Menü des Debug-Logs (Suffix der ID muss mit dem zugehörigen #logs Elements übereinstimmen) -->
    		<p id="sel_runtime" class="selected">Runtime</p>
    		<p id="sel_authentication">Authentication</p>
    		<p id="sel_broadcasts">Broadcasts</p>
    		<p id="sel_properties">Properties</p>
    		<p id="sel_database">Database</p>
    		<p id="sel_overlay">Overlay</p>
    	</div>
    	<div id="logs">
    		<!-- je nach Bereich werden hier die Debug-Einträge eingefügt -->
			<div id="log_runtime" class="selected"></div>
			<div id="log_authentication"></div>
			<div id="log_broadcasts"></div>
			<div id="log_properties"></div>
			<div id="log_database"></div>
			<div id="log_overlay"></div>
		</div>
    </div>
</body>
</html>
# PriceBooster 2.0 

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F7094222%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/PriceBooster-2.0/-/releases) 
[![pipeline status](https://gitlab.com/eggerd/PriceBooster-2.0/badges/master/pipeline.svg)](https://gitlab.com/eggerd/PriceBooster-2.0/-/pipelines) 
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=pricebooster_2.0&metric=alert_status)](https://sonarcloud.io/dashboard?id=pricebooster_2.0) 
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=pricebooster_2.0&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=pricebooster_2.0)

An Tankstellen ist es gang und gäbe die Spritpreise von Wettbewerbspartnern zu beobachten, um bei Preissenkungen schnellstmöglich nachziehen zu können. Der PriceBooster 2.0 ist eine webbasierte Anwendung, mit der diese Beobachtung automatisiert werden kann.

- Regelmäßige Überprüfung der Preise von mehreren Wettbewerbspartnern gleichzeitig
- Visuelle & akustische Benachrichtigung des Personals bei Preissenkungen
- Statische Preisabweichungen zu einzelnen Wettbewerbspartnern können berücksichtigt werden
- Pausiert die Beobachtung vorübergehend, wenn eine Preiserhöhung an der eigenen Station erkannt wird
- Pächter können sich bei versäumter Anpassung der Preise benachrichtigen lassen

Von Mai 2015 bis Dezember 2018 war der PriceBooster 2.0 erfolgreich an einer Tankstelle im Einsatz. Inwzischen wurden Preisanpassungen vom Mutterkonzern jedoch vollständig automatisiert, wodurch Tankstellen keine Preismeldungen mehr an die Zentrale senden müssen und der PriceBooster damit überflüssig wurde.

## Demo

Unter [archiv.dustin-eckhardt.de](https://archiv.dustin-eckhardt.de/pricebooster) ist die aktuellste Entwicklerversion testbar. Eine Anmeldung ist mit dem Benutzernamen und Passwort `demo` möglich. Über die [Kontrollseite](https://archiv.dustin-eckhardt.de/pricebooster/_simulate) können folgende Situationen simuliert werden: 
1. Preiserhöhung der eigenen Station
1. Preissenkung eines oder mehrerer Wettbewerbspartners
1. Preise sind an alle Wettbewerbspartner angeglichen
1. Die eigene Station hat geschlossen

## Voraussetzungen

Es wird PHP 5.5.0 oder höher, sowie eine MySQL-Datenbank benötigt. Optional sollte der verwendete Webserver `.htaccess`-Dateien unterstützen und `mod_rewrite` sowie `mod_headers` aktiviert sein.

Darüber hinaus wäre eine Datenbankverwaltung mit grafischer Oberfläche zu empfehlen (wie z. B. phpMyAdmin), um die Arbeit mit der Datenbank zu erleichtern, da der PriceBooster ausschließlich über direkten Zugriff auf diese administriert wird.

## Installation

**Hinweis: Die Installation und Konfiguration des PriceBooster ist sehr techniklastig, da dieser eigentlich nicht für die Verwendung durch Dritte vorgesehen war!**

Zunächst sollten die Konfigurationsdateien in `/configs` den eigenen Bedürfnissen angepasst werden und die Zugangsdaten für die MySQL-Datenbank eingetragen werden. Es wird allerdings empfohlen sensible Daten, wie Passwörter, in einer `secrets.php` abzulegen - mehr dazu kann im [Wiki](https://gitlab.com/eggerd/PriceBooster-2.0/-/wikis) des verwendeten Konfigurationsmanagers gefunden werden. Anschließend genügt es, alle Dateien des Repository auf den eigenen Webspace hochzuladen.

Abschließend muss noch die [Grundstruktur der Datenbank](https://gitlab.com/eggerd/PriceBooster-2.0/-/wikis/database_base_structure) in die eigene Datenbank importiert und entsprechend der eigenen Bedürfnisse die benötigten Datensätze angelegt werden (z. B. Stationen, Wettbewerbspartner usw.). Hierzu kann die ausführliche Dokumentation der Datenbank, die im [Wiki](https://gitlab.com/eggerd/PriceBooster-2.0/-/wikis) des Repository zu finden ist, zur Hilfe genommen werden.

Passwörter für Stationen und Pächter werden als `IHA1`-Hash in der Datenbank abgelegt. Diese Hashs können mithilfe eines Aufrufs von `hash("plain_passwort")` in `/includes/libraries/iha.class.php` generiert werden.

## Dokumentation

Eine ausführliche Dokumentation der Datenbank, die zur Verwaltung des PriceBooster verwendet wird, kann im [Wiki](https://gitlab.com/eggerd/PriceBooster-2.0/-/wikis) des Repository gefunden werden.

## Lizenzierung

Copyright (c) 2018 Dustin Eckhardt. Alle Rechte vorbehalten.
<script language="javascript" type="text/javascript">

	// Alle Werte mittels var_export() ausgeben!

    var $SERVICE_VERSION = <?php var_export(SERVICE_BUILD); ?>;
	var $RUNTIME_TIMEOUT = <?php var_export(RUNTIME_TIMEOUT); ?>;
	var $RUNTIME_EXEC_TIME = <?php var_export(RUNTIME_EXEC_TIME); ?>;
	var $PLAY_NOTIFICATION_SOUNDS = <?php var_export(PLAY_NOTIFICATION_SOUNDS); ?>;
	var $HIDE_MENU_TIME = <?php var_export(HIDE_MENU_TIME); ?>;
	var $DEBUG_MAX_ENTRIES = <?php var_export(DEBUG_MAX_ENTRIES); ?>;
	var $DEBUG_CLICKS_TO_SHOW = <?php var_export(DEBUG_CLICKS_TO_SHOW); ?>;
	var $KIOSK_REMOTE_REGISTRATION_KEY = <?php var_export(KIOSK_REMOTE_REGISTRATION_KEY); ?>;

</script>
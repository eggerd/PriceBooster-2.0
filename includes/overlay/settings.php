<head>
<meta charset="UTF-8" />
</head>
<?php
	require_once(realpath(dirname(__FILE__).'/../functions').'/mysql.php');
	require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');

	$db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

    if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
    {
?>
		<link href="./css/overlay-settings.css" type="text/css" rel="stylesheet" />


		<!-- Pächter-PIN Abfrage -->
		<div id="reqppin">
			<p>Bitte geben Sie den P&auml;chter-PIN ein, um fortzufahren</p>
			<div id="value" class="ppinform">
				<input type="password" maxlength="1">
				<input type="password" maxlength="1">
				<input type="password" maxlength="1">
				<input type="password" maxlength="1">
				<button class="imgbutton" name="submit" disabled><img src="./images/overlay/button-check.png"></button>
			</div>
		</div>


		<div id="option-wrapper" style="display: none">

			<!-- Settings Content -->
			<div id="option-content">
				<div id="ppin">
					<p class="desc">Der P&auml;chter-PIN f&uuml;r u.A. f&uuml;r das &Auml;ndern von Einstellungen oder zum Abmelden des Clients ben&ouml;tigt - und ist zudem f&uuml;r alle Ihre
					Stationen &uuml;bergreifend g&uuml;ltig! Er sollte daher nicht an Mitarbeiter weitergegeben werden, um Missbrauch zu vermeiden.</p>

					<div id="value" class="ppinform">
						<input type="password" maxlength="1">
						<input type="password" maxlength="1">
						<input type="password" maxlength="1">
						<input type="password" maxlength="1">
						<button class="imgbutton" name="submit" disabled><img src="./images/overlay/button-check.png"></button>
						<button class="imgbutton" name="cancel" style="display: none"><img src="./images/overlay/button-trash.png"></button>
					</div>
				</div>
				<div id="passwd">
					<p class="desc">Das Stationspasswort wird lediglich f&uuml;r die Anmeldung am PriceBooster ben&ouml;tigt und f&uuml;r jede Station getrennt definiert. Es muss
					aus mindestens vier Zeichen bestehen und sollte allen Mitarbeitern bekannt sein.</p>

					<div id="value">
						<input type="password" name="passwd" placeholder="Passwort" minlength="4">
						<button class="imgbutton disabled" name="cancel" style="display: none"><img src="./images/overlay/button-trash.png"></button>
						<button class="imgbutton disabled" name="submit" disabled><img src="./images/overlay/button-check.png"></button>
					</div>
				</div>
				<div id="contact">
					<p class="desc">Dies ist eine &Uuml;bersicht Ihrer Kontaktdaten. Falls Sie, abgesehen von der E-Mail, etwas &auml;ndern m&ouml;chten, bitte ich Sie mich zu kontaktieren.</p>

					<?php
						$contactmeta = $db->query('select t.title, t.first_name, t.last_name, t.mail from tenants as t, stations as s where s.id = "'.mysqli_real_escape_string($db, $_COOKIE['station_id']).'" and s.tenant_id = t.id limit 1');
						$contactmeta = $contactmeta->fetch_object();
					?>

					<table>
						<tr class="noedit">
							<td>Anrede</td>
							<td><input type="value" value="<?php echo $contactmeta->title; ?>" disabled></td>
						</tr>
						<tr class="noedit">
							<td>Name</td>
							<td><input type="value" value="<?php echo $contactmeta->first_name.' '.$contactmeta->last_name; ?>" disabled></td>
						</tr>
						<tr>
							<td>E-Mail</td>
							<td style="padding-top: 3px">
								<input type="value" name="mail" placeholder="E-Mail Adresse" maxlength="50" value="<?php echo $contactmeta->mail; ?>">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<button class="imgbutton disabled" name="cancel" disabled><img src="./images/overlay/button-trash.png"></button>
								<button class="imgbutton disabled" name="submit" disabled><img src="./images/overlay/button-check.png"></button>
							</td>
						</tr>
					</table>
				</div>
				<div id="ohours">
					<p class="desc">Definieren Sie hier die &Ouml;ffnungszeiten dieser Station. Sonderreglungen k&ouml;nnen mit dem Support ausgemacht werden.</p>

					<table>
						<tr>
							<td>Montag</td>
							<td><input type="time"></td>
							<td>bis</td>
							<td><input type="time"></td>
						</tr>
						<tr>
							<td>Dienstag</td>
							<td><input type="time"></td>
							<td>bis</td>
							<td><input type="time"></td>
						</tr>
						<tr>
							<td>Mittwoch</td>
							<td><input type="time"></td>
							<td>bis</td>
							<td><input type="time"></td>
						</tr>
						<tr>
							<td>Donnerstag</td>
							<td><input type="time"></td>
							<td>bis</td>
							<td><input type="time"></td>
						</tr>
						<tr>
							<td>Freitag</td>
							<td><input type="time"></td>
							<td>bis</td>
							<td><input type="time"></td>
						</tr>
						<tr>
							<td>Samstag</td>
							<td><input type="time"></td>
							<td>bis</td>
							<td><input type="time"></td>
						</tr>
						<tr>
							<td>Sonntag</td>
							<td><input type="time"></td>
							<td>bis</td>
							<td><input type="time"></td>
						</tr>
					</table>
				</div>
				<?php
					$supportsql = $db->query('select value1 from station_properties where station_id = "'.mysqli_real_escape_string($db, $_COOKIE['station_id']).'" and key1 = "support_verification" and key2 = "null" and (value2 is NULL or value2 <= "'.time().'") limit 1');

					if($supportsql->num_rows > 0)
					{
						$supportmeta = $supportsql->fetch_object();

						echo '
						<div id="support">
							<p class="desc">Hier k&ouml;nnen Sie die Kommunikation mit dem Support verifizieren, so dass dieser Sie als berechtigten Kommunikationspartner erkennen kann, bevor &Auml;nderungen an Ihrer Station vorgenommen werden k&ouml;nnen.</p>';

							if($supportmeta->value1 != null)
							{
								echo '<p class="desc" style="font-size: 13px"><b>Anmerkung:</b> '.$supportmeta->value1.'</p>';
							}

							echo '
							<div id="action">
								<button class="imgbutton" name="cancel"><img src="./images/overlay/button-trash.png"><p>Abbrechen</p></button>
								<button class="imgbutton" name="submit"><img src="./images/overlay/button-check.png"><p>Best&auml;tigen<p></button>
							</div>
						</div>';
					}
				?>
			</div>


			<!-- Settings-Menü -->
			<div id="option-menu">
				<div class="option-menu-entry active">
					<img src="./images/overlay/settings/key.png">
					<p>P&auml;chter-PIN</p>
				</div>
				<div class="option-menu-entry">
					<img src="./images/overlay/settings/lock.png">
					<p>Stationspasswort</p>
				</div>
				<div class="option-menu-entry">
					<img src="./images/overlay/settings/mail.png">
					<p>Kontaktdaten</p>
				</div>
				<div class="option-menu-entry" style="display: none">
					<img src="./images/overlay/settings/clock.png">
					<p>&Ouml;ffnungszeiten</p>
				</div>
				<div class="option-menu-entry" style="display: none">
					<img src="./images/overlay/settings/report.png">
					<p>Vers&auml;umnismeldungen</p>
				</div>

				<?php
					if($supportsql->num_rows > 0)
					{
						// sollte immer der letzte Menüpunkt sein, da dies ein Selektor-Kriterium ist!
						echo '
						<div class="option-menu-entry">
							<img src="./images/overlay/settings/support.png">
							<p>Support Verifikation</p>
						</div>';
					}
				?>
			</div>
		</div>


		<script type="text/javascript">

			$(function() // wird nach dem Laden der Seite ausgeführt
		    {
		    	$.when( // benötgte Scripte nachladen
                	$.ajax(
            		{
        				url: './javascript/functions/ppin.js',
        				dataType: 'script',
        				success: function()
        				{
        					$.ajax({url: './javascript/functions/overlay-settings.js', dataType: 'script'});
        				}
        			})
            	)
  				.then(null, function()
				{
					overlay_cancel({"success": false, "errors": {"6e016": "Das Content-Overlay konnte nicht geladen werden!"}, "data": null});
				});
		    });

		</script>
<?php
	}
	else
	{
		echo '
		<script type="text/javascript">

			$(function() // wird nach dem Laden der Seite ausgeführt
		    {
		    	overlay_cancel({"success": false, "errors": {"6e014": "Das Content-Overlay konnte nicht geladen werden!"}, "data": null})
		    	handle_errors("overlay", '.json_encode($db_connect).');
		    });
		</script>';
	}
?>
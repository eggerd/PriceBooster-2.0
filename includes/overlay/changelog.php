<?php

	require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/login.php');


	$authenticated = authenticated();

    if($authenticated['success'] && $authenticated['data'] === true)
    {
    	echo '<link href="./css/overlay-changelog.css" type="text/css" rel="stylesheet" />';


		$data = file_get_contents(realpath(dirname(__FILE__).'/../../').'/changelog.xml');
		$xml = simplexml_load_string($data);

		echo '<h6 id="current_version">aktuelle Version: '.SERVICE_BUILD.'</h6>';

	    foreach($xml->version as $version) // für jede Version im Changelog
	    {
	        if(!isset($version['display']) || $version['display'] == "true") // nur wenn diese Version auf für die Anzeige freigegeben ist
	        {
	        	$hidden_changes = 0;

		        echo '
		        <div class="version">
		        	<h4 class="build">'.$version->build.' </h4>
		        	<h6 class="date">'.$version->date.'</h6><br />'; // Versionsnummer und Releasdate ausgeben

		        foreach ($version->changes->change as $change) // für alle Changes innerhalb dieser Version
		        {
		        	if(!isset($change['display']) || $change['display'] == "true") // sofern der Eintrag auf öffentlich ist
		        	{
		        		if(isset($change['type']) && isset($change['action'])) // nur wenn für den Change auch alle nötigen Attribute definiert wurden
		        		{
			        		// Change-Note ausgeben
			        		echo '
			        		<div class="change">
			        			<img class="icon" src="./images/overlay/changelog/'.$change['action'].'.png">
			        			<p class="description">'.$change->text.'</p>
			        		</div>';
		        		}
		        	}
		        	else
		        	{
		        		$hidden_changes++;
		        	}
		        }

		        if($hidden_changes > 0) // kleinen Counter für versteckte Changes einblenden
		        {
		        	echo '
	        		<div class="change">
	        			<p class="description hidden_changes">+'.$hidden_changes.' weitere kleine Änderungen</p>
	        		</div>';
		        }

		        echo '</div>';
	        }
	    }
    }
    else
    {
    	echo '
		<script type="text/javascript">

			$(function() // wird nach dem Laden der Seite ausgeführt
		    {
		    	overlay_cancel({"success": false, "errors": {"6e029": "Das Content-Overlay konnte nicht geladen werden!"}, "data": null})
		    	handle_errors("overlay", '.json_encode($authenticated).');
		    });
		</script>';
    }

?>
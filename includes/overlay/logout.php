<style type="text/css">
	#overlay #context {margin-left: 0px !important; margin-top: 0px !important;}
</style>

<!-- Pächter-PIN Abfrage -->
<div id="reqppin">
	<p style="color: red; margin: 0 0 5px 0">Sie sind im Begriff den Client abzumelden!</p>
	<p>Bitte geben Sie den Pächter-PIN ein, um fortzufahren</p>
	<div id="value" class="ppinform">
		<input type="password" maxlength="1">
		<input type="password" maxlength="1">
		<input type="password" maxlength="1">
		<input type="password" maxlength="1">
		<button class="imgbutton" name="submit" disabled><img src="./images/overlay/button-check.png"></button>
	</div>
</div>


<script type="text/javascript">

	$(function() // wird nach dem Laden der Seite ausgeführt
    {
    	$.when( // benötgte Scripte nachladen
        	$.ajax(
    		{
				url: './javascript/functions/ppin.js',
				dataType: 'script',
				success: function()
				{
					overlay_show();

					$overlay_view = new overlay_reqppin($('#overlay #reqppin'), function()
					{
						logout();
					});
				}
			})
    	)
		.then(null, function()
		{
			overlay_cancel({"success": false, "errors": {"6e016": "Das Content-Overlay konnte nicht geladen werden!"}, "data": null});
		});
    });

</script>
<?php

	require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/login.php');


	$authenticated = authenticated();

    if($authenticated['success'] && $authenticated['data'] === true)
    {
		echo '
		<p style="margin: 0">Sollten Sie Fragen, Probleme, Verbesserungsvorschl&auml;ge oder sonstige Anliegen haben,
		verwenden Sie bitte eine der nachfolgenden Kontaktm&ouml;glichkeiten um sich mit mir in
		Verbindung zu setzen.<br>
		<br>
		Nennen Sie bitte immer Ihre Stationsnummer und, im Fall von Problemen,
		bitte auch einen Zeitraum sowie die Kennungsnummer von Fehlermeldungen (neben dem Titel der
		Meldung), sofern vorhanden.</p>

		<table style="margin: 15px">
			<tr>
				<td style="width: 22px"><img src="./images/overlay/contact/person.png" ></td>
				<td>Dustin Eckhardt</td>
			</tr>
			<tr>
				<td><img src="./images/overlay/contact/mail.png" ></td>
				<td>pricebooster@dustin-eckhardt.de</td>
			</tr>
		</table>

		<p style="font-size: x-small; margin-left: 15px">Bitte gew&auml;hren Sie eine Antwortzeit von bis zu 24 Stunden!</p>';
	}
    else
    {
    	echo '
		<script type="text/javascript">

			$(function() // wird nach dem Laden der Seite ausgeführt
		    {
		    	overlay_cancel({"success": false, "errors": {"6e031": "Das Content-Overlay konnte nicht geladen werden!"}, "data": null})
		    	handle_errors("overlay", '.json_encode($authenticated).');
		    });
		</script>';
    }
?>
<?php

	function authenticated()
	{
		if(isset($_COOKIE['logs']))
		{
			return array('success' => true, 'errors' => null, 'data' => true);
		}
		else
		{
			return array('success' => true, 'errors' => null, 'data' => false);
		}
	}





	function login($password)
	{
		require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
		require_once(realpath(dirname(__FILE__).'/../libraries').'/iha.class.php');

		$iha = new iha();

		if($iha->compare($password, LOGs_LOGIN_PASSWORD)) // Passwörter vergleichen
		{
			if(setcookie('logs', time(), time() + LOGS_LOGIN_DURATION, '/')) // wenn das Login-Cookie gesetzt werden konnte
			{
				return array('success' => true, 'errors' => null, 'data' => true); // Login war erfolgreich
			}
			else
			{
				return array('success' => false, 'errors' => array('7e001' => 'Das Authentication-Cookie konnte nicht gesetzt werden'), 'data' => null);
			}
		}
		else
		{
			return array('success' => true, 'errors' => null, 'data' => false); // Passwörter stimmen nicht überein
		}
	}

?>
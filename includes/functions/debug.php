<?php

   /* Version:  1.0.1
    * Stand:    13. August 2015
    *
    * Input:
    *   $station_id 	: integer 	= die ID der zugehörigen Station
    *   $dump 			: string 	= das Dump des Logs, das zu speichern ist
    *
    * Success:
    *   array
    *   {
    *       ['success']	=> true 	: boolean
    *       ['errors']  => null 	: null
    *       ['data']    => 			: boolean
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    * 	passed 	-> db_connect()
    * 	passed	-> debug_prepare_directory()
    * 	0e002 	=> "Die Station des Debug-Dump wurde nicht gefunden!"
    * 	0e003 	=> "Die Logdatei konnte nicht geöffnet werden, um den Dump zu speichern!"
    */
	function debug_save_dump($station_id, $dump)
	{
		require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
        	// Stationsnummer und Debug-Status abfragen
        	$sql = $db->query('select debug, name from stations where id = '.mysqli_real_escape_string($db, $station_id).' limit 1');

            if($sql->num_rows > 0) // wenn die Station gefunden wurde
            {
            	$row = $sql->fetch_object();
                if($row->debug == "true") // nur wenn Debugging für die Station aktiviert ist
                {
					$path = debug_prepare_directory($station_id, $row->name); // Zielverzeichnis überprüfen und vorbereiten

					if($path['success'] === true)
					{
						// Debug-Log-Datei öffnen
	                	$file = fopen($path['data'].date('Y-m-d', time()).'.log', 'a');

	                	if($file !== false) // nur wenn die Datei geöffnet werden konnte
	                	{
	                		fwrite($file, $dump.PHP_EOL); // Dump in die Log-Datei schreiben
	                		fclose($file); // Datei wieder schließen

	                		return array('success' => true, 'errors' => null, 'data' => true);
	                	}
	                	else // Datei konnte nicht geöffnet werden
	                	{
	                		return array('success' => false, 'errors' => array('0e003' => 'Die Logdatei konnte nicht ge&ouml;ffnet werden, um den Dump zu speichern!'), 'data' => null);
	                	}
                	}
                	else // Zielverzeichnis konnte nicht überprüft werden
                	{
                		return array('success' => false, 'errors' => $path['errors'], 'data' => null);
                	}
                }
                else // Debugging ist für die Station deaktiviert
                {
                	return array('success' => true, 'errors' => null, 'data' => false);
                }
            }
            else // die Station konnte nicht gefunden werden
            {
                return array('success' => false, 'errors' => array('0e002' => 'Die Station des Debug-Dump wurde nicht gefunden!'), 'data' => null);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
	}





   /* Überprüft das Zielverzeichnis für die Log-Datei, ob dieses bereits existiert
   	* und ob die maximale Gesamtgröße an Log-Dateien nicht überschritten wurde.
   	* Sollte die Gesamtgröße überschritten werden, werden ältere Logs gelöscht.
   	*
   	* Version: 	1.0.0
   	* Stand: 	13. August 2015
   	*
   	* Input:
   	* 	$station_id 	: integer 	= die ID der zugehörigen Station
   	* 	$station_name	: string 	= die Stationsnummer
   	*
   	* Success:
   	* 	array
    *   {
    *       ['success']	=> true 	: boolean
    *       ['errors']  => null 	: null
    *       ['data']    => string	: boolean 	= Pfad zum Zielverzeichnis
    *   }
    *
    * Failure:
    * 	array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
   	*
   	* Errors:
   	*	0e004	=> "Das Zielverzeichnis für die Log-Datei konnte auf dem Server nicht erstellt werden!"
    */
	function debug_prepare_directory($station_id, $station_name)
	{
		require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');

		$path_base = realpath(dirname(__FILE__).'/../../').'/'.DEBUG_DIRECTORY.'/';
		$path_dir = strtolower($station_name).'_'.$station_id.'/';

		if(!is_dir($path_base.$path_dir)) // falls das Verzeichnis noch nicht existiert
		{
			if(!mkdir($path_base.$path_dir, 0755, true)) // falls das Verzeichnis nicht angelegt werden konnte
			{
				return array('success' => false, 'errors' => array('0e004' => 'Das Zielverzeichnis f&uuml;r die Log-Datei konnte auf dem Server nicht erstellt werden!'), 'data' => null);
			}
			else // Verzeichnis wurde erstellt
			{
				return array('success' => true, 'errors' => null, 'data' => $path_base.$path_dir);
			}
		}
		else // Verzeichnis ist bereits vorhanden
		{
			$files = scandir($path_base.$path_dir); // Dateien aus dem Verzeichnis auslesen (in Array gespeichert)

			if($files !== false) // wenn die Dateien ausglesesen werden konnten
			{
				$dir_size = 0; // speichert die Summe der Dateigrößen

				foreach($files as $file) // für jeden Dateinamen im Array
				{
					if(filetype($path_base.$path_dir.$file) == 'file') // nur wenn es sich auch um eine Datei und nicht um einen Ordner handelt
					{
						$dir_size += filesize($path_base.$path_dir.$file); // Dateigröße zur Summe hinzufügen
					}
				}


				if(($dir_size / 1024 / 1024) >= DEBUG_MAX_LOG_SIZE) // falls die maximale Größe überschritten wurde
				{
					for($i = 0; $i < count($files); $i++) // für jeden Dateinamen im Array
					{
						if(filetype($path_base.$path_dir.$files[$i]) == 'file') // nur wenn es sich auch um eine Datei und nicht um einen Ordner handelt
						{
							$file_size = filesize($path_base.$path_dir.$files[$i]); // Dateigröße zwischenspeichern

							if(unlink($path_base.$path_dir.$files[$i])) // Versuch die Datei zu löschen
							{
								$dir_size -= $file_size; // Größe der Datei von der Summe abziehen
							}
						}

						// sofern entweder die Hälfte der maximalen Größe erreicht wurde, oder nur noch X Logs vorhanden sind
						if(($dir_size / 1024 / 1024) <= (DEBUG_MAX_LOG_SIZE / 2) || (count($files) - DEBUG_PRESERVE_LOGS) <= $i + 1)
						{
							return array('success' => true, 'errors' => null, 'data' => $path_base.$path_dir); // Löschen alter Logs hiermit beenden
						}
					}
				}
				else // maximale Größe wurde nicht überschritten
				{
					return array('success' => true, 'errors' => null, 'data' => $path_base.$path_dir);
				}
			}
			else // das Verzeichnis konnte nicht nach Dateien durchsucht werden
			{
				return array('success' => false, 'errors' => array('0e005' => 'Auf das Zielverzeichnis f&uuml;r die Log-Dateien konnte nicht zugegriffen werden!'), 'data' => null);
			}
		}
	}

?>
<?php

   /* Übernimmt den übergebenen PPIN für den Pächter der aktuell angemeldeten Station.
   	* Der PPIN wird als IHA-Hash in der Datenbank hinterlegt.
   	*
   	* Version: 	1.0.0
   	* Stand: 	26. November 2015
   	*
   	* Input:
   	* 	$ppin 	: string 	= der PPIN in Klartext
   	*
   	* Success:
   	* 	array
   	* 	{
   	* 		['success'] => true : boolean
   	* 		['errors']	=> null : null
   	* 		['data'] 	=> true : boolean
   	* 	}
   	*
   	* Failure:
   	* 	array
   	* 	{
   	* 		['success'] => false : boolean
   	* 		['errors']	=> array
   	* 		{
   	* 			[: string] 	=> : string
   	* 		}
   	* 		['data'] 	=> null : null
   	* 	}
   	*
   	* Errors:
   	* 	passed 	-> mysql()
   	* 	6e008 	=  "Der Pächter-PIN konnte nicht gespeichert werden!"
   	* 	6e022	=  "Die Stationskennung konnte nicht ermittelt werden!"
    */
	function set_ppin($ppin)
	{
		require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
		require_once(dirname(__FILE__).'/mysql.php');
		require_once(realpath(dirname(__FILE__).'/../libraries').'/iha.class.php');

		$db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            if(isset($_COOKIE['station_id']))
            {
	            $iha = new iha();
				$hash = $iha->hash($ppin);

				if($db->query('update tenants set password = "'.$hash.'" where id = (select tenant_id from stations where id = "'.mysqli_real_escape_string($db, $_COOKIE['station_id']).'" limit 1)'))
				{
					return array('success' => true, 'errors' => null, 'data' => true);
				}
				else
				{
					return array('success' => false, 'errors' => array('6e008' => 'Der P&auml;chter-PIN konnte nicht gespeichert werden!'), 'data' => null);
				}
			}
			else
			{
				return array('success' => false, 'errors' => array('6e022' => 'Die Stationskennung konnte nicht ermittelt werden!'), 'data' => null);
			}
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
	}





   /* Übernimmt die angegebenen Kontaktdaten für den Pächter der aktuell angemeldeten
   	* Station. Die Kontaktdaten können hierbei dynamisch übergeben werden - heißt, nur
   	* die geänderten Daten können übergeben werden.
   	*
   	* Version: 	1.0.0
   	* Stand: 	26. November 2015
   	*
   	* Input:
   	* 	$data 	= array
   	* 	{
   	* 		[: string] 	=> : mixed 	= die Value wird unter dem zugehörigen Feldnamen der Datenbank gespeichert
   	* 	}
   	*
   	* Success:
   	* 	array
   	* 	{
   	* 		['success'] => true : boolean
   	* 		['errors']	=> null : null
   	* 		['data'] 	=> true : boolean
   	* 	}
   	*
   	* Failure:
   	* 	array
   	* 	{
   	* 		['success'] => false : boolean
   	* 		['errors']	=> array
   	* 		{
   	* 			[: string] 	=> : string
   	* 		}
   	* 		['data'] 	=> null : null
   	* 	}
   	*
   	* Errors:
   	* 	passed 	-> 	mysql()
   	* 	6e015 	=  	"Die Kontaktdaten konnte nicht gespeichert werden!"
   	* 	6e023 	=	"Die Stationskennung konnte nicht ermittelt werden!"
    */
	function set_contact($data)
	{
		require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
		require_once(dirname(__FILE__).'/mysql.php');

		$db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
        	if(isset($_COOKIE['station_id']))
        	{
	        	$querymeta = array();

	            foreach($data as $field => $value)
	            {
	            	$querymeta[] = $field.' = "'.mysqli_real_escape_string($db, $value).'"';
	            }

				if($db->query('update tenants set '.implode(', ', $querymeta).' where id = (select tenant_id from stations where id = "'.mysqli_real_escape_string($db, $_COOKIE['station_id']).'" limit 1)'))
				{
					return array('success' => true, 'errors' => null, 'data' => true);
				}
				else
				{
					return array('success' => false, 'errors' => array('6e015' => 'Die Kontaktdaten konnte nicht gespeichert werden!'), 'data' => null);
				}
			}
			else
			{
				return array('success' => false, 'errors' => array('6e023' => 'Die Stationskennung konnte nicht ermittelt werden!'), 'data' => null);
			}
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
	}





   /* Übernimmt das übergebene Passwort für für die aktuell angemeldete Station.
   	* Das Passwort wird als IHA-Hash in der Datenbank hinterlegt.
   	*
   	* Version: 	1.0.0
   	* Stand: 	08. Dezember 2015
   	*
   	* Input:
   	* 	$passwd	: string 	= der PPIN in Klartext
   	*
   	* Success:
   	* 	array
   	* 	{
   	* 		['success'] => true : boolean
   	* 		['errors']	=> null : null
   	* 		['data'] 	=> true : boolean
   	* 	}
   	*
   	* Failure:
   	* 	array
   	* 	{
   	* 		['success'] => false : boolean
   	* 		['errors']	=> array
   	* 		{
   	* 			[: string] 	=> : string
   	* 		}
   	* 		['data'] 	=> null : null
   	* 	}
   	*
   	* Errors:
   	* 	passed 	-> mysql()
   	* 	6e027 	=  "Das Passwort konnte nicht gespeichert werden!"
   	* 	6e028	=  "Die Stationskennung konnte nicht ermittelt werden!"
    */
	function set_passwd($passwd)
	{
		require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
		require_once(dirname(__FILE__).'/mysql.php');
		require_once(realpath(dirname(__FILE__).'/../libraries').'/iha.class.php');

		$db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            if(isset($_COOKIE['station_id']))
            {
	            $iha = new iha();
				$hash = $iha->hash($passwd);

				if($db->query('update stations set password = "'.$hash.'" where id = "'.mysqli_real_escape_string($db, $_COOKIE['station_id']).'"'))
				{
					return array('success' => true, 'errors' => null, 'data' => true);
				}
				else
				{
					return array('success' => false, 'errors' => array('6e027' => 'Das Passwort konnte nicht gespeichert werden!'), 'data' => null);
				}
			}
			else
			{
				return array('success' => false, 'errors' => array('6e028' => 'Die Stationskennung konnte nicht ermittelt werden!'), 'data' => null);
			}
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
	}





   /* Speichert die Antwort des Client, auf die aktuell vorhandene Support Verifikations-
   	* Anfrage. Die Antwort wird hierbei in Key2 gespeichert. Key2 muss bis dahin "null"
   	* sein! Falls ein Timestamp vorhanden ist, wird dieser beim Speichern nicht mehr
   	* beachtet (nur beim Öffnen der Einstellungen).
   	*
   	* Version: 	1.0.0
   	* Stand: 	18. Dezember 2015
   	*
   	* Input:
   	* 	$answer	: string 	= "true", "false" als Antwort des Client
   	*
   	* Success:
   	* 	array
   	* 	{
   	* 		['success'] => true : boolean
   	* 		['errors']	=> null : null
   	* 		['data'] 	=> true : boolean
   	* 	}
   	*
   	* Failure:
   	* 	array
   	* 	{
   	* 		['success'] => false : boolean
   	* 		['errors']	=> array
   	* 		{
   	* 			[: string] 	=> : string
   	* 		}
   	* 		['data'] 	=> null : null
   	* 	}
   	*
   	* Errors:
   	* 	passed 	-> mysql()
   	* 	passed 	-> authenticated()
   	* 	6e033 	=  "Die Verifikation konnte nicht gespeichert werden!"
    */
	function support_verification($answer)
	{
		require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
		require_once(dirname(__FILE__).'/login.php');
		require_once(dirname(__FILE__).'/mysql.php');

		$db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            $authenticated = authenticated(); // Login Status abfragen

            if($authenticated['success']) // nur wenn Abfrage erfolgreich
            {
            	if($authenticated['data'] === true) // nur wenn auch angemeldet
            	{
            		// Antwort in Datenbank speichern
            		if($db->query('update station_properties set key2 = "'.mysqli_real_escape_string($db, $answer).'" where station_id = "'.mysqli_real_escape_string($db, $_COOKIE['station_id']).'" and key1 = "support_verification" and key2 = "null"'))
					{
						return array('success' => true, 'errors' => null, 'data' => true);
					}
					else
					{
						return array('success' => false, 'errors' => array('6e033' => 'Die Verifikation konnte nicht gespeichert werden!'), 'data' => null);
					}
            	}
            }
            else
            {
            	return array('success' => false, 'errors' => $authenticated['errors'], 'data' => null);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
	}

?>
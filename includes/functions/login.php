<?php

   /* Überprüft ob die Station angemeldet ist und ob die vorhandene Sitzung
    * überhaupt noch gültig ist.
    *
    * Version:  1.0.2
    * Stand:    12. September 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => true : boolean
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  ->  db_connect()
    *   1e001   =   "Die Login-Session ist ungültig!"
    */
    function authenticated()
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        if(isset($_COOKIE['session_id'])) // nur wenn überhaupt ein Cookie gesetzt ist
        {
            $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

            if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
            {
                $session = $db->query('select session_id from station_sessions where session_id = "'.mysqli_real_escape_string($db, $_COOKIE['session_id']).'" limit 1');

                if($session->num_rows > 0) // wenn eine aktive Session gefunden wurde
                {
                    return array('success' => true, 'errors' => null, 'data' => true);
                }
                else
                {
                    // Cookies löschen
                    setcookie('session_id', '', 0, '/');
                    setcookie('station_id', '', 0, '/');
                    setcookie('support', '', 0, '/');

                    return array('success' => true, 'errors' => array('1n001' => 'Die Login-Session ist ung&uuml;ltig oder abgelaufen!'), 'data' => false);
                }
            }
            else // Verbindung zur Datenbank ist fehlgeschlagen
            {
                return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
            }
        }
        else // kein Station-ID-Cookie gesetzt
        {
            return array('success' => true, 'errors' => null, 'data' => false);
        }
    }





   /* Überprüft die Zugangsdaten der Station und meldet diese ggf. an,
    * generiert also die Session ID, legt diese in der Datenbank an und
    * setzt das Cookie.
    *
    * Version:  1.0.3
    * Stand:    10. Januar 2016
    *
    * Input:
    *   $station 	: integer
    *   $password 	: string
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => true : boolean
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  ->  db_connect()
    *   1e002   =   "Die Kombination aus Stationsnummer und Passwort ist nicht korrekt!"    ==>     "Die Station ist nicht beim PriceBooster registriert!"
    *   1w003   =   "Die Station ist zurzeit deaktiviert!"
    *   1e004   =   "Die Kombination aus Stationsnummer und Passwort ist nicht korrekt!"
    *   1e005   =   "Beim Erstellen der Session ist ein Datenbankfehler aufgetreten!"
    *   1e012	= 	"Die für den Login benötigten Cookies konnten nicht gesetzt werden!"
    */
    function login($station, $password)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            $sql = $db->query('select id, password, active from stations where name = "'.mysqli_real_escape_string($db, $station).'" limit 1'); // Daten der Station laden

            if($sql->num_rows > 0 && $row = $sql->fetch_object()) // nur wenn auch eine Station gefunden wurde
            {
                require_once(realpath(dirname(__FILE__).'/../libraries').'/iha.class.php');
                $iha = new iha();

                if($iha->compare($password, $row->password)) // Passwörter vergleichen
                {
                    if($row->active == 'true') // nur wenn die Station aktuell aktiv ist
                    {
                        $session_id = sha1(time().$_SERVER['REMOTE_ADDR'].rand(0, 9999999)); // Session ID generieren

                        if($db->query('delete from station_sessions where support = "false" and station_id = '.$row->id)) // bereits vorhandene Sessions löschen
                        {
                            if($db->query('insert into station_sessions (session_id, station_id, timestamp, ip) values ("'.$session_id.'", '.$row->id.', '.time().', "'.$_SERVER['REMOTE_ADDR'].'")')) // neue Session erstellen
                            {
                                // Session-Cookies setzen
                                if(setcookie('session_id', $session_id, time() + 31556926, '/') && setcookie('station_id', $row->id, time() + 31556926, '/'))
                                {
                                	return array('success' => true, 'errors' => null, 'data' => true); // Login wurde erfolgreich abgeschlossen
                                }
                                else // Cookies konnten nicht gesetzt werden
                                {
                                	return array('success' => false, 'errors' => array('1e012' => 'Die f&uuml;r den Login ben&ouml;tigten Cookies konnten nicht gesetzt werden!'), 'data' => null);
                                }
                            }
                        }

                        return array('success' => false, 'errors' => array('1e005' => 'Beim Erstellen der Session ist ein Datenbankfehler aufgetreten!'), 'data' => null);
                    }
                    else // die Station ist als inaktiv markiert
                    {
                        return array('success' => true, 'errors' => array('1e003' => 'Die Station ist zurzeit deaktiviert!'), 'data' => false);
                    }
                }
                else // Passwörter stimmen nicht überein
                {
                    $errors = array('1e004' => 'Die Kombination aus Stationsnummer und Passwort ist nicht korrekt!'); // Fehlermeldung die auf jeden Fall übergeben wird, außer bei erfolgreichem Login
                    $support = support_login($station, $password); // versucht einen Support Login durchzuführen

                    if($support['success']) // sofern keine Fehler aufgetreten sind
                    {
                    	if($support['data'] === true) // wenn der Support-Login erfolgreich war
                    	{
                    		return array('success' => true, 'errors' => $support['errors'], 'data' => true); // Login wurde erfolgreich abgeschlossen
                    	}
                    }
                    else // beim Versuch einen Support-Login durchzuführen, sind Fehler aufgetreten
                    {
                    	if($support['errors'] != null)
                    	{
	                    	foreach ($support['errors'] as $code => $message) // alle Fehlermeldungen in Array schreiben
	                    	{
	                    		$errors[$code] = $message;
	                    	}
	                    }
                    }

                    return array('success' => true, 'errors' => $errors, 'data' => false);
                }
            }
            else // die Station exestiert nicht
            {
                return array('success' => true, 'errors' => array('1e002' => 'Die Kombination aus Stationsnummer und Passwort ist nicht korrekt!'), 'data' => false);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }





   /* Wird von login() aufgerufen, wenn das übermittelte Passwort nicht mit dem der
   	* Station übereinstimmt. Die Funktion versucht ein Support-Login-Passwort für die
   	* Station zu finden und vergleicht dies mit dem übermittelten Passwort. Falls diese
   	* übereinstimmen wird ein Support-Login gestartet.
   	*
   	* Version: 	1.0.0
   	* Stand: 	12. September 2015
   	*
   	* Input:
   	* 	$station 	: string 	= der Name der Station
   	* 	$password 	: string 	= das Passwort
   	*
   	* Success:
   	* 	array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => array
    *       {
    *       	['1n016']	=> 'Support-Login' 	= wird bei erfolgreichem Login immer übergeben => Magic-Error!
    *           [string]    => : string 		= es ist möglich, dass weitere Meldungen übergeben werden
    *       }
    *       ['data']    => : boolean
    *   }
   	*
   	* Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    * 	passed  ->  db_connect()
    *   1e013   =   "Die für den Login benötigten Cookies konnten nicht gesetzt werden!"
    *   1e014	=	"Beim Erstellen der Support-Session ist ein Datenbankfehler aufgetreten!"
    *   1w015	= 	"Das Support-Passwort konnte nicht aus der Datenbank entfernt werden!"
    *   1n016	=	"Support-Login"
    */
    function support_login($station, $password)
    {
    	require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
        	$sql = $db->query('select s.id, sp.value1 from station_properties as sp, stations as s where s.name = "'.mysqli_real_escape_string($db, $station).'" and s.id = sp.station_id and sp.key1 = "support_login" limit 1'); // Passwort für den Support Login laden

            if($sql->num_rows > 0 && $row = $sql->fetch_object()) // nur wenn überhaupt ein Passwort vorhanden ist
            {
            	if($row->value1 === md5($password)) // wenn die Passwörter übereinstimmen
            	{
            		$session_id = sha1(time().$_SERVER['REMOTE_ADDR'].rand(0, 9999999)); // Session ID generieren

                    if($db->query('delete from station_sessions where support = "true" and station_id = '.$row->id)) // bereits vorhandene Support-Sessions löschen
                    {
                        if($db->query('insert into station_sessions (session_id, station_id, timestamp, ip, support) values ("'.$session_id.'", '.$row->id.', '.time().', "'.$_SERVER['REMOTE_ADDR'].'", "true")')) // neue Support-Session erstellen
                        {
                            // Session-Cookies setzen
                            if(setcookie('session_id', $session_id, time() + 31556926, '/') && setcookie('station_id', $row->id, time() + 31556926, '/') && setcookie('support', true, time() + 31556926, '/'))
                            {
                            	$errors = array('1n016' => 'Support-Login');

                            	if(!$db->query('delete from station_properties where key1 = "support_login" and station_id = '.$row->id)) // Support-Passwort löschen, da verwendet
                    			{
                    				$errors['1w015'] = "Das Support-Passwort konnte nicht aus der Datenbank entfernt werden!";
                    			}

                            	return array('success' => true, 'errors' => $errors, 'data' => true); // Login wurde erfolgreich abgeschlossen
                            }
                            else // Cookies konnten nicht gesetzt werden
                            {
                            	return array('success' => false, 'errors' => array('1e013' => 'Die f&uuml;r den Login ben&ouml;tigten Cookies konnten nicht gesetzt werden!'), 'data' => null);
                            }
                        }
                    }

                    return array('success' => false, 'errors' => array('1e014' => 'Beim Erstellen der Support-Session ist ein Datenbankfehler aufgetreten!'), 'data' => null);
            	}
            }
            else // Passwörter stimmen ebenfalls nicht überein
            {
            	return array('success' => true, 'errors' => $db_connect['errors'], 'data' => false);
            }
    	}
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }





   /* Beendet den Login des Clients auf Seite des Servers. Im Normalfall wird diese FUnktion
    * beim Logout des Clients aufgerufen - muss jedoch nicht zwingend aufgerufen werden.
    * Wichtig ist das eine von beiden Logout-Methoden ausgeführt werden.
    *
    * Version:  1.0.1
    * Stand:    17. Dezember 2015
    *
    * Input:
    *     none
    *
    * Success:
    *     array
    *     {
    *         ['success'] => true : boolean
    *         ['errors']  => null : null
    *         ['data']    => null : boolean
    *     }
    *
    * Failure:
    *     array
    *     {
    *         ['success'] => false : boolean
    *         ['errors']  => array
    *         {
    *             [string]    => : string
    *         }
    *         ['data']    => null : boolean
    *     }
    *
    * Errors:
    *     passed    ->  db_connect()
    *     passed 	-> 	authenticated()
    */
    function logout()
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $authenticated = authenticated();

        if($authenticated['success'])
        {
    		if($authenticated['data'] === true)
    		{
		        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

		        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
		        {
		            // Session des Logins löschen
		            $db->query('delete from station_sessions where station_id = "'.mysqli_real_escape_string($db, $_COOKIE['station_id']).'" and session_id = "'.mysqli_real_escape_string($db, $_COOKIE['session_id']).'"'); // betroffene Session löschen

		            if(mysqli_affected_rows($db) >= 0) // wenn bei der Delete-Aktion auch mindestens eine Zeile betroffen war
		            {
		                return array('success' => true, 'errors' => null, 'data' => null);
		            }
		            else // offenbar wurde nichts gelöscht
		            {
		                return array('success' => false, 'errors' => null, 'data' => null);
		            }
		        }
		        else // Verbindung zur Datenbank ist fehlgeschlagen
		        {
		            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
		        }
	        }
	        else
	        {
	        	return array('success' => true, 'errors' => null, 'data' => null);
	        }
        }
        else
        {
        	return array('success' => false, 'errors' => $authenticated['errors'], 'data' => null);
        }
    }

?>
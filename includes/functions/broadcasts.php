<?php

   /* Lädt alle verfügbaren Broadcasts aus der Datenbank. Sofern keine Stations-ID
    * übergeben wird, werden einfach alle Broadcasts
    *
    * Version:  1.1.0
    * Stand:    11. August 2016
    *
    * Input:
    *   $station_id : integer   => null     = gibt die ID der Station an (optional)
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => array
    *       {
    *           ['id']          => : integer
    *           ['title']       => : string
    *           ['type']        => : string
    *           ['content']     => : string
    *       }
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  ->  db_connect()
    */
    function fetch_broadcasts($station_id = null)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            $data = array();

            // globale Broadcasts auslesen
            $sql_global = $db->query('select id, title, content, type from system_broadcasts where active = "true" && global = "true" && (start_timestamp is NULL || start_timestamp <= '.time().') && (end_timestamp is NULL || end_timestamp >= '.time().') order by id');

            if($sql_global->num_rows > 0) // sofern Broadcasts vorhanden sind
            {
                while($row = $sql_global->fetch_assoc()) // geladene Daten im Rückgabe-Array schreiben
                {
                    $data[] = array('id' => $row['id'], 'title' => $row['title'], 'type' => $row['type'], 'content' => $row['content']);
                }
            }

            if($station_id != null) // wenn eine Station definiert wurde
            {
                // Broadcasts für die Station laden
                $sql_station = $db->query('select b.id, b.title, b.content, b.type from system_broadcasts as b, system_broadcast_station_join as bsj, stations as s where s.id = '.mysqli_real_escape_string($db, $station_id).' && s.id = bsj.station_id && bsj.message_id = b.id && b.active = "true" && b.global = "false" && (b.start_timestamp is NULL || b.start_timestamp <= '.time().') && (b.end_timestamp is NULL || b.end_timestamp >= '.time().')');

                if($sql_station->num_rows > 0) // sofern Broadcasts vorhanden sind
                {
                    while($row = $sql_station->fetch_assoc()) // geladene Daten im Rückgabe-Array schreiben
                    {
                        $data[] = array('id' => $row['id'], 'title' => $row['title'], 'type' => $row['type'], 'content' => $row['content']);
                    }
                }
            }

            if(count($data) > 0) // wenn Broadcasts zum Zurückgeben vorhanden sind
            {
                return array('success' => true, 'errors' => null, 'data' => $data);
            }
            else
            {
                return array('success' => true, 'errors' => null, 'data' => null);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }

?>
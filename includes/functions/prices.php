<?php

   /* Initialisiert das Abfragen der Preise und alle dazugehörigen Schritte wie z.B. das
   	* Vergleichen der abgerufenen Preise oder das Anwenden von Workarrounds.
   	*
   	* Version: 	1.1.5
   	* Stand: 	23. Juni 2016
   	*
   	* Input:
   	* 	$station_id 	: integer 	= die ID der Station, deren Preise abgeglichen werden soll
   	*
   	* Success:
   	*	passed 	-> order_price_array()
   	*
   	* Failure:
   	* 	array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    * 	2n013 	=> "Die Abfrage erfolgt außerhlab der Öffnungszeiten dieser Station!"
    */
    function price_validation($station_id)
    {
	   $opend = check_openinghours($station_id); // Öffnungszeiten überprüfen

        if($opend['success']) // sofern die Überprüfung erfolgreich war
        {
            if($opend['data'] === true) // nur wenn die Station auch geöffnet hat
            {
                $station_meta = fetch_station_meta($station_id); // Stations-Daten laden (Spritsorten, URL usw.)

                if($station_meta['success']) // nur wenn die Stations-Daten geladen werden konnten
                {
                    $station_prices = fetch_fuel_prices($station_meta['data']['reference_url'], $station_meta['data']['fuels']); // Spritpreise für die eigene Station laden

                    if($station_prices['success']) // wenn die Preise geladen werden konnten
                    {
                        $analyse_meta = analyse_station_prices($station_id, $station_prices['data']); // auf Preiserhöhungen der eigenen Station prüfen

                        if($analyse_meta['success'] && $analyse_meta['errors'] == null) // nur wenn keine Preiserhöhung festgestellt wurde
                    	{
	                        $reference_meta = fetch_station_references($station_id); // Daten (wie z.B. Preise) zu den Referenzen laden

	                        if($reference_meta['success']) // wenn die Referenz-Daten geladen werden konnten
	                        {
	                            $prices = apply_fuel_workarounds($station_id, compare_prices($analyse_meta['data'], $reference_meta['data'])['data']); // Stations-Preise und Referenz-Preise vergleichen, nachdem Workaround angewand wurden

	                            if($prices['success']) // wenn die finalen Preise zusammengestellt werden konnten
	                            {
	                                $ordered_prices = order_price_array($station_meta['data'], $prices['data']); // Daten noch sortieren und dann zurückgeben
	                                dereliction_lookup($station_id, $ordered_prices['data']); // zusätzlich prüfen ob ein Dereliction-Alert gesendet werden soll

	                                return $ordered_prices;
	                            }
	                            else // es sind Fehler beim Vergleichen der Preise oder bei den Workarounds aufgetreten
	                            {
	                                return array('success' => false, 'errors' => $prices['errors'], 'data' => null);
	                            }
	                        }
	                        else // Referenz-Daten konnten nicht geladen werden
	                        {
	                            return array('success' => false, 'errors' => $reference_meta['errors'], 'data' => null);
	                        }
                        }
                        else // Analyse war erfolgreich und / oder Fehler sind aufgetreten
                        {
                        	if($analyse_meta['success']) // Preiserhöhung erkannt bzw. Rise-Timeout noch aktiv - Timestamp bis zum Ende des Timeouts zurückgeben
                        	{
                        		return array('success' => true, 'errors' => $analyse_meta['errors'], 'data' => $analyse_meta['data']);
                    		}
                    		else // Fehler bei der Analyse
                    		{
                    			return array('success' => false, 'errors' => $analyse_meta['errors'], 'data' => null);
                    		}
                        }
                    }
                    else // Stations-Preise konnten nicht geladen werden
                    {
                        return array('success' => false, 'errors' => $station_prices['errors'], 'data' => null);
                    }
                }
                else // Stations-Daten konnten nicht geladen werden
                {
                    return array('success' => false, 'errors' => $station_meta['errors'], 'data' => null);
                }
            }
            else // die Station hat geschlossen
            {
                return array('success' => true, 'errors' => array('2n013' => 'Die Abfrage erfolgt au&szlig;erhlab der &Ouml;ffnungszeiten dieser Station!'), 'data' => null);
            }
        }
        else // die Öffnungszeiten konnten nicht überprüft werden
        {
            return array('success' => false, 'errors' => $opend['errors'], 'data' => null);
        }
    }





   /* Sortiert das Preis-Array _nachdem_ die Preise bereits verglichen wurden.
    * Sofern eine Spritsorte nicht in der Sortierreihenfolge genannt wird, wird
    * diese am Ende des Array hinzugefügt.
    *
    * Version:  1.0.1
    * Stand:    23. Juni 2016
    *
    * Input:
    *   $station_meta   : array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => array
    *       {
    *           ['fuels']           => array
    *           {
    *               [: integer] => : string
    *           }
    *           ['reference_url']   => : string
    *           ['fuel_order']      => : string
    *       }
    *   }
    *   $prices         : array
    *   {
    *       []  => array
    *       {
    *           ['id']      => : integer
    *           ['image']   => : string
    *           ['prices']  => array
    *           {
    *               [: integer] => array
    *               {
    *                   ['value']   => : integer
    *                   ['name']    => : string
    *               }
    *               ...
    *           }
    *       }
    *       ...
    *   }
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => array
    *       {
    *           []  => array
    *           {
    *               ['id']      => : integer
    *               ['image']   => : string
    *               ['prices']  => array
    *               {
    *                   [: string] => array     // ACHTUNG: Ab hier enthält die ID, die der Array Key ist, ein Leerzeichen am Ende! Dieses verhindert das Browser/Javascript das Array wieder nummerisch sortieren..
    *                   {
    *                       ['value']   => : integer
    *                       ['name']    => : string
    *                   }
    *                   ...
    *               }
    *           }
    *           ...
    *       }
    *   }
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function order_price_array($station_meta, $prices)
    {
        if(is_array($prices) && count($prices) > 0 && isset($station_meta['fuel_order'])) // nur wenn eine Reihenfolge definiert wurde
        {
            ksort($station_meta['fuel_order']);

            foreach($prices as $ref_id => $reference) // für jede vorhandene Referenz
            {
                $array = array();

                foreach($station_meta['fuel_order'] as $fuel) // für jede Sorte in der Reihenfolge
                {
                    if(isset($reference['prices'][$fuel])) // wenn die zugehörige Sorte auch im Preis-Array vorhanden ist
                    {
                        // Preis dem neuen Array hinzufügen
                        $array['#'.$fuel] = $reference['prices'][$fuel]; // Achtung: Ab hier enthält die ID, die der Array Key ist, eine Raute am Anfang! Dieses verhindert das Browser/Javascript das Array wieder nummerisch sortieren..
                        unset($reference['prices'][$fuel]); // Preis aus dem Preis-Array löschen, da bereits verarbeitet
                    }
                }

                if(count($reference['prices']) > 0) // falls nach dem Sortieren noch Preise übrig sind
                {
                    foreach($reference['prices'] as $index => $data) // für jeden der übrigen Preise
                    {
                        $array['#'.$index] = $data; // dem neuem Array anhängen
                    }
                }

                $prices[$ref_id]['prices'] = $array; // altes Preis-Array mit dem neuen Array überschreiben
            }
        }

        return array('success' => true, 'errors' => null, 'data' => $prices);
    }





   /* Lädt die für eine Station definierten "fuel_workaround" um diese
    * auf die _bereits verglichenen Preise_ anzuwenden.
    *
    * Version:  1.0.1
    * Stand:    23. Juni 2016
    *
    * Input:
    *   $station_id : integer
    *   $prices     : array
    *   {
    *       []  => array
    *       {
    *           ['id']      => : integer
    *           ['image']   => : string
    *           ['prices']  => array
    *           {
    *               [: integer] => array
    *               {
    *                   ['value']   => : integer
    *                   ['name']    => : string
    *               }
    *               ...
    *           }
    *       }
    *       ...
    *   }
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => array
    *       {
    *           []  => array
    *           {
    *               ['id']      => : integer
    *               ['image']   => : string
    *               ['prices']  => array
    *               {
    *                   [: integer] => array
    *                   {
    *                       ['value']   => : integer
    *                       ['name']    => : string
    *                   }
    *                   ...
    *               }
    *           }
    *           ...
    *       }
    *   }
    *
    * Failure:
    *   none
    *
    * Errors:
    *   passed  -> db_connect();
    */
    function apply_fuel_workarounds($station_id, $prices)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        if(is_array($prices) && count($prices) > 0)
        {
            $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

            if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
            {
                // Workaround laden
                $sql = $db->query('select sfj.fuel_id, f.name, sfj.workaround_source, sfj.workaround_adjustment from fuels as f, station_fuel_join as sfj where sfj.station_id = '.mysqli_real_escape_string($db, $station_id).' && sfj.fuel_id = f.id');

                if($sql->num_rows > 0) // nur wenn auch Workarounds für die Station definiert wurden
                {
                    while($row = $sql->fetch_assoc()) // für jeden Workaround
                    {
                        foreach($prices as $ref_id => $reference) // für jede Referenz im Preis-Array
                        {
                            if(isset($reference['prices'][$row['workaround_source']])) // nur wenn auch die Referenz-Sorte vorhanden ist
                            {
                                // Key2     = ID der Spritsorte
                                // Value1   = ID der Spritsorte die als Referenz verwendet wird
                                // Value2   = Anzahl Cents die auf die Referenz aufgeschlagen wird um den Preis zu erhalten

                                // Preis abspeichern
                                $prices[$ref_id]['prices'][$row['fuel_id']] = array('value' => $reference['prices'][$row['workaround_source']]['value'] + $row['workaround_adjustment'], 'name' => $row['name']);
                            }
                        }
                    }
                }

                return array('success' => true, 'errors' => null, 'data' => $prices);
            }
            else // Verbindung zur Datenbank ist fehlgeschlagen
            {
                return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
            }
        }
        else // es wurden keine Preise übergeben
        {
            return array('success' => true, 'errors' => null, 'data' => $prices);
        }
    }





   /* Vergleicht die Preise der eigenen Station mit denen der Referenz-Stationen.
    * Werden billigere Preise gefunden wird die Referenz im Rückgabe-Array,
    * zusammen mit den jeweiligen Preisen gespeichert.
    *
    * Version:  1.3.1
    * Stand:    23. Juni 2016
    *
    * Input
    *   station_prices  : array
    *   {
    *       [: integer] => array
    *       {
    *           ['value']   => : integer
    *           ['name']    => : string
    *       }
    *       ...
    *   }
    *   reference_meta  : array
    *   {
    *       []  => array
    *       {
    *           ['id']          => : integer
    *           ['image']       => : string
    *           ['identifier']	=> : integer
    *           ['prices']      => array
    *           {
    *               [: integer] => array
    *               {
    *                   ['value']   => : integer
    *                   ['name']    => : string
    *               }
    *               ...
    *           }
    *       }
    *       ...
    *   }
    *   fuels_correction	: array
    *   {
    *   	[: integer] 	=> : string
    *   	...
    *   }
    *
    * Success:
    *   array
    *   {
    *       ['success'] => : boolean
    *       ['errors']  => null : null
    *       ['data']    => array
    *       {
    *           []  => array
    *           {
    *               ['id']      	=> : integer
    *               ['image']   	=> : string
    *               ['identifier']	=> : integer
    *               ['prices'] 		=> array
    *               {
    *                   [: integer] => array
    *                   {
    *                       ['value']   => : integer
    *                       ['name']    => : string
    *                   }
    *                   ...
    *               }
    *           }
    *           ...
    *       }
    *   }
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function compare_prices($station_prices, $reference_meta)
    {
        $data = array();
        $used_fuels = array(); // speichert die IDs der im Data-Array verwendeten Spritsorten
        $adjustments = array(); // Zwischenspeicher für anzuwendende Korrekturen

        foreach($reference_meta as $reference) // für alle Referenzen
        {
            $prices = array(); // enthält später die Preise die zurückgegeben werden sollen
            $adjustments[$reference['id']] = array(); // enthält später für jede vorkommende Spritsorte einen zugehörigen Korrekturwert

            foreach($station_prices as $fuel_id => $fuel_meta) // für jede Spritsorte der Station
            {
                // wenn die aktuelle Sorte auch bei der Referenz existiert & ein Preis ermittelt werden konnte
                if(isset($reference['prices'][$fuel_id]) && $reference['prices'][$fuel_id] != null)
                {
                    if(isset($reference['adjustments'][null])) // falls eine Korrektur für die komplette Referenz definiert wurde
                    {
                        $adjustment_value = $reference['adjustments'][null];
                    }
                    else
                    {
                        if(isset($reference['adjustments'][$fuel_id])) // falls eine Korrektur für die aktuelle Spritsorte definiert wurde
                        {
                            $adjustment_value = $reference['adjustments'][$fuel_id];
                        }
                        else // es gibt keine Korrektur
                        {
                            $adjustment_value = 0;
                        }
                    }


                    if($fuel_meta['value'] > $reference['prices'][$fuel_id]['value'] + $adjustment_value) // Preise vergleichen; dabei Preise der Referenz um Korrekturwert anpassen
                    {
                        $prices[$fuel_id] = $reference['prices'][$fuel_id]; // Preis speichern, da billiger als bei Station
                        $adjustments[$reference['id']][$fuel_id] = $adjustment_value; // Korrekturwert speichern; wird für den Vergleich zwischen Referenzen noch benötigt
                        $used_fuels[] = $fuel_id; // ID der Spritsorte als "verwendet" speichern
                    }
                }
            }

            if(count($prices) > 0) // nur wenn mindestens eine Sorte billiger ist
            {
                // Referenz in Rückgabe-Array speichern
                $data[] = array('id' => $reference['id'], 'image' => $reference['image'], 'identifier' => $reference['identifier'], 'prices' => $prices);
            }
        }

        if(count($data) > 0) // nur wenn überhaupt Referenzen zum Zurückgeben gespeichert wurden
        {
        	if(count($data) > 1) // falls mehr als eine Referenz zurückgegeben werden soll, werden diese untereinander ebenfalls verglichen
        	{
	        	foreach(array_unique($used_fuels) as $fuel_id) // doppelte Values herausfiltern - für verwendete Spritsorte ein Loop
				{
					$cheapest_id = null; // die ID der Referenz, mit dem billigstem Preis, für diese Spritsorte
					$cheapest_price = PHP_INT_MAX; // der bisher billigste Preis, für die aktuelle Spritsorte

					foreach ($data as $reference_id => $reference) // für jede vorhandene Referenz
					{
						if(isset($reference['prices'][$fuel_id]['value'])) // sofern die Spritsorte überhaupt in dieser Referenz vorhanden ist
						{
							if(($reference['prices'][$fuel_id]['value'] + $adjustments[$reference['id']][$fuel_id]) < $cheapest_price) // falls diese Referenz einen billigeren Preis besitzt
							{
								// ID und Preis der aktuellen Referenz als am billigsten speichern
								$cheapest_id = $reference_id;
								$cheapest_price = $reference['prices'][$fuel_id]['value'] + $adjustments[$reference['id']][$fuel_id];

								foreach ($data as $backwards_reference_id => $backwards_reference) // für alle bereits verarbeiteten Referenzen
								{
									if($backwards_reference_id != $cheapest_id) // bis die aktuelle Referenz weider erreicht wird
									{
										if(isset($backwards_reference['prices'][$fuel_id]['value'])) // falls aktuelle Spritsorte in einer vorherigen Referenz vorhanden war
										{
											unset($data[$backwards_reference_id]['prices'][$fuel_id]); // Preis für die aktuelle Spritsorte aus einer vorherigen Referenz entfernen
										}
									}
									else
									{
										break; // aktuelle Referenz wurde erreicht
									}
								}
							}
							else // Referenz ist nicht billiger
							{
								if(($reference['prices'][$fuel_id]['value'] + $adjustments[$reference['id']][$fuel_id]) == $cheapest_price) // falls die Referenz den selben Preis hat, wie die billigste Referenz
								{
									// hier wird versucht die Preise möglichst kompakt in einer Referenz zu halten
									// und diese nicht unnötig zu verteilen. Hat z.B. Jet D-105 und Hessol D-105, S-139
									// wird Hessol bevorzugt, da es noch weitere Preise beinhaltet und die Jet
									// Referenz somit sogar komplett entfernt werden kann.

									if(count($data[$reference_id]['prices']) > count($data[$cheapest_id]['prices'])) // wenn die aktuelle Referenz mehr Spritsorten übrig hat, als die des billigsten Preises
									{
										unset($data[$cheapest_id]['prices'][$fuel_id]); // Preis für die aktuelle Spritsorte aus der bisher billigeren Referenz löschen

										if(count($data[$cheapest_id]['prices']) == 0) // falls die bisher billigere Referenz keine Spritsorten mehr beinhaltet
										{
											unset($data[$cheapest_id]); // komplette Referenz aus den Rückgabe-Daten entfernen
										}

										// ID und Preis der aktuellen Referenz als am billigsten speichern
										$cheapest_id = $reference_id;
										$cheapest_price = $reference['prices'][$fuel_id]['value'] + $adjustments[$reference['id']][$fuel_id];
									}
									else
									{
										unset($data[$reference_id]['prices'][$fuel_id]); // Preis für die aktuelle Spritsorte aus der aktuellen Referenz löschen
									}
								}
								else // die Referenz ist offenbar teurer
								{
									unset($data[$reference_id]['prices'][$fuel_id]); // Preis für die aktuelle Spritsorte aus der aktuellen Referenz löschen
								}
							}
						}
					}
				}

				foreach ($data as $reference_id => $reference) // für jede vorhandene Referenz prüfen ob diese noch Spritsorten beinhalten
				{
					if(count($data[$reference_id]['prices']) == 0) // falls die aktuelle Referenz keine Spritsorten mehr beinhaltet
					{
						unset($data[$reference_id]); // komplette Referenz aus den Rückgabe-Daten entfernen
					}
				}


				$data = array_values($data); // Array neu indexieren, so dass die Indexe wieder fortlaufend sind (falls z.B. die Referenz unter Index #0 entfernt wird)
			}

            return array('success' => true, 'errors' => null, 'data' => $data);
        }
        else // keine Referenzen vorhanden, die zurückgegeben werden könnten
        {
            return array('success' => true, 'errors' => null, 'data' => null); // keine neuen Preise verfügbar
        }
    }





   /* Lädt alle relevanten Informationen der Referenzen die mit der angegebenen
    * Station verbunden sind (max 2 Stück). Hierbei werden auch ggf. neue Preise
    * geladen oder die bereits vorhandenen Preise aus der Datenbank verwendet.
    * Sollten neue Preise abgefragt werden, werden zudem die Preise in der
    * Datenbank aktualisiert.
    *
    * Version:  1.0.6
    * Stand:    23. Juni 2016
    *
    * Input:
    *   $station_id : integer   = die ID der Station
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null / array{[string] => : string}	// es werden ggf. Warnungen zurückgegeben!
    *       ['data']    => array
    *       {
    *           []  => array
    *           {
    *               ['id']          => : integer
    *               ['image']       => : string
    *               ['identifier']	=> : integer
    *               ['timestamp']   => : integer
    *               ['url']         => : string
    *               ['prices']      => array
    *               {
    *                   [: integer] => array
    *                   {
    *                       ['value']   => : integer
    *                       ['name']    => : string
    *                   }
    *                   ...
    *               }
    *           }
    *           ...
    *       }
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  ->  db_connect()
    *   passed  ->  fetch_fuel_prices()
    *   2e011   =   "Für die Station wurden keine Referenz-Tankstellen definiert!"
    *   2e012   =   "Es wurden keine Spritsorten definiert!"
    *   2w019	= 	"Die gespeicherten Preise einer Referenz konnten nicht aktualisiert werden!"
    *   2w020	=	"Der Price-Timestamp einer Referenz konnte nicht aktualisiert werden!"
    */
    function fetch_station_references($station_id)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            // Referenz Details laden (ID, Name, Image, Timestamp, Anzahl der gespeicherten Preise, URL)
            $sql = $db->query('select r.id, rb.image, r.timestamp, srj.identifier, (select count(fuel_id) from reference_prices as rp where rp.reference_id = r.id) as amount, r.url from `references` as r, reference_brands as rb, station_reference_join as srj where srj.station_id = '.mysqli_real_escape_string($db, $station_id).' && srj.reference_id = r.id && r.brand = rb.id limit '.MAX_REFERENCE_AMOUNT);

            if($sql->num_rows > 0) // nur wenn die Daten geladen werden konnten
            {
                $data = array();
                $errors = array(); // enthält ggf. Errors / Warnings für das Rückgabe-Array

                while($row = $sql->fetch_assoc()) // für jede vorhandene Referenz (max. 2 Stück!)
                {
                    // die Grundinformationen im Rückgabe-Array speichern
                    $data[] = array('id' => $row['id'], 'image' => $row['image'], 'identifier' => $row['identifier'], 'timestamp' => $row['timestamp'], 'url' => $row['url'], 'prices' => array(), 'adjustments' => array());

                    if($row['amount'] > 0 && $row['timestamp'] + DATABASE_REFERENCE_PRICE_DURABILITY > time()) // überprüfen ob die Preise in der Datenbank noch aktuell sind & ob auch Preise gespeichert wurden
                    {
                        $sql_prices = $db->query('select rp.fuel_id, f.name, rp.value from reference_prices as rp, fuels as f where rp.reference_id = '.$row['id'].' and rp.fuel_id = f.id'); // Preise (inkl. ID und Namen) aus der Datenbank auslesen

                        while($row_prices = $sql_prices->fetch_assoc()) // für jede Spritsorte die ausgelesen wurde
                        {
                            $data[count($data) - 1]['prices'][$row_prices['fuel_id']] = array('value' => $row_prices['value'], 'name' => $row_prices['name']); // Spritsorte => Preis im Rückgabe-Array abspeichern
                        }
                    }
                    else // Preise sind bereits zu alt
                    {
                        $sql_fuels = $db->query('select id, name from fuels'); // alle vorhandenen Spritsorten auslesen
                        $fuels = array();

                        if($sql_fuels->num_rows > 0) // nur wenn überhaupt Spritsorten vorhanden sind (sollte theoretisch immer der Fall sein)
                        {
                            while($row_fuels = $sql_fuels->fetch_assoc()) // Sorten in Array speichern
                            {
                                $fuels[$row_fuels['id']] = $row_fuels['name'];
                            }

                            $reference_prices = fetch_fuel_prices($row['url'], $fuels); // Preise für diese Referenz neu abfragen

                            if($reference_prices['success']) // wenn die Abfrage erfolgreich war
                            {
                                $data[count($data) - 1]['prices'] = $reference_prices['data']; // die neuen Preise im Rückgabe-Array abspeichern
                                $data[count($data) - 1]['timestamp'] = time(); // den Timestamp neu setzten

                                // -- Preise in der Datenbank, für diese Referenz, aktualisieren --

                                $values = array(); // enthält später alle Values für den SQL String
                                foreach($reference_prices['data'] as $fuel_id => $fuel_meta) // für jede ermittelte Spritsorte
                                {
                                	if(!is_null($fuel_meta['value'])) // nur wenn für die Sorte ein Preis gefunden wurde
                                	{
                                		$values[] = '('.$row['id'].', '.$fuel_id.', '.$fuel_meta['value'].')'; // String im Array speichern
                                	}
                                }

                                if($db->query('insert into reference_prices (reference_id, fuel_id, value) values '.implode(',', $values).' on duplicate key update value = values(value)')) // Preise aktualisieren
                            	{
                            		if(!$db->query('update `references` set `timestamp` = '.$data[count($data) - 1]['timestamp'].' where id = '.$row['id'])) // wenn Timestamp der Referenz nicht aktualisiert werden konnte
                            		{
                            			$errors['2w020'] = 'Der Price-Timestamp einer Referenz konnte nicht aktualisiert werden!';
                            		}
                                }
                                else // Preise konnten nicht aktualisiert werden
                                {
                                	$errors['2w019'] = 'Die gespeicherten Preise einer Referenz konnten nicht aktualisiert werden!';
                                }
                            }
                            else
                            {
								// Hinweis: die hier angehängten Fehlermeldungen werden nicht an den Client übergeben, sollte die Funktion mit Success->True enden
								$errors = array_merge($errors, $reference_prices['errors']);
                            }
                        }
                        else // es wurden keine Spritsorten definiert
                        {
                            return array('success' => false, 'errors' => array('2e012' => 'Es wurden keine Spritsorten definiert!'), 'data' => null);
                        }
                    }


                    // Korrekturwerte für diese Referenz laden
                    $sql_ref_adjust = $db->query('select fuel_id, adjustment from station_reference_fuel_adjustments where station_id = '.mysqli_real_escape_string($db, $station_id).' && reference_id = '.$row['id']);

                    if($sql_ref_adjust->num_rows > 0) // nur falls Korrekturwerte definiert wurden
                    {
                    	while($row_ref_adjust = $sql_ref_adjust->fetch_object())
                    	{
                    		$data[count($data) - 1]['adjustments'][$row_ref_adjust->fuel_id] = $row_ref_adjust->adjustment;
                    	}

                    	if(isset($data[count($data) - 1]['adjustments'][null])) // sollte ein Korrekturwert ohne Fuel-ID existieren (=global für Referenz)
                    	{
                    		foreach ($data[count($data) - 1]['adjustments'] as $key => $value) // alle spezifischen Korrekturwerte, die ggf. auch geladen wurden, wieder verwerfen
                    		{
                    			if($key != null)
                    			{
                    				unset($data[count($data) - 1]['adjustments'][$key]);
                    			}
                    		}
                    	}
                    }
                    else
                    {
                    	$data[count($data) - 1]['adjustments'] = null; // es existieren keine Korrekturwerte für diese Referenz
                    }
                }

                return array('success' => true, 'errors' => (count($errors) > 0 ? $errors : null), 'data' => $data);
            }
            else // Referenz-Daten konnte nicht geladen werden
            {
                return array('success' => false, 'errors' => array('2e011' => 'F&uuml;r die Station wurden keine Referenz-Tankstellen definiert!'), 'data' => null);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }





   /* Lädt den Quellcode der angegebenen Quelle und sucht in diesem nach
    * den übergebenen Spritsorten und deren Preisen.
    *
    * Version:  1.0.1
    * Stand:    12. September 2015
    *
    * Input:
    *   source  : string    = der URL zur Source
    *   fuels   : array     = enthält die Namen der Spritsorten die gesucht werden sollen
    *   {
    *       [: integer] => : string     = Zuweisung ist ID => Name
    *   }
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => array
    *       {
    *           [: integer] => array 					// Key = ID der Spritsorte
    *           {
    *           	['value]	=> : integer / :null    // 1,16 € = 116 oder NULL wenn Spritsorte nicht im Quellcode gefunden wurde!
    *           	['name']	=> : string 			// Name der Spritsorte
    *           }
    *       }
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   2e008   =   "Quellcode der Referenzseite konnte nicht geladen werden!"
    *   2w009   =   "Die geladenen Preise sind veraltet!"
    *   2e010   =   "Preis Informationen konnten im geladenen Quellcode nicht gefunden werden!"
    */
    function fetch_fuel_prices($source, $fuels)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');

        $return = array(); // wird später zurückgegeben
        $header = stream_context_create(array('http'=>array('user_agent'=>"User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11\r\n"))); // wird benötigt um den Quellcode auslesen zu können - Sparsamtanken.de hat eine Client-Only Restriction eingebaut!
        $website_source = @file_get_contents($source, false, $header); // Quellcode der Ziel-Webseite auslesen

        if($website_source !== false) // nur wenn der Quellcode ausgelesen werden konnte
        {
            foreach($fuels as $fuel_id => $fuel_name) // für jede angeforderte Spritsorte
            {
                preg_match('/'.$fuel_name.'<\/a>:<\/td>(.*?)9 (.*?)vom(.*?)<\/td>/si', $website_source, $matches); // Preis inkl. Datum & Uhrzeit ermitteln

                if(isset($matches[1])) // nur wenn die Spritsorte inkl. Preis auch gefunden wurde
                {
                    // Zeitangeben voneinander trennen
                    $data_timeinfo = preg_split('/ /', $matches[3]);
                    $data_date = preg_split('/\./', $data_timeinfo[1]);
                    $data_time = preg_split('/:/', $data_timeinfo[2]);

                    if(mktime($data_time[0], $data_time[1], 0, $data_date[1], $data_date[0], $data_date[2]) >= time() - REFERENCE_DATA_TIMEOUT) // nur wenn die Preise noch nicht zu alt sind
                    {
                            $price = preg_replace('/[^0-9]/', null, $matches[1]); // entfernt Leerzeichen und Kommas aus dem Preis
                            $return[$fuel_id] = array('value' => intval($price), 'name' => $fuel_name); // speichert den Preis
                    }
                    else
                    {
                       //trigger_error('fetched fuel information are outdated!');
                       return array('success' => false, 'errors' => array('2w009' => 'Die geladenen Preise sind veraltet!'), 'data' => null); // Preisinformationen sind zu alt - Abbruch! (wenn einer aus x Preisen outdated ist)
                    }
                }
                else
                {
                    $return[$fuel_id] = null; // Spritsorte wird durch "NULL" als nicht gefunden markiert
                }
            }

            if(count($return) > 0)
            {
                return array('success' => true, 'errors' => null, 'data' => $return);
            }
            else
            {
                //trigger_error('couldn\'t find fuel information in external source code!');
                //**TODO** Fehlermeldung hat keinen Effekt, da das IF immer TRUE sein wird - zudem wird min. ein NULL enthalten sein (wegen Super Plus)
                return array('success' => false, 'errors' => array('2e010' => 'Preis Informationen konnten im geladenen Quellcode nicht gefunden werden!'), 'data' => null); // Preisinformationen wurde nicht im Quellcode gefunden - Abbruch!
            }
        }
        else // Quellcode konnte nicht ausgelesen werden
        {
            //trigger_error('couldn\'t fetch fuel information from external source!');
            return array('success' => false, 'errors' => array('2e008' => 'Quellcode der Referenzseite konnte nicht geladen werden!'), 'data' => null); // Quellcode konnte nicht ausgelesen werden - Abbruch!
        }
    }





   /* Lädt hauptsächlich den Referenz-URL der eigenen Station und die
    * definierten Spritsorten. Zudem werden ggf. weitere Einstellungen
    * geladen, die später noch benötigt werden.
    *
    * Version:  1.1.1
    * Stand:    23. Juni 2016
    *
    * Input:
    *   $station_id : integer   = die ID der Station
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => array
    *       {
    *           ['fuels']           => array
    *           {
    *               [: integer] => : string
    *           }
    *           ['reference_url']   => : string
    *           ['fuel_order']      => : string
    *           [fuels_correction]	=> array
    *           {
    *           	[: integer] 	=> : string
    *           }
    *       }
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  ->  db_connect()
    *   2e005   =   "Es wurde kein Referenz-URL für diese Station definiert!"
    *   2e006   =   "Preise wurden nicht überprüft, da die Station zurzeit deaktiviert ist!"
    *   2e007   =   "Die Preise konnten nicht überprüft werden, da keine Spirtsorten definiert wurden!"
    */
    function fetch_station_meta($station_id)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            // Referenz-URL und ggf. zusätzliche Einstellungen laden
            $sql = $db->query('select s.active, sp.key1, sp.value1 from station_properties as sp, stations as s where s.id = '.mysqli_real_escape_string($db, $station_id).' && sp.key1 in("reference_url") && s.id = sp.station_id');

            if($sql->num_rows > 0) // nur wenn zumindest der Referenz-URL gefunden wurde
            {
                $data = array('fuels' => array()); // Data-Array Struktur definieren

                while($row = $sql->fetch_assoc()) // für alle geladenen Einstellungen
                {
                    if(!$row['active']) // falls die Station jedoch gerade inaktiv ist
                    {
                        // Abbruch der Funktion!
                        return array('success' => false, 'errors' => array('2e006' => 'Preise wurden nicht &uuml;berpr&uuml;ft, da die Station zurzeit deaktiviert ist!'), 'data' => null);
                    }

                    $data[$row['key1']] = $row['value1'];
                }

                // Spritsorten der Station laden
                $sql = $db->query('select f.id, f.name, sfj.sort from station_fuel_join as sfj, fuels as f where sfj.station_id = '.mysqli_real_escape_string($db, $station_id).' && sfj.fuel_id = f.id');

                if($sql->num_rows > 0) // nur wenn auch Spritsorten definiert wurden
                {
                    while($row = $sql->fetch_assoc()) // für jede Spritsorte
                    {
                        $data['fuels'][$row['id']] = $row['name']; // im Data-Array speichern

                        if($row['sort'] != null)
                        {
                        	$data['fuel_order'][$row['sort']] = $row['id']; // Reihenfolge für diese Spritsorte speichern
                        }
                    }

                    return array('success' => true, 'errors' => null, 'data' => $data); // Funktion beenden und Data-Array zurückgeben
                }
                else
                {
                    return array('success' => false, 'errors' => array('2e007' => 'Die Preise konnten nicht &uuml;berpr&uuml;ft werden, da keine Spirtsorten definiert wurden!'), 'data' => null);
                }
            }
            else // es wurde keine Referenz definiert
            {
                return array('success' => false, 'errors' => array('2e005' => 'Es wurde kein Referenz-URL f&uuml;r diese Station definiert!'), 'data' => null);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }





   /* Lädt die Öffnungszeiten aus der Datenbank und überprüft ob die aktuelle Uhrzeit
    * innerhlab dieser liegt. Zuvor werden die Öffnungszeiten jedoch um einen global
    * oder für die Station spezifischen Wert verschoben (um z.B. Meldungen kurz vor
    * Öffnung zu ermöglichen).
    *
    * Version:  1.2.2
    * Stand:    28. Dezember 2015
    *
    * Input:
    *   station_id  : integer   = die ID der Station, deren Öffnungszeiten überprüft werden sollen
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => : boolean
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  ->  db_connect()
    */
    function check_openinghours($station_id)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
        	$db->query('delete from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' and key1 = "opening_hours_exception" and key2 < date_format(from_unixtime('.time().'), "%Y-%m-%d")'); // bereits vergangene "abweichende Öffnungszeiten" löschen
        	$sql = $db->query('select value1, value2 from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' and key1 = "opening_hours_exception" and key2 = "'.date('Y-m-d', time()).'" limit 1'); // abweichende Öffnungszeiten laden, falls vorhanden

            if($sql->num_rows == 0) // falls keine Ausnahme gefunden wurde; normale Öffnungszeiten ermitteln
            {
	            // Öffnungszeiten auslesen
	            $sql = $db->query('select value1, value2 from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' and key1 = "opening_hours" and key2 = "'.date('N', time()).'" limit 1');
	        }

            if($sql->num_rows > 0 && $row = $sql->fetch_object()) // nur wenn Öffnungszeiten gefunden wurden
            {
                if($row->value1 != null && $row->value2 != null) // falls für die Öffnungszeiten auch Uhrzeiten definiert wurden
                {
	                // Stunden und Minuten trennen und anschließend in Timestamp umwandeln
	                $opening_timestamp = mktime(preg_split('/:/', $row->value1)[0], preg_split('/:/', $row->value1)[1], 0, date('m', time()), date('d', time()), date('y', time()));
	                $closing_timestamp = mktime(preg_split('/:/', $row->value2)[0], preg_split('/:/', $row->value2)[1], 0, date('m', time()), date('d', time()), date('y', time()));

	                // Overwirte Values initialisieren
	                $opening_hour_overwrite = OPENING_HOUR_OVERWRITE;
	                $closing_hour_overwrite = CLOSING_HOUR_OVERWRITE;

	                // falls eigene Overwirte Values definiert wurden, diese auslesen
	                $sql = $db->query('select key1, value1 from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' && key1 in("opening_hour_overwrite", "closing_hour_overwrite") limit 2');

	                if($sql->num_rows > 0) // falls eigene Values gefunden wurden
	                {
	                    while($row = $sql->fetch_assoc()) // die geladenen Values durchlaufen
	                    {
	                        // bereits initialisierte Config-Values mit den eigenen Values überschreiben
	                        if($row['key1'] == 'opening_hour_overwrite')
	                        {
	                            $opening_hour_overwrite = $row['value1'];
	                        }
	                        else
	                        {
	                            $closing_hour_overwrite = $row['value1'];
	                        }
	                    }
	                }

	                // Öffnungszeiten um den definierten Wert verschieben
	                $opening_timestamp -= $opening_hour_overwrite;
	                $closing_timestamp -= $closing_hour_overwrite;


	                if($opening_timestamp < $closing_timestamp) // sollte die Öffnungszeit auch vor dem Ladenschluss liegen
	                {
		                if($opening_timestamp <= time() && $closing_timestamp >= time()) // sofern die aktuelle Zeit innerhalb der Öffnungszeiten liegt
		                {
		                    return array('success' => true, 'errors' => null, 'data' => true);
		                }
		                else
		                {
		                    return array('success' => true, 'errors' => null, 'data' => false);
		                }
	                }
	                else // Ladenschluss liegt vor der Öffnungszeit (wie im Fall von 0:00 bis 0:00)
	                {
	                	return array('success' => true, 'errors' => null, 'data' => true);
	                }
                }
                else // die Station hat geschlossen
                {
                	return array('success' => true, 'errors' => null, 'data' => false);
                }
            }
            else // für die Station wurden keine Öffnungszeiten definiert - Station wird als geöffnet betrachtet
            {
                return array('success' => true, 'errors' => null, 'data' => true);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }





   /* In den Meta-Daten jeder Station werden die aktuellen Preise jener gespeichert. Vor
   	* der Abfrage der Referenz-Preise werden die Preise der eigenen Station ermittelt und
   	* durch diese Funktion analysiert. Es wird versucht eine Preiserhöhung der eigenen
   	* Station, durch Vergleiche mit den gespeicherten Preisen, festzustellen.
   	*
   	* Version: 	1.1.0
   	* Stand: 	30. Juni 2016
   	*
   	* Input:
   	* 	$station_id 	: integer 	= die ID der Station, bei der eine Preiserhöhung ermittelt werden soll
   	* 	$prices 		: array
   	* 	{
   	* 		[: integer]	=> array 	= die ID der Spritsorte
   	* 		{
   	* 			['value']	=> : integer 	= der ermittelte Preis der Sorte (z.B. 119 für 1,19 Euro)
   	* 			['name']	=> : string
   	* 		}
   	* 		...
   	* 	}
   	*
   	* Success:
   	* 	array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => : mixed 			= wird eine Preiserhöhung erkannt, wird hier eine Meldung übergeben, zusammen mit dem End-Timestamp im Daten-Segment
    *       ['data']    => : mixed 			= gibt entweder das Preis-Array (ziehe $prices) zurück oder eine Integer-Value, falls eine Preiserhöhung erkannt wird
    *   }
    *
    * Failure:
    * 	array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors
    * 	passed 	-> db_connect();
    * 	2n014 	=> "Es wurde eine Preiserhöhung festgestellt und daher alle Preisabfragen pausiert!"
    * 	2n015	=> "Auf Grund einer kürzlichen Preiserhöhung sind die Preisabfragen noch pausiert!"
    * 	2e016	=> "Die Analyse der Stationspreise ist zu keinem Ergebnis gekommen!"
    * 	2e017	=> "Das Ergebnis der Stationsanalyse konnte nicht in der Datenbank gespeichert werden!"
    * 	2e018	=> "Auf Grund eines Datenbankfehlers konnten die erkannte Preiserhöhung nicht verarbeitet werden!"
    */
    function analyse_station_prices($station_id, $prices)
    {
    	require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
        	$properties = array('station_price_rise' => 0, 'station_price_rise_timeout_exception' => array()); // enthält später einige Einstellungen der Station
        	$sql_insert_values = array(); // Zwischenablage für alle Values die im Insert/Update verwendet werden sollen
        	$price_rise = false; // Flag das umgeschaltet wird, sollte eine Preiserhöhung erkannt werden
        	$return = array(); // Zwischenablage für Preise die später zurückgeben werden

        	$sql_properties = $db->query('select key1, key2, value1, value2 from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' and key1 like "station_price_rise%" && (key2 = "null" || key2 = '.date('N').')'); // Rise-Flag und Timeout-Exceptions laden

        	if($sql_properties->num_rows > 0) // nur wenn auch Properties geladen werden konnten
        	{
        		while($row_properties = $sql_properties->fetch_object()) // für alle geladenen Einstellungen
        		{
        			if($row_properties->key1 == 'station_price_rise') // falls das Rise-Flag geladen wurde
        			{
        				$properties[$row_properties->key1] = $row_properties->value1; // Flag speichern
        			}
        			else
        			{
        				$properties[$row_properties->key1][] = array('day' => $row_properties->key2, 'time' => $row_properties->value1, 'duration' => $row_properties->value2); // Exception-Values speichern
        			}
        		}
        	}

    		if(time() >= $properties['station_price_rise']) // nur wenn das Rise-Flag wieder abgelaufen ist
    		{
    			$sql = $db->query('select fuel_id, price, timestamp from station_fuel_join where station_id = '.mysqli_real_escape_string($db, $station_id).' && workaround_source is null'); // gespeicherten Preise auslesen
    			while($row = $sql->fetch_assoc()) // für jeden gespeicherten Preis
    			{
    				if(isset($prices[$row['fuel_id']])) // nur wenn die gespeicherte Spritsorte auch bei den ermittelten Preisen vorhanden ist
    				{
    					if($row['timestamp'] + DATABASE_STATION_PRICE_DURABILITY > time()) // wenn der Timestamp des Preises + Lebensdauer noch nicht erreicht wurde = Preis noch gültig
    					{
    						if($row['price'] < $prices[$row['fuel_id']]['value'] && !$price_rise) // wenn der ermittelte Preis größer als der gespeicherte Stationspreis ist
    						{
    							$price_rise = true; // Preiserhöhung wurde erkannt
    							$timeout = PRICE_RISE_TIMEOUT; // zunächst den Default-Timeout verwenden

    							// Timeout-Länge ermitteln
				        		if(count($properties['station_price_rise_timeout_exception']) > 0)
				        		{
				        			array_multisort($properties['station_price_rise_timeout_exception']); // Elemente mit day = "null" && time = null nach ganz oben bringen
				        			$general_timeout_defined = array_search(null, array_column($properties['station_price_rise_timeout_exception'], 'time')); // nach einer generellen Exception suchen

				        			foreach ($properties['station_price_rise_timeout_exception'] as $exception) // für alle Exceptions
				        			{
				        				if($exception['time'] != null) // falls eine Zeit angegeben ist
				        				{
				        					if($general_timeout_defined === false) // nur falls zuvor keine generelle Exception gefunden wurde
				        					{
				        						// Zeitraum in Timestamps umwandeln
						        				$exception_times = explode('-', $exception['time']);

					        					$exception_start = explode(':', $exception_times[0]);
					        					$exception_start = mktime($exception_start[0], $exception_start[1], 0);

					        					$exception_end = explode(':', $exception_times[1]);
					        					$exception_end = mktime($exception_end[0], $exception_end[1], 0);

					        					if($exception_start <= time() && time() <= $exception_end) // wenn aktuelle Zeit innerhalb des Zeitraums liegt
					        					{
					        						$timeout = intval($exception['duration']);
					        						break;
					        					}
				        					}
				        				}
			        					else // generelle Exception verwenden
			        					{
			        						$timeout = intval($exception['duration']);
			        						break;
			        					}
				        			}
				        		}
    						}
    					}

						$sql_insert_values[] = '('.mysqli_real_escape_string($db, $station_id).', '.$row['fuel_id'].', '.mysqli_real_escape_string($db, $prices[$row['fuel_id']]['value']).', '.(time() - (time() % 60)).')'; // Preis für das Update der Datenbank speichern
						$return[$row['fuel_id']] = $prices[$row['fuel_id']]; // Preis für die spätere Rückgabe speichern
    				}
    			}

    			if(!$price_rise) // sofern während dem Vergleich der Preise keine Preiserhöhung festgestellt wurde
    			{
    				$prices = $return; // ursprüngliches Preis-Array mit den zu verwendenden Preisen überschreiben
    			}
    			else // es wurde eine Preiserhöhung festgestellt
    			{
    				$sql_insert_flag = $db->query('insert into station_properties (station_id, key1, value1) values ('.mysqli_real_escape_string($db, $station_id).', "station_price_rise", '.((time() - (time() % 60)) + $timeout).') on duplicate key update station_id = values(station_id), key1 = values(key1), value1 = values(value1)'); // Rise-Flag in die Datenbank schreiben / aktualisieren
    				$sql_insert_prices = $db->query('insert into station_fuel_join (station_id, fuel_id, price, timestamp) values '.implode(',', $sql_insert_values).' on duplicate key update station_id = values(station_id), fuel_id = values(fuel_id), price = values(price), timestamp = values(timestamp)'); // Preise in der Datenbank aktualisieren

					if($sql_insert_flag && $sql_insert_prices) // wenn ale Daten gespeichert werden konnten
	        		{
	        			return array('success' => true, 'errors' => array('2n014' => 'Es wurde eine Preiserh&ouml;hung festgestellt und daher alle Preisabfragen pausiert!'), 'data' => $timeout);
	        		}
	        		else // Fehler beim Insert/Update in die Datenbank
	        		{
	        			return array('success' => false, 'errors' => array('2e018' => 'Auf Grund eines Datenbankfehlers konnten die erkannte Preiserh&ouml;hung nicht verarbeitet werden!'), 'data' => null);
	        		}
    			}
    		}
    		else // Rise-Flag ist gesetzt und der Timeout ist noch nicht abgelaufen!
    		{
    			return array('success' => true, 'errors' => array('2n015' => 'Auf Grund einer k&uuml;rzlichen Preiserh&ouml;hung sind die Preisabfragen noch pausiert!'), 'data' => ($properties['station_price_rise'] - time()));
    		}


        	// egal welcher Zweig des oberen IF verarbeitet wurde, dies ist der finale Part der Funktion, wo die Preise der
        	// Datenbank aktualisiert werden und die zu verwendenden Preise zurückgegeben werden. Dieser Teil der Funktion
        	// wird nicht erreicht, sollte eine Preiserhöhung bzw. ein aktiver Rise-Timeout festgestellt werden.

        	if(count($sql_insert_values) > 0)
        	{
	        	if($db->query('insert into station_fuel_join (station_id, fuel_id, price, timestamp) values '.implode(',', $sql_insert_values).' on duplicate key update station_id = values(station_id), fuel_id = values(fuel_id), price = values(price), timestamp = values(timestamp)'))
	    		{
	    			return array('success' => true, 'errors' => null, 'data' => $prices);
	    		}
	    		else // Fehler beim Insert/Update in die Datenbank
	    		{
	    			return array('success' => false, 'errors' => array('2e017' => 'Das Ergebnis der Stationsanalyse konnte nicht in der Datenbank gespeichert werden!'), 'data' => null);
	    		}
    		}
    		else // keine Preise zum Inserten/Updaten.. sollte niemals eintreten
    		{
    			return array('success' => false, 'errors' => array('2e016' => 'Die Analyse der Stationspreise ist zu keinem Ergebnis gekommen!'), 'data' => null);
    		}
    	}
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }





   /* In dieser Funktion wird festgestellt wie oft die gleichen Preise an eine Station übermittelt werden.
   	* Daraus kann geschlossen werden, dass die gemeldeten Preise von der Station nicht eingegeben wurden.
    * Für jede Station kann hierfür eine gewisse Tolleranz definiert werden, bevor eine Benachrichtigung
    * and die Station gesendet wird (via Mail).
    *
    * Version:	1.0.2
    * Stand:	10. Januar 2016
    *
    * Input:
    * 	$station_id 	: integer 	= die ID der betroffenen Station
    * 	$prices 		: array 	= die gemeldeten Preise
    *   {
    *   	[]  => array
    *       {
    *       	['id']      => : integer
    *           ['image']   => : string
    *           ['prices']  => array
    *           {
    *           	[: string] => array
    *               {
    *               	['value']   => : integer
    *                   ['name']    => : string
    *               }
    *               ...
    *           }
    *       }
    *       ...
    *   }
    *
    * Success:
    * 	array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => 		: null
    *       ['data']    => 		: mixed 	= {true: keine Versäumnis, false: Versäumnis festgestellt, "off": für Station deaktiviert, "premature": verfrühter Aufruf, "undefined": Flag nicht für Station gesetzt, null: keine Preise übergeben}
    *   }
    *
    * Failure:
    * 	array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    * 	passed 	-> db_connect();
    * 	2e021 	=> "Auf Grund eines Datenbankfehlers konnten die Versäumnis-Daten nicht gespeichert werden!"
    * 	2e022 	=> "Auf Grund eines Datenbankfehlers konnten die Versäumnis-Daten nicht gespeichert werden!"
    */
    function dereliction_lookup($station_id, $prices)
    {
    	if($prices != null) // nur wenn überhaupt Preise zum Auswerten übergeben wurden
    	{
	    	require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
	        require_once(dirname(__FILE__).'/mysql.php');

	        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

	        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
	        {
	        	$sql = $db->query('select value1 from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' and key1 = "dereliction_alert"'); // Dereliction-Flag auslesen

	        	if($sql->num_rows > 0 && $row = $sql->fetch_object()) // sofern das Flag für die Station gefunden wurde
	        	{
	        		if($row->value1 > 0) // nur wenn die Überprüfung für diese Station aktiviert ist
	        		{
	        			$sql_meta = $db->query('select value1 from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' and key1 = "dereliction_alert_meta"'); // Dereliction-Daten auslesen

	        			if($sql_meta->num_rows > 0 && $row_meta = $sql_meta->fetch_object()) // sofern Daten für die Station vorhanden sind
	        			{
	        				$meta = json_decode($row_meta->value1, true); // geladene Daten wieder in Array umwandeln
	        				$return = true; // RÜckgabe-Zustand vordefinieren

							if($meta['last_time'] + RUNTIME_TIMEOUT - 60 <= (time() - (time() % 60))) // nur wenn die Funktion nach Ablauf des Runtime-Timeouts wieder aufgerufen wurde (fängt somit Client Reloads ab)
							{
		        				if($meta['last_time'] + DERELICTION_DATA_DURABILITY >= (time() - (time() % 60))) // sofern die Daten noch als aktuell betrachtet werden können
		        				{
		        					$mismatches = 0; // Zähler für die Unterschiede zwischen den gespeicherten und den übergebenen Preisen

		        					foreach($prices as $reference_id => $reference_meta) // für jede übergebene Referenz
		        					{
		        						foreach($reference_meta['prices'] as $price_id => $price_meta) // für jede Spritsorte dieser Referenz
		        						{
		        							if(isset($meta['data'][$reference_id]) && isset($meta['data'][$reference_id]['prices'][$price_id]))
		        							{
		        								$mismatches += count(array_diff($price_meta, $meta['data'][$reference_id]['prices'][$price_id])); // Unterschiede zwischen dieser und gespeicherter Spritsorte zählen und dem Counter hinzufügen
		        							}
		        						}
		        					}

		        					if($mismatches == 0) // sofern sich die Arrays nich unterscheiden
		        					{
		        						if($meta['count'] + 1 > $row->value1) // falls der Counter größer der maximalen Anzahl für diese Station ist
		        						{
		        							dereliction_alert($station_id, $prices, $meta); // Versäumnisbenachrichtigung senden
		        							$data = json_encode(array('count' => 1, 'first_time' => (time() - (time() % 60)), 'last_time' => (time() - (time() % 60)), 'data' => $prices)); // Datem für die Datenbank vorbereiten (neue Timestamps, Conter wieder auf 1)
		        							$return = false; // Rückgabe-Zustand neu setzten (False = Dereliction Alert ausgelöst)
		        						}
		        						else // Counter hat die maximale Anzahl noch nicht überschritten
		        						{
		        							$data = json_encode(array('count' => $meta['count'] + 1, 'first_time' => $meta['first_time'], 'last_time' => (time() - (time() % 60)), 'data' => $prices)); // Daten für die Datenbank vorbereiten (Counter erhöht)
		        						}
		        					}
		        					else // die Arrays unterscheiden sich
		        					{
		        						$data = json_encode(array('count' => 1, 'first_time' => (time() - (time() % 60)), 'last_time' => (time() - (time() % 60)), 'data' => $prices)); // Daten für die Datenbank vorbereiten (neuer Counter, neue Timestamps)
		        					}
		        				}
		        				else // Daten sind abgelaufen -> neu einfügen
		        				{
		        					$data = json_encode(array('count' => 1, 'first_time' => (time() - (time() % 60)), 'last_time' => (time() - (time() % 60)), 'data' => $prices)); // Daten für die Datenbank vorbereiten (neuer Counter, neue Timestamps)
		        				}


		        				if($db->query('update station_properties set value1 = "'.mysqli_real_escape_string($db, $data).'" where station_id = '.$station_id.' and key1 = "dereliction_alert_meta"')) // die neuen Daten in die Datenbank schreiben
				        		{
				        			return array('success' => true, 'errors' => null, 'data' => $return);
				        		}
				        		else // Fehler beim Insert/Update in die Datenbank
				        		{
				        			return array('success' => false, 'errors' => array('2e022' => 'Auf Grund eines Datenbankfehlers konnten die Vers&auml;umnis-Daten nicht gespeichert werden!'), 'data' => null);
				        		}
				        	}
				        	else // falls die Funktion zufrüh wieder aufgerufen wird
				        	{
				        		return array('success' => true, 'errors' => null, 'data' => 'premature');
				        	}
	        			}
	        			else // es sind bisher keine Daten vorhanden -> einfügen
	        			{
	        				if($db->query('insert into station_properties (station_id, key1, value1) values ('.$station_id.', "dereliction_alert_meta", "'.mysqli_real_escape_string($db, json_encode(array('count' => 1, 'first_time' => (time() - (time() % 60)), 'last_time' => (time() - (time() % 60)), 'data' => $prices))).'") on duplicate key update value1 = values(value1)')) // neue Dereliction-Daten in die Datenbank einfügen
			        		{
			        			return array('success' => true, 'errors' => null, 'data' => true);
			        		}
			        		else // Fehler beim Insert/Update in die Datenbank
			        		{
			        			return array('success' => false, 'errors' => array('2e021' => 'Auf Grund eines Datenbankfehlers konnten die Vers&auml;umnis-Daten nicht gespeichert werden!'), 'data' => null);
			        		}
	        			}
	        		}
	        		else // die Überprüfung ist für die Station zur zeit deaktiviert
	        		{
	        			return array('success' => true, 'errors' => null, 'data' => 'off');
	        		}
	        	}
	        	else // das Flag wurde nicht gefunden
	        	{
	        		return array('success' => true, 'errors' => null, 'data' => 'undefined');
	        	}
	    	}
	        else // Verbindung zur Datenbank ist fehlgeschlagen
	        {
	            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
	        }
	    }
	    else // keine Preise zum Verarbeiten vorhanden
	    {
	    	return array('success' => true, 'errors' => null, 'data' => null);
	    }
    }





   /* Wird diese Funktion aufgerufen, soll an die angegebene Station eine Versäumnismeldung gesendet werden,
   	* da offenbar mehrmals die selben Preise an die Station gesendet wurden. Hier wird die E-Mail
   	* zusammengebaut und anschließend versendet.
   	*
   	* Version: 	1.1.1
   	* Stand: 	04. Juli 2016
   	*
   	* Input
   	* 	$station_id 	: integer 	= die ID der betroffenen Station
   	* 	$prices 		: array 	= die gemeldeten Preise
    *   {
    *   	[]  => array
    *       {
    *       	['id']      => : integer
    *           ['image']   => : string
    *           ['prices']  => : array
    *           {
    *           	[: string] => array
    *               {
    *               	['value']   => : integer
    *                   ['name']    => : string
    *               }
    *               ...
    *           }
    *       }
    *       ...
    *   }
    *   $meta 			: array 	= die Dereliction-Meta Daten aus der Datenbank
    *   {
    *   	['count'] 		=> : integer 	= wie oft die Preise bereits an die Station gesendet wurde
    *   	['first_time']	=> : integer 	= Timestamp; wann die Preise das erste Mal gesendet wurden
    *   	['last_time']	=> : integer 	= Timestamp; wann die Preise zuletzt gesendet wurden
    *   	['data']		=> : array
    *   	{
    *    		[]  => array
    *       	{
    *       		['id']      => : integer
    *           	['image']   => : string
    *            	['prices']  => : array
    *             	{
    *              		[: string] => array
    *                	{
    *               	 	['value']   => : integer
    *                   	['name']    => : string
    *                   }
    *                   ...
    *               }
    *           }
    *           ...
    *   	}
    *   }
    *
    * Success:
    * 	array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => 		: null
    *       ['data']    => true : boolean
    *   }
    *
    * Failure:
    * 	array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => false    : boolean
    *   }
    *
    * Errors
    * 	passed 	-> db_connect();
    * 	2e023 	=> "Die Versäumnismeldung konnte nicht gesendet werden, da die Stationsdaten nicht geladen werden konnten!"
    * 	2e024 	=> "Die Versäumnismeldung konnte nicht gesendet werden!"
   	*/
    function dereliction_alert($station_id, $prices, $meta)
    {
    	require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
    	require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
        	$sql = $db->query('select s.name, t.title, t.last_name, t.mail from stations as s, tenants as t where s.id = '.mysqli_real_escape_string($db, $station_id).' and t.id = s.tenant_id'); // Daten des Pächters auslesen

        	if($sql->num_rows > 0 && $row = $sql->fetch_object()) // nur wenn die Daten des Pächters geladen werden konnten
        	{
        		$row->title == 'Herr' ? $title_prefix = 'r ' : $title_prefix = ' '; // Anrede für die Mail zusammensetzen
        		count($prices) == 2 ? $reference_multiline = ' station' : $reference_multiline = null; // entscheidet ob die Referenz neben den eigenen Preisen (wenn 1x) oder darunter (wenn 2x) dargestellt werden soll

        		$station_meta = fetch_station_meta($station_id); // die Daten der Station (Referenz URL, Reihenfolge der Spritsorten usw.) laden
        		$sql_station_prices = $db->query('select sfj.fuel_id, sfj.price, f.name, sfj.timestamp from station_fuel_join as sfj, fuels as f where f.id = sfj.fuel_id and sfj.station_id = '.mysqli_real_escape_string($db, $station_id).''); // Preise der eigenen Station aus der Datenbank ermitteln

        		if($sql_station_prices->num_rows > 0) // nur wenn die Preise aus der Datenbank ausgelesen werden konnten
        		{
    				// Array Struktur für apply_fuel_workarounds() und order_price_array() vorbereiten
    				$station_prices = array('success' => true, 'errors' => null, 'data' => array(array('id' => 1, 'image' => 'station.png', 'prices' => array())));

        			while($row_sp = $sql_station_prices->fetch_assoc()) // für alle geladenen Preise
        			{
        				if($row_sp['timestamp'] + DATABASE_STATION_PRICE_DURABILITY > time()) // sofern die Preise noch als aktuell betrachtet werden können
        				{
        					$station_prices['data'][0]['prices'][$row_sp['fuel_id']] = array('value' => $row_sp['price'], 'name' => $row_sp['name']); // Preis im Array abspeichern
    					}
    					else // aktueller Preis ist nicht mehr aktuell genug
    					{
    						if($station_meta['success']) // sofern die Stationsdaten geladen werden konnten (Referenz URL wird benötigt)
    						{
    							$station_prices_fetched = fetch_fuel_prices($station_meta['data']['reference_url'], $station_meta['data']['fuels']); // Spritpreise, für die eigene Station, von der Source laden

    							if($station_prices_fetched['success']) // wenn die Preise von der Source geladen werden konnten
    							{
    								$station_prices['data'][0]['prices'] = $station_prices_fetched['data']; // die geladenen Preise im Array abspeichern, wobei bisher gespeicherte Preise überschrieben werden
    							}
    							else // Preise konnten nicht geladen werden
    							{
    								$station_prices['success'] = false; // Ausführung als fehlgeschlagen markieren
    							}
    						}
    						else // Meta Daten konnten nicht geladen werden
    						{
    							$station_prices['success'] = false; // Ausführung als fehlgeschlagen markieren
    						}

    						break; // Schleife hier beenden, da Preise vollständig von Source nachgeladen wurden
    					}

        			}


        			if($station_prices['success']) // nur wenn die Preise der Station erfolgreich ermittel werden konnten (aus DB oder von Source)
        			{
	    				$station_prices_workaround = apply_fuel_workarounds($station_id, $station_prices['data']); // Workarounds auf die Preise anwenden

	    				if($station_meta['success']) // nur wenn zuvor die Stationsdaten geladen werden konnten
    					{
    						$station_prices['data'] = order_price_array($station_meta['data'], $station_prices_workaround['data'])['data']; // Preise sortieren
    					}
        			}
        		}

        		// E-Mail Content zusammenbauen
		    	$content = '
		    	<!DOCTYPE HTML>
				<html lang="de">
				<head>
					<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
					<title>Vers&auml;umnismeldung</title>
					<style type="text/css">
						html, body {font-family: Tahoma; background-color: #FFFFFF;}

						#wrapper {width: 650px; box-shadow: 0px 0px 20px -4px rgba(0,0,0,0.2); margin: 40px auto 0 auto;}
						#content {padding: 0 15px 1px 15px;}
						#footer {width: 640px; color: #666666; margin: 0 auto 0 auto; font-size: 11px;}
						#footer p {padding: 0 15px 0 15px;}


						/***** *** Kopfzeile *** *****/
						#header {width: 100%; height: 50px; padding: 10px 0 10px 0; background-color: #F1F1F1; margin: 0 0 20px 0;}

							/* Icon und Titel */
							#header #caption {width: 95%; height: 100%; float: left;}
							#header #caption img {max-height: 100%; float: left; margin-left: 10px;}
							#header #caption h1 {margin: 8px 10px 0 10px; font-size: 30px; float: left;}
							#header #caption h4 {margin: 21px 0 0 0; color: #808080; float: left;}


						/***** *** Wettbewerbsreferenzen *** *****/
						#references-wrap {width: 100%; float: left; margin-top: 20px;}
						#references {max-width: 620px; margin: 0 auto 0 auto; position: relative; float: left; left: 50%;}
						.references {display: inline-block; margin: 0 15px 30px 0; vertical-align: top; position: relative; left: -50%;}
						.references .brand {display: inline-block;}
						.references .brand img {margin: 10px 5px 0 0; width: 95px;}
						.references .brand p {margin: 5px 10px 0 0; text-align: center;}
						.references .prices {display: inline-block; border-left: 1px solid #D7D7D7; padding: 10px; vertical-align: top; min-height: 90px;}
						.references .prices ul {list-style: none; padding: 0; margin: 0 0 0 5px;}
						.references .prices li {padding: 5px 0 5px 0; max-width: 175px; height: 21px;}
						.references .prices li p {margin: 0; padding: 0; display: inline-block; vertical-align: middle;}
						.references .prices li span {float: right; margin-left: 15px;}

						.references.station {width: 100%;}
						.references .address {font-size: 11px; text-align: center; color: #9a9a9a; max-width: 250px;}
					</style>
				</dead>
				<body>
					<div id="wrapper">
						<div id="header">
					        <div id="caption">
					            <img src="http://common.eggerd.de/images/de-logo-400x400.png" alt="&nbsp;[Logo]" />
					            <h1>'.SERVICE_NAME.'</h1>
					            <h4>Vers&auml;umnismeldung</h4>
					        </div>
					    </div>
					    <div id="content">
					    	<p><b>Sehr geehrte'.$title_prefix.$row->title.' '.$row->last_name.',</b><br>
					    		<br>
					    		an Ihrer Station mit der Kennung '.$row->name.' wurde es offenbar '.$meta['count'].'-mal in Folge vers&auml;umt, die vom PriceBooster gemeldeten Preise Ihrer Wettbewerbspartner einzugeben.<br>
					    		<br>
					    		Nachfolgend finden Sie die Preise Ihrer Station sowie die der betroffenen Wettbewerbspartner, zum Zeitpunkt der Preismeldungen.<br>
					    		<br>
					    		Die Preise Ihrer Wettbewerbspartner wurden erstmalig am '.date("d.m.y, \u\m H:i", $meta['first_time']).' Uhr und zuletzt am '.date("d.m.y, \u\m H:i", time()).' Uhr gemeldet.
				    		</p>


				    		<div id="references-wrap">
				    			<div id="references">
					    			<div class="references'.$reference_multiline.'">
					    				<div class="brand">
					    					<img src="'.SERVICE_URL.'/images/brands/station.png" alt="&nbsp;[Station]">
					    					<p>Station</p>
				    					</div>
					    				<div class="prices">
											<ul>';

												if($station_prices['success']) // wenn die Preise der eigenen Station ermittelt werden konnten
												{
													foreach($station_prices['data'][0]['prices'] as $price) // für jede Spritsorte der eigenen Station
													{
														// Spritsorte dem Content hinzufügen
														$content .= '
														<li>
															<p>'.$price['name'].'</p>
															<span>'.number_format(($price['value'] / 100), 2).'<sup>9</sup></span>
														</li>';
													}
												}
												else // Preise konnten nicht ermittelt werden (nicht aus DB und nich von Source) - passiert hoffentlich nie ;-)
												{
													$content .= '<li>N/A</li>'; // eigene Preise als unbekannt betiteln
												}

									$content .= '
											</ul>
										</div>
									</div>';

									foreach($prices as $reference_meta) // für jede betroffene Referenz
									{
										$sql_refmeta = $db->query('select r.address, srj.identifier, rb.name from `references` as r, station_reference_join as srj, reference_brands as rb where srj.station_id = '.mysqli_real_escape_string($db, $station_id).' && srj.reference_id = '.$reference_meta['id'].' && srj.reference_id = r.id && r.brand = rb.id'); // Adresse und Identifier der Referenz laden

										// Adresse und Identifier festlegen
										if($sql_refmeta->num_rows > 0 && $row_refmeta = $sql_refmeta->fetch_object())
										{
											$address = $row_refmeta->address;
											$identifier = '#'.$row_refmeta->identifier;
											$name = $row_refmeta->name;
										}
										else
										{
											$address = null;
											$identifier = null;
											$name = ucfirst(preg_split('/\./', $reference_meta['image'])[0]); // Image-Name verwenden (Dateiendung entfernen)
										}

										$content .= '
					    				<div class="references">
					    					<div class="brand">
					    						<img src="'.SERVICE_URL.'/images/brands/'.$reference_meta['image'].'" alt="&nbsp;['.$name.']">
					    						<p>'.$identifier.'</p>
				    						</div>
					    					<div class="prices">
											<ul>';

											foreach($reference_meta['prices'] as $price_meta) // für jede Spritsorte der Referenz
											{
												$content .= '
												<li>
													<p>'.$price_meta['name'].'</p>
													<span>'.number_format(($price_meta['value'] / 100), 2).'<sup>9</sup></span>
												</li>';
											}

										$content .= '
												</ul>
											</div>
											<p class="address">'.$address.'</p>
										</div>';
									}

							$content .= '
								</div>
							</div>

							<p>Mit freundlichen Gr&uuml;&szlig;en,<br>
								Dustin Eckhardt</p>
					    </div>
					</div>
					<div id="footer">
						<p>Bei dieser E-Mail handelt es sich um eine automatisch versendete Nachricht. Antworten Sie daher bitte nicht auf diese E-Mail.
							<!-- Sollten Sie zuk&uuml;nftig keine E-Mails dieser Art mehr erhalten wollen, k&ouml;nnen Sie diese in den Einstellungen Ihres '.SERVICE_NAME.' deaktivieren
							oder auch die tolerierte Anzahl der vers&auml;umten Eingaben erh&ouml;hen bzw. herabsetzen.--><br>
							<br>
							Dustin Eckhardt &copy; '.date('Y').'</p>
					</div>
				</body>
				</html>';

				$receiver = $row->mail;
				$submitter = MAIL_SUBMITTER;
				$subject = '=?UTF-8?B?'.base64_encode(SERVICE_NAME.": Versäumnismeldung").'?=';

				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $submitter\r\n";
				$header .= "X-Mailer: PHP ". phpversion();

				if(!@mail($receiver, $subject, $content, $header))
				{
					return array('success' => false, 'errors' => array('2e024' => 'Die Vers&auml;umnismeldung konnte nicht gesendet werden!'), 'data' => null);
				}
			}
        	else // die Daten der Station konnten nicht ermittelt werden
        	{
        		return array('success' => false, 'errors' => array('2e023' => 'Die Vers&auml;umnismeldung konnte nicht gesendet werden, da die Stationsdaten nicht geladen werden konnten!'), 'data' => null);
        	}
		}
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }

?>
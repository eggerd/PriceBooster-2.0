<?php

   /* Stellt eine Verbindung zur angegebenen Datenbank her und gibt das
    * dadurch entstandene MySQLi-Objekt zurück.
    *
    * Version:  1.0.1
    * Stand:    28. Juli 2015
    *
    * Input:
    *   $hostname   : string    = der Hostname bzw. die IP unter dem die Datenbank zu erreichen ist
    *   $username   : string    = der zu verwendende Benutzername
    *   $password   : string    = das Passwort für den Datenbankbenutzer
    *   $database   : string    = die Datenbank die verwendet werden soll
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true             : boolean
    *       ['errors']  => passed->mysqli() : mixed
    *       ['data']    => $db              : object
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false            : boolean
    *       ['errors']  => passed->mysqli() : mixed
    *       ['data']    => null             : null
    *   }
    *
    * Errors:
    *   passed  ->  mysqli()
    */
    function db_connect($hostname, $username, $password, $database)
    {
        $db = @new mysqli($hostname, $username, $password, $database); // Verbindung mit Datenbank herstellen

        if($db->connect_error) // wenn bei der Verbindung fehler aufgetreten sind
        {
            return array('success' => false, 'errors' => array('5e001' => 'Es konnte keine Verbindung zur Datenbank hergestellt werden!'), 'data' => null);
        }
        else
        {
            return array('success' => true, 'errors' => null, 'data' => $db);
        }
    }





   /* Schließt die Verbindung der übergebenen Datenbank-Verbindung.
    * Wurde ausschließlich für zukünftige Funktionen/Aktionen integriert,
    * die ausgeführt werden sollen, wenn die Verbindung beendet wird.
    *
    * Version:  1.0.0
    * Stand:    26. Dezember 2014
    *
    * Input
    *   $db : object    = das Datenbank-Objekt, dessen Verbindung beendet werden soll
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => null : null
    *   }
    *
    * Failure:
    *  none
    *
    * Errors:
    *   none
    */
    function db_disconnect($db)
    {
        $db->close();

        return array('success' => true, 'errors' => null, 'data' => null);
    }

?>
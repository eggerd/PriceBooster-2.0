<?php

   /* Liest die Value des angegebenen Key-Paars für die aktuelle Station aus der
    * Datenbank aus, wodurch Einstellunden und sonstige Daten spezifisch für jede
    * Station geladen werden können.
    *
    * Version: 	1.0.0
    * Stand: 	11. August 2015
    *
    * Input:
    * 	$station_id	: integer 	= die ID der zugehörigen Station
    *	$key1     	: string    = der benötigte Haupt-Key
    *   $key2     	: string    = der optionale zweite Key
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null
    *       ['data']    => array
    *       {
    *       	[$key1] => array
    *       	{
    *       		[$key2] => array
    *       		{
    *       			['value1'] 	=> : mixed
    *       			['value2'] 	=> : mixed
    *       		}
    *       	}
    *       }
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => null
    *   }
    *
    * Errors:
    * 	passed 	-> db_connect()
    *	4w003 	=> "Zu den angegebenen Keys konnten keine Einstellungen geladen werden!"
    */
    function fetch_station_properties($station_id, $key1, $key2 = 'null')
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            // Value zu den zugehörigen Keys auslesen
            $sql = $db->query('select * from station_properties where station_id = '.mysqli_real_escape_string($db, $station_id).' and key1 = "'.mysqli_real_escape_string($db, $key1).'" and key2 = "'.mysqli_real_escape_string($db, $key2).'"');

            if($sql->num_rows > 0) // sofern Daten zu den angegebenen Keys gefunden wurden
            {
                $data = array(); // Rückgabe-Array

                while($row = $sql->fetch_assoc()) // für alle gefundenen Datensätze
                {
                    $data[$row['key1']] = array($row['key2'] => array($row['value1'], $row['value2'])); // Eintrag im Rückgabe-Array anlegen
                }

                return array('success' => true, 'errors' => null, 'data' => $data);
            }
            else // es wurde keine Daten gefunden
            {
                return array('success' => false, 'errors' => array('4w003' => 'Zu den angegebenen Keys konnten keine Einstellungen geladen werden!'), 'data' => null);
            }
        }
        else // Verbindung zur Datenbank konnte nicht hergestellt werden
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }





   /* Über diese Funktion werden grundlegende Daten der Station aus der Datenbank geladen.
    * Hierzu gehören z.B. die Adresse oder den Stationsnamen.
    *
    * Version:  1.0.0
    * Stand:    11. August 2015
    *
    * Input:
    *   $station_id 	: integer 	= die ID der Station, zu der die grundlegenden Infos benötigt werden
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null
    *       ['data']    => array
    *       {
    *       	['name'] 	=> : string
    *       	['address']	=> : string
    *       }
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => null
    *   }
    *
    * Errors:
    *   passed  -> db_connect()
    *   4e006   => "Informationen konnten nicht geladen werden, da die Station nicht existiert!"
    */
    function fetch_station_info($station_id)
    {
        require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
        require_once(dirname(__FILE__).'/mysql.php');

        $db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
        {
            // grundlegende Daten der Station abfragen
            $sql = $db->query('select name, address from stations where id = '.mysqli_real_escape_string($db, $station_id).' limit 1');

            if($sql->num_rows > 0) // sofern auch Daten geladen wurden
            {
                $data = array(); // Rückgabe-Array
                $row = $sql->fetch_object(); // Daten als Object laden

                // Daten im Rückgabe-Array abspeichern
                $data['name'] = $row->name;
                $data['address'] = $row->address;

                return array('success' => true, 'errors' => null, 'data' => $data);
            }
            else // es wurden keine Daten zur Station gefunden, da die Station offenbar nicht existiert
            {
                return array('success' => false, 'errors' => array('4e006' => 'Informationen konnten nicht geladen werden, da die Station nicht existiert!'), 'data' => null);
            }
        }
        else // Verbindung zur Datenbank ist fehlgeschlagen
        {
            return array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
        }
    }

?>
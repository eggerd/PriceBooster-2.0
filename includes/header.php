<head>
    <title><?php echo SERVICE_NAME; ?></title>

    <meta charset="UTF-8" />
    <meta name="description" content="Benachrichtigt Tankstellen sofern deren Wettbewerbspartner einen günstigeren Preis haben." />
    <meta name="author" content="Dustin Eckhardt" />
    <meta name="keywords" content="PriceBooster" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <link href="./css/layout.css" type="text/css" rel="stylesheet" />
    <link href="./css/perfect-scrollbar.css" type="text/css" rel="stylesheet" />
    <link href="./images/de-logo-42x42.ico" type="image/x-icon" rel="shortcut icon" />

    <!-- Javascript Libraries -->
    <script language="javascript" type="text/javascript" src="./javascript/libraries/jquery-3.2.1.min.js"></script>
    <script language="javascript" type="text/javascript" src="./javascript/libraries/kiosk-browser.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/libraries/jquery-ui.min.js"></script> <!-- wird für den Data-Selektor benötigt -->
    <script async language="javascript" type="text/javascript" src="./javascript/libraries/jquery-cookie.min.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/libraries/offline-0.7.13.min.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/libraries/perfect-scrollbar.jquery.min.js"></script> <!-- wird bei Overlays verwendet -->
    <script async language="javascript" type="text/javascript" src="./javascript/libraries/eckstein-countdown.js"></script> <!-- wird in Placeholdern und bei Referenzen verwendet -->

    <!-- Javascript Functions -->
    <script language="javascript" type="text/javascript" src="./javascript/functions/properties.js"></script>
    <script language="javascript" type="text/javascript" src="./javascript/functions/debug.js"></script>
    <script language="javascript" type="text/javascript" src="./javascript/functions/login.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/functions/notifications.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/functions/broadcasts.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/functions/prices.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/functions/runtime.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/functions/connection.js"></script>
    <script async language="javascript" type="text/javascript" src="./javascript/functions/overlay.js"></script>

    <script language="javascript" type="text/javascript" src="./javascript/onload.js"></script> <!-- wird nach dem Laden der Seite ausgeführt -->
</head>
<?php

   /* Version:  1.0.1
    * Stand:    13. September 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   passed  ->  fetch_broadcasts()
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */

    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/broadcasts.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/login.php');

    if(authenticated()['data'] === true) // sofern eine Station angemeldet ist
    {
        $return = fetch_broadcasts($_COOKIE['station_id']); // Broadcasts für diese Station abfragen
    }
    else // Default Aktion: Global Broadcasts
    {
        $return = fetch_broadcasts(); // Broadcasts abfragen, die für keine bestimmte Station definiert wurden
    }

    echo json_encode($return);

?>
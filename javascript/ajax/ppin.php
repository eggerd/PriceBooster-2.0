<?php

   /* Version:  1.0.3
    * Stand:    17. Dezember 2015
    *
    * Input:
    *   ?type 	: string    = welcher Bereich der Einstellungen verarbeitet werden soll
    *   ?data   : string    = die Daten die gespeichert werden sollen
    *
    * Success:
    *   passed  ->  set_ppin()
    *
    * Failure:
    * 	passed  ->  set_ppin()
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    * 	passed 	=> 	db_connect()
    * 	passed 	=> 	authenticated()
    *   6e007   =   "Es wurden nicht alle ben&ouml;tigten Argumente &uuml;bergeben!"
    */

    require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/mysql.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/login.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/libraries/iha.class.php');


    $authenticated = authenticated();

    if($authenticated['success'] && $authenticated['data'] === true)
    {
	    if(isset($_REQUEST['data']))
	    {
			$db_connect = db_connect(CONF_MYSQL_HOSTNAME, CONF_MYSQL_USERNAME, CONF_MYSQL_PASSWORD, CONF_MYSQL_DATABASE); // Verbindung mit Datenbank herstellen

	        if($db_connect['success'] === true && $db = $db_connect['data']) // wenn Verbindung zur Datenbank hergestellt wurde
	        {
	        	$sql = $db->query('select t.password from tenants as t, stations as s where s.id = '.mysqli_real_escape_string($db, $_COOKIE['station_id']).' and s.tenant_id = t.id limit 1');

				if($sql->num_rows > 0 && $row = $sql->fetch_object())
				{
					$iha = new iha();
					$data = json_decode($_REQUEST['data']);

					if($iha->compare($data, $row->password))
					{
						$return = array('success' => true, 'errors' => null, 'data' => true);
					}
					else // PPIN nicht korrekt
					{
						$return = array('success' => true, 'errors' => null, 'data' => false);
						// E-Mail Benachrichtigung senden?
					}
				}
				else
				{
					$return = array('success' => false, 'errors' => array('6e021' => 'Der P&auml;chter-PIN konnte nicht ermittelt werden!'), 'data' => null);
				}
	        }
	        else // Verbindung zur Datenbank ist fehlgeschlagen
	        {
	            $return = array('success' => false, 'errors' => $db_connect['errors'], 'data' => null);
	        }
	    }
	    else
	    {
	    	$return = array('success' => false, 'errors' => array('6e020' => 'Es wurden nicht alle ben&ouml;tigten Argumente &uuml;bergeben!'), 'data' => null);
	    }
	}
	else
	{
		$return = array('success' => false, 'errors' => $authenticated['errors'], 'data' => null);
	}


    echo json_encode($return);

?>
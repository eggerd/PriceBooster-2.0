<?php

   /* Version:  1.0.1
    * Stand:    17. Dezember 2015
    *
    * Input:
    *   ?dump 	: string 	= die Einträge aus dem Debug-Log die gespeichert werden sollen
    *
    * Success:
    *   array
    *   {
    *       ['success']	=> true 	: boolean
    *       ['errors']  => null 	: null
    *       ['data']    => null 	: null
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    * 	passed 	-> save_debug_dump()
    * 	passed 	-> authenticated()
    * 	0e001 	=> "Dem Server wurde kein Debug-Dump übermittelt, das hätte gespeichert werden können!"
    */

    require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/debug.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/login.php');

    $authenticated = authenticated();

    if($authenticated['success'])
    {
    	if($authenticated['data'] === true)
    	{
		    if(isset($_REQUEST['dump'])) // sofern ein Typ definiert wurde
		    {
		        $return = debug_save_dump($_COOKIE['station_id'], $_REQUEST['dump']);
		    }
		    else // es wurde kein Type angegeben - nicht definiert welche Daten benötigt werden
		    {
		        $return = array('success' => false, 'errors' => array('0e001' => 'Dem Server wurde kein Debug-Dump &uuml;bermittelt, das h&auml;tte gespeichert werden k&ouml;nnen!'), 'data' => null);
		    }
	    }
	    else
	    {
	    	$return = array('success' => false, 'errors' => $authenticated['errors'], 'data' => null);
	    }
    }
    else
    {
    	$return = array('success' => false, 'errors' => $authenticated['errors'], 'data' => null);
    }

    echo json_encode($return);

?>
<?php

   /* Version:  1.0.3
    * Stand:    11. August 2015
    *
    * Input:
    *   ?type 	: string 	= gibt an welche Aktion ausgeführt werden soll
    *   ?key1 	: string 	= Key1 der für fetch_station_properties() benötigt wird
    *   ?key2 	: string 	= Key2 der für fetch_station_properties() benötigt wird
    *
    * Success:
    *   passed  ->  fetch_station_properties()
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed 	-> fetch_station_info()
    *   passed 	-> fetch_station_properties()
    *
    * 	4e004 	=> "Beim Versuch Stations-Einstellungen zu laden wurde keine Stationskennung gefunden!"
    * 	4e005 	=> "Beim Laden von Einstellungen wurde keine Typsepzifikation übergeben!"
    */

    require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/properties.php');

    if(isset($_COOKIE['station_id'])) // sofern eine Station angemeldet ist
    {
        if(isset($_REQUEST['type'])) // sofern ein Typ definiert wurde
        {
            if($_REQUEST['type'] == 'info') // wenn die grundlegenden Stationsdaten angefordert werden
            {
                $return = fetch_station_info($_COOKIE['station_id']);
            }
            else
            {
                $return = fetch_station_properties($_COOKIE['station_id'], json_decode($_REQUEST['key1']), json_decode($_REQUEST['key2']));
            }
        }
        else // es wurde kein Type angegeben - nicht definiert welche Daten benötigt werden
        {
            $return = array('success' => false, 'errors' => array('4e005' => 'Beim Laden von Einstellungen wurde keine Typsepzifikation &uuml;bergeben!'), 'data' => null);
        }
    }
    else // auf dem Client ist offenbar keine Station angemeldet - Abfrage verweigert!
    {
        $return = array('success' => false, 'errors' => array('4e004' => 'Beim Versuch Stations-Einstellungen zu laden wurde keine Stationskennung gefunden!'), 'data' => null);
    }

    echo json_encode($return);

?>
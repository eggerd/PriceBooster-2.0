<?php

   /* Version:  1.0.2
    * Stand:    04. August 2015
    *
    * Input:
    *   ?station        : string    = enthält die Stationsnummer
    *   ?password       : string    = enthält das verschlüsselte Passwort für die Station
    *   ?authenticated  : null      = falls gesetzt wird nach dem Login Status gefragt
    *   ?logout         : null      = falls gesetzt wird der Logout angefordert
    *
    * Success:
    *   passed  ->  login()
    *   passed  ->  authenticated()
    *   passed  ->  logout()
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   1e006   =   "Es wurden nicht alle ben&ouml;tigten Argumente &uuml;bergeben!"
    */

    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/login.php');

    if(isset($_REQUEST['station']) && isset($_REQUEST['password'])) // nur wenn alle Argumente übergeben wurden
    {
        $return = login($_REQUEST['station'], $_REQUEST['password']); // Login durchführen
    }
    elseif(isset($_REQUEST['authenticated'])) // falls der Login-Status abgefragt werden soll
    {
        $return = authenticated();
    }
    elseif(isset($_REQUEST['logout'])) // wird ein Logout anfefordert
    {
        $return = logout();
    }
    else // es wurde keine gültige Aktion übergeben
    {
        $return = array('success' => false, 'errors' => array('1e006' => 'Es wurden nicht alle ben&ouml;tigten Argumente &uuml;bergeben!'), 'data' => null);
    }

    echo json_encode($return);

?>
<?php

   /* Version:  1.0.2
    * Stand:    04. August 2015
    *
    * Input:
    *   ?type 	: string    = welcher Bereich der Einstellungen verarbeitet werden soll
    *   ?data   : string    = die Daten die gespeichert werden sollen
    *
    * Success:
    *   passed  ->  set_ppin()
    *   passed 	->  set_contact()
    *
    * Failure:
    * 	passed  ->  set_ppin()
    * 	passed 	-> 	set_contact()
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   6e007   =   "Es wurden nicht alle ben&ouml;tigten Argumente &uuml;bergeben!"
    */

    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/overlay-settings.php');


    if(isset($_REQUEST['type']) && isset($_REQUEST['data']))
    {
    	if($_REQUEST['type'] == 'ppin') // verarbeitung des "Pächter-PIN" Bereichs
		{
			$data = json_decode($_REQUEST['data']); // Passwort Daten decode

			$return = set_ppin($data); // Passwort für Pächter übernehmen
		}
		elseif($_REQUEST['type'] == 'contact') // verarbeitung des "Kontaktdaten" Bereichs
		{
			$data = json_decode($_REQUEST['data']); // Kontaktdaten decode
			$return = set_contact($data); // Kontaktdaten übernehmen
		}
		elseif($_REQUEST['type'] == 'passwd') // verarbeitung des "Kontaktdaten" Bereichs
		{
			$data = json_decode($_REQUEST['data']); // Passwort Daten decode

			$return = set_passwd($data); // Passwort für Pächter übernehmen
		}
		elseif($_REQUEST['type'] == 'support')
		{
			$return = support_verification(json_decode($_REQUEST['data']));
		}
    }
    else
    {
    	$return = array('success' => false, 'errors' => array('6e007' => 'Es wurden nicht alle ben&ouml;tigten Argumente &uuml;bergeben!'), 'data' => null);
    }


    echo json_encode($return);

?>
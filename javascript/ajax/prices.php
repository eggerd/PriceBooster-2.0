<?php

   /* Version:  1.0.0
    * Stand:    13. März 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   passed  ->  authenticated()
    *   passed  ->  price_validation()
    *
    * Failure:
    *   passed  ->  authenticated()
    *   passed  ->  price_validation()
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  -> authenticated()
    *   2e003   => "Das Überprüfen der Preise ist fehlgeschlagen, da keine Stationskennung übermittelt wurde!"
    */

    // Funktionen einbinden
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/prices.php');
    require_once(realpath(dirname(__FILE__).'/../../').'/includes/functions/login.php');

    if(isset($_COOKIE['station_id'])) // nur wenn überhaupt eine Stationskennung gesetzt ist
    {
        $authenticated = authenticated(); // Login überprüfen

        if($authenticated['data'] === true)
        {
            $return = price_validation($_COOKIE['station_id']); // Price Check starten
        }
        else
        {
            $return = array('success' => false, 'errors' => $authenticated['errors'], 'data' => null);
        }
    }
    else
    {
        $return = array('success' => false, 'errors' => array('2e003' => 'Das &Uuml;berpr&uuml;fen der Preise ist fehlgeschlagen, da keine Stationskennung &uuml;bermittelt wurde!'), 'data' => null);
    }

    echo json_encode($return);

?>
<?php

   /* Version:  1.0.2
    * Stand:    11. August 2015
    *
    * Input:
    *   ?content 	: string 	= definiert die Datei die den angeforderten Content enthält
    *
    * Success:
    *   array
    *   {
    *       ['success'] => true    : boolean
    *       ['errors']  => null
    *       ['data']    => : string
    *   }
    *
    * Failure:
    *   array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [string]    => : string
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   6e003 	=> "Es wurde kein Content spezifiziert, der geladen werden hätte können!"
    *   6e004 	=> "Der benötigte Content konnte nicht geladen werden, da er auf dem Server nicht gefunden werden konnte!"
    */

    require_once(realpath(dirname(__FILE__).'/../../').'/configs/configure.php');

    if(isset($_REQUEST['content'])) // sofern eine Content-Datei definiert wurde
    {
        if(is_readable(realpath(dirname(__FILE__).'/../../').'/includes/overlay/'.$_REQUEST['content'])) // sofern die angeforderte Datei existiert und lesbar ist
        {
            ob_start(); // Ausgaben im Puffer abfangen

            require_once(realpath(dirname(__FILE__).'/../../').'/includes/overlay/'.$_REQUEST['content']); // Content-Datei einbinden
            $return = array('success' => true, 'errors' => null, 'data' => ob_get_clean()); // in der Zwischenzeit getätigte Ausgaben im Return-Array speichern und Puffer leeren

            ob_end_clean(); // Puffer abschalten und alle übrigen Daten verwerfen
        }
        else // Content-Datei ist nicht lesbar oder nicht vorhanden
        {
            $return = array('success' => false, 'errors' => array('6e004' => 'Der ben&ouml;tigte Content konnte nicht geladen werden, da er auf dem Server nicht gefunden werden konnte!'), 'data' => null);
        }
    }
    else // es wurde keine Content-Datei definiert
    {
        $return = array('success' => false, 'errors' => array('6e003' => 'Es wurde kein Content spezifiziert, der geladen werden h&auml;tte k&ouml;nnen!'), 'data' => null);
    }

    echo json_encode($return);

?>

//	http://github.hubspot.com/offline/
//
//
//	Offline.check(): Check the current status of the connection.
//
//	Offline.state: The current state of the connection 'up' or 'down'
//
//	Offline.on(event, handler, context): Bind an event. Events:
//
//		up: The connection has gone from down to up
//		down: The connection has gone from up to down
//		confirmed-up: A connection test has succeeded, fired even if the connection was already up
//		confirmed-down: A connection test has failed, fired even if the connection was already down
//		checking: We are testing the connection
//		reconnect:started: We are beginning the reconnect process
//		reconnect:stopped: We are done attempting to reconnect
//		reconnect:tick: Fired every second during a reconnect attempt, when a check is not happening
//		reconnect:connecting: We are reconnecting now
//		reconnect:failure: A reconnect check attempt failed
//		requests:flush: Any pending requests have been remade
//		requests:hold: A new request is being held
//
//	Offline.off(event, handler): Unbind an event


	Offline.options = {

		checks: {image: {url: 'https://www.google.com/s2/favicons?domain=google.de'}, active: 'image'},

		// Should we check the connection status immediatly on page load.
		checkOnLoad: false,

		// Should we monitor AJAX requests to help decide if we have a connection.
		interceptRequests: true,

		// Should we automatically retest periodically when the connection is down (set to false to disable).
		reconnect: false, /*{
		// How many seconds should we wait before rechecking.
		initialDelay: 3,

		// How long should we wait between retries.
		delay: (1.5 * last delay, capped at 1 hour)
		},*/

		// Should we store and attempt to remake requests which fail while the connection is down.
		requests: false,

		// Should we show a snake game while the connection is down to keep the user entertained?
		// It's not included in the normal build, you should bring in js/snake.js in addition to
		// offline.min.js.
		game: false
	}


	Offline.on('confirmed-up', function()
	{
		handle_errors('runtime', {'errors': {'4e009': 'Die Service-Version konnte auf Grund von Verbindungsproblemen nicht &uuml;berpr&uuml;ft werden!'}});
		prepare_reference_container(null, 'malfunction'); // Malfunction-Placeholder anzeigen
	});


	Offline.on('confirmed-down', function()
	{
		prepare_reference_container(function($return)
		{
			if($return != true)
			{
				handle_errors('runtime', {'errors': {'4e011': 'Es besteht offenbar keine Verbindung zum Internet!'}});
			}
		}, 'offline'); // Malfunction-Placeholder anzeigen
	});
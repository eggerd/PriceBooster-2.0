
	$(function() // wird nach dem Laden der Seite ausgeführt
    {
    	overlay_show();
    	var $hide_timeout = 0;

    	overlay_destructor(function(callback) // wird vor dem Schließen des Overlays ausgeführt
		{
			if(typeof $overlay_view != 'undefined' && typeof $overlay_view.delete == 'function')
			{
				$overlay_view.delete(); // aktuelle View und alle damit verbundenen Handler / Funktionen löschen
			}

			clearTimeout($hide_timeout);
			$('#overlay #context').off(); // alle Event-Handler des Contents entfernen

			$overlay_view = undefined; // Variable mit aktuellem View-Objekt löschen
			$hide_timeout = 0;

			// View-Objekte löschen
			overlay_ppin = overlay_passwd = undefined;

			callback();
		});


		overlay_hidehandler(function() // wird nach dem ausblenden des Overlays ausgeführt
		{
			if(typeof $overlay_view == 'object' && $.type($overlay_view.delete) == 'function') // nur wenn wirklich ein View-Objekt gesetzt ist, das auch eine Delete-Funktion besitzt
    		{
    			$overlay_view.delete(); // aktuelle View löschen
			}

			// View wieder auf PPIN Eingabe setzen
			$overlay_view = new overlay_reqppin($('#overlay #reqppin'), function()
			{
				$('#overlay #option-menu .option-menu-entry:eq(0)').click();
				//$overlay_view = new overlay_ppin();
				//$('#overlay #option-wrapper').show();
			});
		});





		// View-Objekt erzeugen und damit Default-Bereich laden
    	$overlay_view = new overlay_reqppin($('#overlay #reqppin'), function()
    	{
    		$overlay_view = new overlay_ppin();
    		$('#overlay #option-wrapper').show();
    	});



    	$('#overlay #option-menu .option-menu-entry').on('click', function() // onClick für Menü-Einträge
    	{
    		var $element_index = $(this).index();

    		$('#overlay #option-menu .option-menu-entry.active').removeClass('active');
    		$(this).addClass('active');

    		if($.type($overlay_view) == 'object' && $.type($overlay_view.delete) == 'function') // nur wenn wirklich ein View-Objekt gesetzt ist, das auch eine Delete-Funktion besitzt
    		{
    			$overlay_view.delete(); // aktuelle View löschen
			}

    		switch($element_index) // je nach Index des angeklickten Menü-Eintrags das entsprechende View erzeugen
    		{
    			case 0: $overlay_view = new overlay_ppin(); break;
    			case 1: $overlay_view = new overlay_passwd(); break;
    			case 2: $overlay_view = new overlay_contact(); break;
    			case 3: $overlay_view = new overlay_ohours(); break;

    			case 5: $overlay_view = new overlay_support(); break;
    		}
    	});



    	$("#overlay #context").on('mousemove keyup', function() // bei Keyup oder Mousemoves innerhalb des Overlays
    	{
    		clearTimeout($hide_timeout); // Hide-Timeout abbrechen

    		// sollte der Benutzer für die Dauer dieses Timeout nicht weiter mit dem Overlay
    		// interagieren (Mousemove, Keyup), wird das Overlay ausgeblendet um es somit wieder
    		// mit dem PPIN zu sperren. Somit wird verhindert, dass das Overlay versehentlich
    		// offengelassen wird und somit misbraucht werden kann.
    		$hide_timeout = setTimeout(function()
    		{
    			overlay_hide(); // Overlay ausblenden
    		}, 300000); // 5 Minuten
		});
    });










   /* ***** KLASSE *****
    * Dieses View-Objekt ist für den Content des Pächter-PINs zuständig und beinhaltet
    * alle FUnktionen die für die Verarbeitung dieses Contents benötigt werden.
    *
    * Version:  1.0.0
    * Stand:    22. November 2015
    *
    * Input:
    *   none
    *
    * Function:
    *   void    : cancel()
    *   void    : save()
    *   mixed   : value()
    *   void    : delete()
    */
	function overlay_ppin()
	{
		var $this = this;
		var $ppin = undefined;

		$('#overlay #option-content #ppin').show(); // zugehörigen Content einblenden



	   /* Bricht die Eingabe ab und versetzt das Formular sowie dieses Objekt in
	   	* seinen Ausgangszustand.
        *
        * Version:  1.0.0
        * Stand:    22. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.cancel = function()
		{
			// Objekt-Variablen zurücksetzen
			$ppin = undefined;

			// Formular zurücksetzen
			$('#overlay #option-content #ppin #value input').val('');
			$('#overlay #option-content #ppin #value input:first').focus();
			$('#overlay #option-content #ppin #value button[name="submit"]').show();
			$('#overlay #option-content #ppin #value button[name="cancel"]').hide();
			$('#overlay #option-content #ppin #value input, #overlay #option-content #ppin #value button').removeAttr('disabled');
			$('#overlay #option-content #ppin #value button[name="cancel"]').removeClass('disabled');
			$('#overlay #option-content #ppin #value button[name="submit"]').attr('disabled', 'disabled');
		}



	   /* Speichert die Eingabe des Benutzers in die Datenbank. Hierbei werden auch
	   	* alle nötigen Style-Anpassungen am Formular gemacht.
        *
        * Version:  1.0.0
        * Stand:    22. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   6e009 	= "Der Pächter-Pin konnte nicht gespeichert werden, da der Server eine ungültige Antwort geliefert hat!"
        *   6e010	= "Auf Grund eines Verbindungsproblems konnten der Pächter PIN nicht gespeichert werden!"
        *   passed	-> ajax -> set_ppin()
        *   passed	-> ajax
        */
		this.save = function()
		{
			var $value = overlay_ppin_value($('#overlay #option-content #ppin #value input')); // Value der Eingabe ermitteln

			if($ppin == $value) // sofern die zweite Eingabe mit der vorherigen übereinstimmt
			{
				// Formular deaktivieren
				$('#overlay #option-content #ppin #value input, #overlay #option-content #ppin #value button').attr('disabled', 'disabled');

				$.ajax({
		            url: './javascript/ajax/overlay-settings.php',
		            data: {type: 'ppin', data: JSON.stringify($ppin)},
		            type: 'post',
		            success: function($ajaxReturn)
		            {
		                try
		                {
		                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
		                }
		                catch(err) // falls die Umwandlung fehlschlägt
		                {
		                   $this.cancel(); // Formular in Ausgangszustand zurücksetzen
		                   handle_errors('overlay', {'success': false, 'errors': {'6e009': 'Der P&auml;chter-Pin konnte nicht gespeichert werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});
		                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
		                }

		                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
		                {
							$this.cancel(); // Formular in Ausgangszustand zurücksetzen
							overlay_hint('limegreen', 'Der neue PIN wurde gespeichert!');
		                }
		                else
		                {
							$this.cancel(); // Formular in Ausgangszustand zurücksetzen
							handle_errors('overlay', $data);
		                }
		            },
		            error: function() // falls Verbindungsprobleme aufgetreten sind
		            {
						$this.cancel(); // Formular in Ausgangszustand zurücksetzen
						handle_errors('overlay', {'success': false, 'errors': {'6e010': 'Auf Grund eines Verbindungsproblems konnten der P&auml;chter PIN nicht gespeichert werden!'}, 'data': null});
		            }
		        });
			}
			else // die Eingaben stimmen nicht überein
			{
				$('#overlay #option-content #ppin #value input').val(''); // Formular leeren
				$('#overlay #option-content #ppin #value input:first').focus(); // erstes Feld fokusieren

				overlay_hint('red', 'Die Eingaben stimmen nicht überein!');
			}
		}



	   	// Event-Handler: Abbrechen-Button
		$('#overlay #option-content #ppin #value button[name="cancel"]').on('click', function()
    	{
			$this.cancel(); // Formular in Ausgangszustand zurücksetzen
    	});



		// Event-Handler: Input Felder
    	$('#overlay #option-content #ppin #value input').keyup(function($key)
    	{
    		overlay_ppin_keyup(this, $key, function($data)
    		{
    			if($data['data'] == 4 && $ppin != undefined) // sofern wir beim letzten Feld angekommen sind und dies die zweite Eingabe ist
	    		{
	    			$this.save(); // Speichervorgang auslösen
	    		}

	    		if($data['data'] == 4 && $ppin == undefined) // sofern wir beim letzten Feld angekommen sind und dies die ERSTE Eingabe ist
	    		{
	    			$('#overlay #option-content #ppin #value button[name="submit"]').removeAttr('disabled'); // Senden Button aktivieren
	    		}
	    		else
	    		{
	    			$('#overlay #option-content #ppin #value button[name="submit"]').attr('disabled', 'disabled'); // Senden Button deaktivieren, falls wieder ein Feld gelöscht wird
	    		}
    		});
    	});



		// Event-Handler: Senden-Button
    	$('#overlay #option-content #ppin #value button[name="submit"]').on('click', function()
    	{
    		var $value = overlay_ppin_value($('#overlay #option-content #ppin #value input')); // Value der Eingabe ermitteln

    		$('#overlay #option-content #ppin #value input').val(''); // Felder wieder leeren
			$('#overlay #option-content #ppin #value input:first').focus(); // erstes Feld auswählen

    		if($value.length == 4) // sofern die Value auch vier Zeichen lang ist
    		{
    			$ppin = $value; // Value in Objekt-Variable abspeichern, für späteren Vergleich

				$('#overlay #option-content #ppin #value button[name="submit"]').hide(); // Senden-Button ausblenden
				$('#overlay #option-content #ppin #value button[name="cancel"]').show(); // Abbrechen-Button einblenden

				overlay_hint('dodgerblue', 'Zum Speichern bitte die Eingabe wiederholen');
			}
			else
			{
				overlay_hint('red', 'Der PIN muss aus vier Zeichen bestehen!');
			}
    	});



       /* Wird aufgerufen wenn der Content im Overlay gewechselt wird. Die Funktion
       	* entfernt dann alle ihre Event-Handler, versteckt ihren Content und löscht
       	* sich als View-Objekt.
        *
        * Version:  1.0.0
        * Stand:    22. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.delete = function()
		{
			$('#overlay #option-content #ppin').find('*').off(); // alle Event-Handler entfernen
			$('#overlay #option-content #ppin').hide(); // Content ausblenden
			$overlay_view = null;
		}
	}










   /* ***** KLASSE *****
    * Dieses View-Objekt ist für den Content des Stationspassworts zuständig und beinhaltet
    * alle FUnktionen die für die Verarbeitung dieses Contents benötigt werden.
    *
    * Version:  1.0.0
    * Stand:    08. Dezember 2015
    *
    * Input:
    *   none
    *
    * Function:
    *   void    : cancel()
    *   void    : save()
    *   void    : delete()
    */
	function overlay_passwd()
	{
		var $this = this;
		var $passwd = undefined;

		$('#overlay #option-content #passwd').show();



	   /* Bricht die Eingabe ab und versetzt das Formular sowie dieses Objekt in
	   	* seinen Ausgangszustand.
        *
        * Version:  1.0.0
        * Stand:    08. Dezember 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.cancel = function()
		{
			// Objekt-Variablen zurücksetzen
			$passwd = undefined;

			// Formular zurücksetzen
			$('#overlay #option-content #passwd #value input').val('');
			$('#overlay #option-content #passwd #value input').focus();
			$('#overlay #option-content #passwd #value button[name="submit"]').show();
			$('#overlay #option-content #passwd #value button[name="cancel"]').hide();
			$('#overlay #option-content #passwd #value input, #overlay #option-content #passwd #value button').removeAttr('disabled');
			$('#overlay #option-content #passwd #value button[name="submit"]').attr('disabled', 'disabled');
		}



	   /* Speichert die Eingabe des Benutzers in die Datenbank. Hierbei werden auch
	   	* alle nötigen Style-Anpassungen am Formular gemacht.
        *
        * Version:  1.0.0
        * Stand:    08. Dezember 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   6e025	= "Auf Grund eines Verbindungsproblems konnte das Passwort nicht gespeichert werden!"
        *   6e026 	= "Das Passwort konnte nicht gespeichert werden, da der Server eine ungültige Antwort geliefert hat!"
        *   passed	-> ajax -> set_passwd()
        *   passed	-> ajax
        */
		this.save = function()
		{
			$('#overlay #option-content #passwd #value input, #overlay #option-content #passwd #value button[name="cancel"]').attr('disabled', 'disabled');

			$.ajax({
	            url: './javascript/ajax/overlay-settings.php',
	            data: {type: 'passwd', data: JSON.stringify($passwd)},
	            type: 'post',
	            success: function($ajaxReturn)
	            {
	                try
	                {
	                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
	                }
	                catch(err) // falls die Umwandlung fehlschlägt
	                {
	                   $this.cancel(); // Formular in Ausgangszustand zurücksetzen
	                   handle_errors('overlay', {'success': false, 'errors': {'6e026': 'Das Passwort konnte nicht gespeichert werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});
	                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
	                }

	                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
	                {
						$this.cancel(); // Formular in Ausgangszustand zurücksetzen
						overlay_hint('limegreen', 'Das neue Passwort wurde gespeichert!');
	                }
	                else
	                {
						$this.cancel(); // Formular in Ausgangszustand zurücksetzen
						handle_errors('overlay', $data);
	                }
	            },
	            error: function() // falls Verbindungsprobleme aufgetreten sind
	            {
					$this.cancel(); // Formular in Ausgangszustand zurücksetzen
					handle_errors('overlay', {'success': false, 'errors': {'6e025': 'Auf Grund eines Verbindungsproblems konnte das Passwort nicht gespeichert werden!'}, 'data': null});
	            }
	        });
		}



		// Event-Handler: Input Felder
    	$('#overlay #option-content #passwd #value input').keyup(function($key)
    	{
    		var $value = $('#overlay #option-content #passwd #value input').val();

    		if($value.length >= 4)
    		{
    			if($passwd == undefined) // wenn es sich um die erste Eingabe handelt
    			{
    				$('#overlay #option-content #passwd #value button').removeAttr('disabled');
    			}
    			else
    			{
    				if($value.length >= $passwd.length)
    				{
    					if($passwd === $value)
    					{
    						$this.save();
    					}
    					else
    					{
    						$('#overlay #option-content #passwd #value input').val(''); // Formular leeren
							$('#overlay #option-content #passwd #value input').focus(); // erstes Feld fokusieren

							overlay_hint('red', 'Die Eingaben stimmen nicht überein!');
    					}
    				}
    			}
    		}
    		else
    		{
    			$('#overlay #option-content #passwd #value button[name="submit"]').attr('disabled', 'disabled');
    		}
    	});



    	// Event-Handler: Abbrechen-Button
		$('#overlay #option-content #passwd #value button[name="cancel"]').on('click', function()
    	{
			$this.cancel(); // Formular in Ausgangszustand zurücksetzen
    	});



    	// Event-Handler: Senden-Button
    	$('#overlay #option-content #passwd #value button[name="submit"]').on('click', function()
    	{
    		var $value = $('#overlay #option-content #passwd #value input').val(); // Value der Eingabe ermitteln

    		$('#overlay #option-content #passwd #value input').val(''); // Felder wieder leeren
			$('#overlay #option-content #passwd #value input').focus(); // Feld auswählen

    		if($value.length >= 4 && $passwd == undefined) // sofern die Value auch vier Zeichen lang ist & es sich um die erste Eingabe handelt
    		{
    			$passwd = $value; // Value in Objekt-Variable abspeichern, für späteren Vergleich

				$('#overlay #option-content #passwd #value button[name="submit"]').hide(); // Senden-Button ausblenden
				$('#overlay #option-content #passwd #value button[name="cancel"]').show(); // Abbrechen-Button einblenden

				overlay_hint('dodgerblue', 'Zum Speichern bitte die Eingabe wiederholen');
			}
			else
			{
				overlay_hint('red', 'Das Passwort muss aus mindestens vier Zeichen bestehen!');
			}
    	});



       /* Wird aufgerufen wenn der Content im Overlay gewechselt wird. Die Funktion
       	* entfernt dann alle ihre Event-Handler, versteckt ihren Content und löscht
       	* sich als View-Objekt.
        *
        * Version:  1.0.0
        * Stand:    08. Dezember 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.delete = function()
		{
			$('#overlay #option-content #passwd').find('*').off(); // alle Event-Handler entfernen
			$('#overlay #option-content #passwd').hide();
			$overlay_view = null;
		}
	}










   /* ***** KLASSE *****
    * Dieses View-Objekt ist für den Content der Kontaktdaten zuständig und beinhaltet
    * alle FUnktionen die für die Verarbeitung dieses Contents benötigt werden.
    *
    * Version:  1.0.0
    * Stand:    22. November 2015
    *
    * Input:
    *   none
    *
    * Function:
    *   void    : cancel()
    *   void    : save()
    *   mixed   : check()
    *   void    : delete()
    */
	function overlay_contact()
	{
		var $this = this;
		var $values = {};

		$('#overlay #option-content #contact').show(); // zugehörigen Content einblenden



	   /* Bricht die Eingabe ab und versetzt das Formular sowie dieses Objekt in
	   	* seinen Ausgangszustand.
        *
        * Version:  1.0.0
        * Stand:    22. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.cancel = function()
		{
			$('#overlay #option-content #contact tr:not(.noedit) input').removeAttr('disabled');
			$('#overlay #option-content #contact button').attr('disabled', 'disabled');

			for(var $index in $values) // für alle veränderten Values
			{
				$('#overlay #option-content #contact input[name="'+$index+'"]').val($values[$index]); // ursprüngliche Value wieder ins Feld schreiben
			}

			$values = {}; // Speicher für ursprüngliche Werte zurücksetzen
		}



	   /* Speichert die Eingabe des Benutzers in die Datenbank. Hierbei werden auch
	   	* alle nötigen Style-Anpassungen am Formular gemacht.
        *
        * Version:  1.0.0
        * Stand:    22. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   6e012 	= "Die Kontaktdaten konnte nicht gespeichert werden, da der Server eine ungültige Antwort geliefert hat!"
        *   6e013 	= "Auf Grund eines Verbindungsproblems konnten die Kontaktdaten nicht gespeichert werden!"
        *   passed 	-> ajax -> set_contact()
        *   passed 	-> ajax
        */
		this.save = function()
		{
			// Formular Elemente deaktivieren
			$('#overlay #option-content #contact input').attr('disabled', 'disabled');
			$('#overlay #option-content #contact button').attr('disabled', 'disabled');

			var $data = {}; // Zwischenspeicher für die zu übermittelnden Daten

			for(var $index in $values) // für alle veränderten Values
			{
				$data[$index] = $('#overlay #option-content #contact input[name="'+$index+'"]').val(); // neue Value des Feldes ermitteln
			}


			$.ajax({
	            url: './javascript/ajax/overlay-settings.php',
	            data: {type: 'contact', data: JSON.stringify($data)},
	            type: 'post',
	            success: function($ajaxReturn)
	            {
	                try
	                {
	                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
	                }
	                catch(err) // falls die Umwandlung fehlschlägt
	                {
	                   $this.cancel(); // Formular in Ausgangszustand zurücksetzen
	                   handle_errors('overlay', {'success': false, 'errors': {'6e012': 'Die Kontaktdaten konnte nicht gespeichert werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});
	                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
	                }

	                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
	                {
						$values = {};
						$this.cancel(); // Formular in Ausgangszustand zurücksetzen
						overlay_hint('limegreen', 'Änderungen gespeichert!');
	                }
	                else
	                {
						$this.cancel(); // Formular in Ausgangszustand zurücksetzen
						handle_errors('overlay', $data);
	                }
	            },
	            error: function() // falls Verbindungsprobleme aufgetreten sind
	            {
					$this.cancel(); // Formular in Ausgangszustand zurücksetzen
					handle_errors('overlay', {'success': false, 'errors': {'6e013': 'Auf Grund eines Verbindungsproblems konnten die Kontaktdaten nicht gespeichert werden!'}, 'data': null});
	            }
	        });
		}



	   /* In dieser Funktion können Kriterien für alle Felder, die bearbeitet werden
	   	* können, definiert werden. Somit überprüft diese Funktion den Zustand des
	   	* gesamten Formulars, ob dieses bereits zum Absenden ist. Falls nicht werden
	   	* in der Rückgabe entsprechende Hint-Meldungen bereitgestellt.
        *
        * Version:  1.0.0
        * Stand:    22. November 2015
        *
        * Input:
        *   callback 	: function 	= die Callback-Funktion die zum Ende dieser Funktion aufgerufen wird
        *
        * Success:
        *   array
        *   {
        *   	['success'] = true : boolean
        *   	['errors']	= null / array
        *   	{
        *   		[: string] 	= : string
        *   	}
        *   	['data']	= : boolean
        *   }
        *
        * Failure:
        *   ~success
        *
        * Errors:
        *   none
        */
		this.check = function(callback)
		{
			var $max_checks = 1;
			var $successful_check = 0;
			var $hints = {};


			// ===== E-Mail Validation =====
			var $filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			var $value_mail = $('#overlay #option-content #contact input[name="mail"]').val(); // aktuellen Wert aus dem Feld ermitteln

			if ($filter.test($value_mail)) // sofern der aktuelle Wert dem Filter entspricht
			{
				$successful_check++; // Bedingung als erfüllt zählen
			}
			else
			{
				$hints['mail'] = 'Bitte geben Sie eine g&uuml;ltige E-Mail ein'; // Hint-Meldung bereitstellen
			}


			if($max_checks == $successful_check) // nur wenn alle Bedingungen erfüllt wurden
			{
				if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});} // Formular ist bereit
			}
			else
			{
				if(typeof callback == 'function'){callback({'success': true, 'errors': $hints, 'data': false});} // Eingaben mmüssen noch angepasst werden
			}
		}



		// Event-Handler: Klick auf gesperrte Felder
		$('#overlay #option-content #contact tr.noedit').on('click', function()
    	{
			overlay_hint('red', 'Kann nur durch Kontaktaufnahme bearbeitet werden'); // Hinweis anzeigen um Benutzer zu verdäutlichen, dass er diese Werte niemals selbst bearbeiten darf
    	});



		// Event-Handler: Klick in ein Feld
    	$('#overlay #option-content #contact input').on('click', function()
    	{
			if($values[$(this).attr('name')] == undefined) // sofern der Wert für dieses Feld noch nicht gespeichert wurde
			{
				$values[$(this).attr('name')] = $(this).val(); // Wert unter dem Feld-Namen speichern
			}

			$(this).select(); // alles innerhalb des Feldes markieren um direktes bearbeiten zu ermöglichen
    	});



    	// Event-Handler: beim Tippen in einem Feld
    	$('#overlay #option-content #contact input').keyup(function()
    	{
    		$('#overlay #option-content #contact button').removeAttr('disabled'); // Buttons aktivieren
    	});



    	// Event-Handler: Abbrechen-Button
		$('#overlay #option-content #contact button[name="cancel"]').on('click', function()
    	{
			$this.cancel(); // Formular in Ausgangszustand zurücksetzen
    	});



    	// Event-Handler: Senden-Button
    	$('#overlay #option-content #contact button[name="submit"]').on('click', function()
    	{
    		$this.check(function($check) // Bedingungen des Formulars überprüfen
    		{
    			if($check['data'] == false) // nicht alle Bedingungen wurden erfüllt
    			{
    				if($.type($check['errors']) == 'object') // sofern im Error-Element ein Array übergeben wurde
    				{
    					overlay_hint('red', $check['errors'][Object.keys($check['errors'])[0]]); // erste Meldung als Hint einblenden
					}
    			}
    			else // alle Bedingungen wurden erfüllt
    			{
    				$this.save(); // Änderungen an Server senden
    			}
    		});
    	});



       /* Wird aufgerufen wenn der Content im Overlay gewechselt wird. Die Funktion
       	* entfernt dann alle ihre Event-Handler, versteckt ihren Content und löscht
       	* sich als View-Objekt.
        *
        * Version:  1.0.0
        * Stand:    22. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.delete = function()
		{
			$('#overlay #option-content #contact').find('*').off(); // alle Event-Handler entfernen
			$('#overlay #option-content #contact').hide();
			$overlay_view = null;
		}
	}










   /* ***** KLASSE *****
    * Dieses View-Objekt ist für den Content der Support Verifikation zuständig und beinhaltet
    * alle FUnktionen die für die Verarbeitung dieses Contents benötigt werden.
    *
    * Version:  1.0.0
    * Stand:    18. Dezember 2015
    *
    * Input:
    *   none
    *
    * Function:
    *   void    : cancel()
    *   void    : submit()
    *   void    : delete()
    */
	function overlay_support()
	{
		var $this = this;

		$('#overlay #option-content #support').show();



	   /* Bricht die Eingabe ab und versetzt das Formular seinen Ausgangszustand.
        *
        * Version:  1.0.0
        * Stand:    18. Dezember 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.cancel = function()
		{
			$('#overlay #option-content #support button').removeAttr('disabled'); // Formular Elemente deaktivieren
		}



	   /* Speichert die Antwort des Benutzers in die Datenbank. Hierbei werden auch
	   	* alle nötigen Style-Anpassungen am Formular gemacht.
        *
        * Version:  1.0.0
        * Stand:    18. Dezember 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   6e034 	= "Die Verifikation konnte nicht gespeichert werden, da der Server eine ungültige Antwort geliefert hat!"
        *   6e035 	= "Auf Grund eines Verbindungsproblems konnte die Verifikation nicht gespeichert werden!"
        *   passed 	-> ajax
        */
		this.submit = function($answer)
		{
			$('#overlay #option-content #support button').attr('disabled', 'disabled'); // Formular Elemente deaktivieren

			$.ajax({
	            url: './javascript/ajax/overlay-settings.php',
	            data: {type: 'support', data: JSON.stringify($answer)},
	            type: 'post',
	            success: function($ajaxReturn)
	            {
	                try
	                {
	                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
	                }
	                catch(err) // falls die Umwandlung fehlschlägt
	                {
	                   $this.cancel(); // Formular in Ausgangszustand zurücksetzen
	                   handle_errors('overlay', {'success': false, 'errors': {'6e034': 'Die Verifikation konnte nicht gespeichert werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});
	                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
	                }

	                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
	                {
						overlay_hint('limegreen', 'Verifikation &uuml;bermittelt!');
						$('#overlay #option-menu .option-menu-entry:eq(0)').click(); // zum ersten Menüpunkt wechseln
						$('#overlay #option-menu .option-menu-entry:last').slideUp(500); // Menüpunkt wieder ausblenden
	                }
	                else
	                {
						$this.cancel(); // Formular in Ausgangszustand zurücksetzen
						handle_errors('overlay', $data);
	                }
	            },
	            error: function() // falls Verbindungsprobleme aufgetreten sind
	            {
					$this.cancel(); // Formular in Ausgangszustand zurücksetzen
					handle_errors('overlay', {'success': false, 'errors': {'6e035': 'Auf Grund eines Verbindungsproblems konnte die Verifikation nicht gespeichert werden!'}, 'data': null});
	            }
	        });
		}



		// Event-Handler: Abbrechen-Button
		$('#overlay #option-content #support button[name="cancel"]').on('click', function()
    	{
			$this.submit("false");
    	});



    	// Event-Handler: Abbrechen-Button
		$('#overlay #option-content #support button[name="submit"]').on('click', function()
    	{
			$this.submit("true");
    	});



	   /* Wird aufgerufen wenn der Content im Overlay gewechselt wird. Die Funktion
       	* entfernt dann alle ihre Event-Handler, versteckt ihren Content und löscht
       	* sich als View-Objekt.
        *
        * Version:  1.0.0
        * Stand:    18. Dezember 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.delete = function()
		{
			$('#overlay #option-content #support').find('*').off(); // alle Event-Handler entfernen
			$('#overlay #option-content #support').hide();
			$overlay_view = null;
		}
	}









	function overlay_ohours()
	{
		$('#overlay #option-content #ohours').show();

		this.delete = function()
		{
			$('#overlay #option-content #ohours').find('*').off(); // alle Event-Handler entfernen
			$('#overlay #option-content #ohours').hide();
			$overlay_view = null;
		}
	}

    var $broadcasts = new Array(); // enthält die Verweise zu allen vorhandenen Broadcast-Objekten



   /* Lädt alle vorhandenen Broadcasts. Sofern eine Stations ID (Cookie)
    * vorhanden ist, werden auch die Stations-Broadcasts geladen.
    *
    * Version:  1.0.2
    * Stand:    13. September 2015
    *
    * Input:
    *   callback    : function  = wird zum Ende der Funktion aufgerufen
    *
    * Success:
    *   call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => passed -> ajax -> fetch_broadcasts()
    *   }
    *
    * Failure:
    *   call->callback(array
    *   {
    *       ['success'] => false : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => null
    *   }
    *
    * Errors:
    *   passed -> ajax -> fetch_broadcasts()
    *
    *   3e001   =>  "Auf Grund eines Verbindungsproblems konnten die Broadcasts nicht überprüft werden!"
    */
    function fetch_broadcasts(callback)
    {
        $.ajax({
            url: './javascript/ajax/broadcasts.php',
            data: {func: 'fetch_broadcasts'},
            type: 'post',
            success: function($ajaxReturn)
            {
                try
                {
                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
                }
                catch(err) // falls die Umwandlung fehlschlägt
                {
                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'3e002': 'Die Abfrage der Broadcasts ist fehlgeschlagen, da der Server eine ung&uuml;ltige Antwort gelifert hat!'}, 'data': $ajaxReturn});}
                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
                }

                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
                {
                    if(typeof callback == 'function'){callback({'success': true, 'errors': $data['errors'], 'data': $data['data']});}
                }
                else
                {
                    if(typeof callback == 'function'){callback({'success': false, 'errors': $data['errors'], 'data': null});}
                }
            },
            error: function()
            {
                if(typeof callback == 'function'){callback({'success': false, 'errors': {'3e001': 'Auf Grund eines Verbindungsproblems konnten die Broadcasts nicht &uuml;berpr&uuml;ft werden!'}, 'data': null});}
            }
        });
    }





   /* Verarbeitet die Rohdaten der geladenen Broadcasts und überprüft
    * ob ein Broadcast noch angezeigt werden soll oder nicht. Zudem
    * wird ein Update vorhandener Broadcasts gestartet.
    *
    * Version:  1.0.0
    * Stand:    26. Januar 2015
    *
    * Input:
    *   $data       : array(array('id' => : integer, 'title' => : string, 'type' => : string, 'content' => : string))
    *   callback    : function  = wird zum Ende der Funktion aufgerufen
    *
    * Success:
    *   call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null
    *       ['data']    => true : boolean
    *   }
    *
    * Failure:
    *   ~Success
    *
    * Errors:
    *   none
    */
    function handle_broadcasts($data, callback)
    {
        var $alive = new Array(); // speichert die IDs der noch vorhandenen Meldungen

        if($data != null) // nur wenn Daten übergeben wurden
        {
            for(var $i = 0; $i < $data.length; $i++) // für jeden geladenen Broadcast
            {
                var $is_new = true; // markiert ob der Broadcast neu ist

                for(var $n = 0; $n < $broadcasts.length; $n++) // für jeden bereits vorhandenen Broadcast
                {
                    if($broadcasts[$n].get_id() == $data[$i]['id']) // wenn geladener Broadcast mit vorhandenem Broadcast übereinstimmt
                    {
                        $broadcasts[$n].update($data[$i]['title'], $data[$i]['type'], $data[$i]['content']); // Update triggern
                        $alive.push($data[$i]['id']); // Broadcast als "immer noch vorhanden" markieren
                        $is_new = false; // ist kein neuer Broadcast, da schon vorhanden
                        break; // Schleife kann beendet werden, da eine Übereinstimmung gefunden wurde
                    }
                }

                if($is_new == true) // nur wenn der Broadcast neu ist
                {
                    $alive.push($data[$i]['id']); // Broadcast ebenfalls als "immer noch vorhanden" markieren, da er sonst direkt wieder entfernt wird
                    $broadcasts.push(new broadcast($data[$i]['id'], $data[$i]['title'], $data[$i]['type'], $data[$i]['content'])); // Objekt erzeugen und abspeichern
                }
            }
        }


        for(var $i = 0; $i < $broadcasts.length; $i++) // für jeden vorhandenen Broadcast
        {
            var $dead = true; // markiert ob der Broadcast gelöscht werden soll

            for(var $n = 0; $n < $alive.length; $n++) // für jeden als "immer noch vorhanden" Broadcast
            {
                if($broadcasts[$i].get_id() == $alive[$n]) // wenn Broadcast als "immer noch vorhanden" markiert wurde
                {
                    $dead = false; // Markierung entfernen
                    $alive.splice($n, 1); // ID kann aus Array entfernt werden, da verarbeitet
                    break;
                }
            }

            if($dead == true) // wenn Meldung noch zum Löschen markiert ist
            {
                $broadcasts[$i].remove(); // Meldung löschen
                $broadcasts.splice($i, 1); // Verweis auf Objekt entfernen und damit das Objekt freigeben
            }
        }

        if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
    }





   /* ***** KLASSE *****
    * Jedes Broadcast-Objekt steht für eine Meldung im DOM und liefert
    * die zugehörigen Funktionen um das Objekt ein- bzw. auszublenden.
    * Hinzu kommt die Möglichkeit die Meldung zu aktualisieren usw.!
    *
    * Version:  1.0.0
    * Stand:    26. Januar 2015
    *
    * Input:
    *   $id         : integer   = die ID der Meldung
    *   $title      : string    = der Titel der Meldung
    *   $type       : string    = der Anzeige-Typ der Meldung
    *   $content    : string    = der Text der Meldung
    *
    * Function:
    *   void    : show(callback : function)
    *   void    : hide(callback : function)
    *   void    : remove(callback : function)
    *   void    : update($title : string, $type : string, $content : string)
    *   integer : get_id()
    */
    function broadcast($id, $title, $type, $content)
    {
        // Objekt zusammenbauen
        var $object = $(
        '<div class="notification ' + $type + '" style="display: none">' +
            '<img src="./images/' + $type + '.png" />' +
            '<div>' +
                '<h1>' + $title + '</h1><h5 class="br">Broadcast ID: ' + $id + '</h5>' +
                '<p>' + $content + '</p>' +
            '</div>' +
        '</div>');

        $('#content #notifications #broadcasts').prepend($object); // Objekt im DOM einfügen



       /* Blendet das Broadcast-Objekt ein.
        *
        * Version:  1.0.0
        * Stand:    26. Januar 2015
        *
        * Input:
        *   callback    : function  = wird zum Ende der Funktion aufgerufen
        *
        * Success:
        *   call -> callback();
        *
        * Failure:
        *   ~Success
        *
        * Errors:
        *   none
        */
        this.show = function(callback)
        {
            $.when($object.slideDown(1000)).then(function() // wenn die Meldung eingeblendet wurde
            {
                if(typeof callback == 'function'){callback();}
            });
        }



       /* Blendet das Broadcast-Objekt wieder aus.
        *
        * Version:  1.0.0
        * Stand:    26. Januar 2015
        *
        * Input:
        *   callback    : function  = wird zum Ende der Funktion aufgerufen
        *
        * Success:
        *   call -> callback();
        *
        * Failure:
        *   ~Success
        *
        * Errors:
        *   none
        */
        this.hide = function(callback)
        {
            $.when($object.slideUp(1000)).then(function() // wenn die Meldung ausgeblendet wurde
            {
                if(typeof callback == 'function'){callback();}
            });
        }



       /* Entfernt das Broadcast-Objekt aus dem DOM und leert alle Variabeln.
        * Damit ist dieses Objekt "gelöscht" und jede Referenz zu ihm sollte
        * aufgehoben werden.
        *
        * Version:  1.0.0
        * Stand:    26. Januar 2015
        *
        * Input:
        *   callback    : function  = wird zum Ende der Funktion aufgerufen
        *
        * Success:
        *   call -> callback();
        *
        * Failure:
        *   ~Success
        *
        * Errors:
        *   none
        */
        this.remove = function(callback)
        {
            this.hide(function() // wenn die Meldung ausgeblendet wurde
            {
                $object.remove(); // Objekt aus dem DOM entfernen
                $id = $title = $type = $content = $object = undefined; // Variabeln löschen

                if(typeof callback == 'function'){callback();}
            });
        }



       /* Aktualisiert, falls nötig, die wichtigsten Elemente derBroadcast-Meldung.
        * Sofern der Titel, Type oder Content verändert wurde, wird das Objekt
        * ausgeblendet, aktualisiert und wieder eingeblendet.
        *
        * Version:  1.0.0
        * Stand:    26. Januar 2015
        *
        * Input:
        *   callback    : function  = wird zum Ende der Funktion aufgerufen
        *
        * Success:
        *   call -> callback();
        *
        * Failure:
        *   ~Success
        *
        * Errors:
        *   none
        */
        this.update = function($newTitle, $newType, $newContent, callback)
        {
            if($title != $newTitle || $type != $newType || $content != $newContent) // falls sich ein Wert verändert hat
            {
                var $this = this; // Referenz auf dieses Objekt speichern

                this.hide(function() // wenn Meldung ausgeblendet wurde
                {
                    $object.children('div').children('h1').html($newTitle); // Titel erneuern
                    $object.children('div').children('p').html($newContent); // Content erneuern
                    $object.children('img').attr('src', './images/' + $newType + '.png'); // Image der Meldung erneuern
                    $object.removeClass($type); // alte Klasse entfernen
                    $object.addClass($newType); // neue Klasse setzen

                    // Variabeln neu befüllen
                    $title = $newTitle;
                    $type = $newType;
                    $content = $newContent;

                    setTimeout(function() // kurze kosmetische Pause
                    {
                        $this.show(function() // wenn die Meldung eingeblendet wurde
                        {
                            if(typeof callback == 'function'){callback();}
                        });
                    }, 1500);
                });
            }
        }



        this.show(); // Meldung nach Erzeugung einblenden



        // ***** ***** Getter Funktionen ***** *****

        this.get_id = function()
        {
            return $id;
        }
    }
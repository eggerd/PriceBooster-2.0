
    var $debug_click_count = 0; // zählt die Klicks auf das Icons
    var $debug_click_timer = 0; // 0 = außerhalb des Zeitlimits, 1 = innerhalb des Zeitlimits
    var $debug_last_tab = {'runtime': 0, 'authentication': 0, 'broadcasts': 0, 'properties': 0, 'database': 0, 'overlay': 0}; // letzte verwendete Tab-Ebene je Bereich speichern
    var $debug_click_timeout = 0; // speichert die Referenz auf den setTimeout() für Klicks





   /* Erzeugt einen neuen Eintrag im Debug-Log mit dem angegebenen Text, Einrückung
	* und Typen.
	*
	* Version: 	2.0.1
	* Stand: 	14. Oktober 2015
   	*
	* Input:
	* 	$area	: string 	= die Bereich, zu dem dieser Eintrag gehört
    *   $tab    : integer   = die Anzahl der Einrückungen für diesen Eintrag
    *   $type   : string    = gibt die Art der Meldung an (info|warning|error)
    *   $text   : string    = die Nachricht des Eintrags
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function debug($area, $tab, $type, $text)
    {
        if(typeof $text == 'object') // falls ein Array oder Objekt als Text übergeben wird
        {
            $text = JSON.stringify($text); // Array bzw. Objekt in Textform umwandeln
        }

        if($tab == null)
        {
        	$tab = $debug_last_tab[$area];
        }
        else
        {
	        if($tab < 0)
	    	{
				$tab = 0;
	    	}

	    	$debug_last_tab[$area] = $tab; // den zuletzt verwendeten Tab speichern
    	}


        // Debug-Eintrag zusammenbauen
        var $entry =
        '<div class="entry">' +
            '<p class="time">' + time() + '</p>' +
            '<p class="type">' + $type + '</p>' +
            '<div class="text"><p class="readable" style="margin-left: ' + 15 * $tab + 'px;">' + $text + '</p></div>' +
        '</div>';

        $('#debug #logs #log_' + $area).prepend($entry); // Debug-Eintrag im  entsprechenden Log einfügen


        var $log_count = parseInt($('#debug #selection #sel_' + $area + ' span').html()); // versuchen die Anzahl der Einträge aus dem Debug-Menü auszulesen

        if(isNaN($log_count)) // nur falls die Anzahl für diesen Bereich bereits gesetzt wurden
        {
        	$('#debug #selection #sel_' + $area).append(' [<span></span>]'); // Span für die Anzahl der Einträge einfügen
        	$('#debug #selection #sel_' + $area + ' span').html($('#debug #logs #log_'+ $area + ' .entry').length); // Anzahl der Einträge ermitteln und einfügen
        }
        else
        {
        	$('#debug #selection #sel_' + $area + ' span').html(($log_count + 1)); // Anzahl der Einträge für dieses Log hochzählen
        }

        if($('#debug').is(':visible')) // falls das Log zurzeit sichtbar ist
        {
			$('#debug').perfectScrollbar('update'); // jQuery Scrollbar aktualisieren
        }
    }





   /* Hängt den angegebenen Text an den letzten Debug-Eintrag an, der dem Log
    * hinzugefügt wurde.
    *
    * Version:  1.1.0
    * Stand:    22. September 2015
    *
    * Input:
    * 	$area	: string 	= die Bereich, zu dem dieser Eintrag gehört
    *   $text   : string    = die Nachricht die an den letzten Eintrag angehängt werden soll
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function debug_append($area, $text)
    {
        $('#debug #logs #log_' + $area + ' .entry:first div p.readable').append($text); // Meldung an den letzten Eintrag anhängen
    }





   /* Fügt dem letzem Log-Eintrag ein spezielles Datenelement hinzu, welches zum
    * visualisieren verarbeiteter Daten gedacht ist. Die Daten sind nur bei Hover
    * des Elements verfügbar.
    *
    * Version:  1.1.0
    * Stand:    22. September 2015
    *
    * Input:
    * 	$area		: string 	= die Bereich, zu dem dieser Eintrag gehört
    *   $data       : mixed     = die Daten die visualisiert werden sollen
    *   $caption    : string    = optionaler Titel der dem Hover-Effekt hinzugefügt wird (als Art kurze Beschreibung)
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function debug_append_data($area, $data, $caption)
    {
		var $entry = $('<p class="data">[<span>data</span>]</p>'); // DOM-Objekt erzeugen
        $data = (($caption != undefined) ? $caption : '') + JSON.stringify($data); // übergebene Daten in Stringformat umwandeln und ggf. Titel hinzufügen

        $entry.attr('title', $data); // Titel setzen - wird nicht direkt beim Erzeugen des Objekts hinzugefügt, da sonst HTML-Special-Chars nicht escaped werden
        $('#debug #logs #log_' + $area + ' .entry:first div').append($entry); // Daten-Element dem letzten Log-Eintrag hinzufügen
    }





   /* Baut einen String aus der aktuellen Uhrzeit, inkl. Millisekunden zusammen, wobei
    * jede Uhrzeit die gleich Länge erhält.
    *
    * Version:  1.0.0
    * Stand:    11. August 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   : string
    *
    * Failure:
    *   ~Success
    *
    * Errors:
    *   none
    */
    function time()
    {
        var $date = new Date();
        return precision($date.getHours()) + ':' + precision($date.getMinutes()) + ':' + precision($date.getSeconds()) + ':' + precision($date.getMilliseconds(), 3);
    }





   /* Wandelt einen übergebenen Timestamp in eine lesbare Uhrzeit um,
    * um z.B. Timeout-Timestamps im Debug-Log visualisieren zu können.
    *
    * Version:  1.0.0
    * Stand:    11. August 2015
    *
    * Input:
    *   $timestamp  : integer   = der Timestamp der umgewandelt werden soll
    *
    * Success:
    *   : string
    *
    * Failure:
    *   ~Success
    *
    * Errors:
    *   none
    */
    function date($timestamp)
    {
        var $date = new Date($timestamp);
        return precision($date.getHours()) + ':' + precision($date.getMinutes()) + ':' + precision($date.getSeconds()) + ':' + precision($date.getMilliseconds(), 3);
    }





   /* Trimmt die Anzahl der Nachkommastellen der übergebenen Zahl auf die
    * gewünschte Länge.
    *
    * Version:  1.0.0
    * Stand:    11. August 2015
    *
    * Input:
    *   $input      : double    = die Zahl deren Nachkommastellen getrimmt werden sollen
    *   $precision  : integer   = die Anzahl der benötigten Nachkommastellen
    *
    * Success:
    *   : double
    *
    * Failure:
    *   ~Success
    *
    * Errors:
    *   none
    */
    function precision($input, $precision)
    {
        var $return = $input;

        if($precision == undefined || $precision < 1) // falls keine Präzision angegeben wurde, oder diese kleiner 1 ist
        {
            $precision = 1; // Präzision auf 1 setzen
        }
        else
        {
            $precision--; // Präzision um 1 verringern
        }

        for(var $i = 1; $i <= $precision; $i++) // für die Anzahl der benötigten Nachkommastellen
        {
            if($input < Math.pow(10, $i)) // wenn die übergebene Zahl noch kleiner von 10^i ist
            {
                $return = '0' + $return; // dem Rückgabestring eine Null hinzufügen
            }
        }

        return $return;
    }





   /* Wird bei jedem Klick auf das Icon aufgerufen und entscheidet somit nach
    * wie vielen Klicks das Debug-Log eingeblendet wird.
    *
    * Version:  1.0.3
    * Stand:    14. Oktober 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function toggle_debug_visibility()
    {
        clearTimeout($debug_click_timeout); // Klick-Timeout zurücksetzen

        if($('#debug').is(':visible')) // falls das Log bereits sichtbar ist
        {
            $('#debug').hide(); // ausblenden

            // Klick-Variabeln zurücksetzen, damit die Funktion nach dem Ausblenden wieder funktioniert wie vorher
            $debug_click_count = 0;
    		$debug_click_timer = 0;
        }
        else // Log ist aktuell ausgeblendet
        {
            if($debug_click_count >= ($DEBUG_CLICKS_TO_SHOW - 1)) // wurde die benötigte Anzahl Klicks erreicht
            {
                if($debug_click_timer == 1) // sofern das Zeitlimit noch nicht abgelaufen ist
                {
                    $('#debug').show(); // Log einblenden
                    $('#header #caption img').css({transform: ''}); // Icon-Rotation zurücksetzen
                    $('#debug').perfectScrollbar(); // jQuery Scrollbar einblenden
                }
                else
                {
                    $debug_click_count = 1; // Klick-Counter zurücksetzen, da das Zeitlimit abgelaufen war
                }
            }
            else
            {
                $debug_click_count++; // Klick-Counter erhöhen
                $debug_click_timer = 1; // Klick als innerhalb des Zeitlimits markieren

                $('#header #caption img').css({transform: 'rotate(' + ($debug_click_count * 90) + 'deg)'}); // Icon um 90° drehen, um den Klick zu visualisieren

                $debug_click_timeout = setTimeout(function() // falls kein neuer Klick auftritt, Daten zurücksetzen
                {
                    $debug_click_timer = 0; // Klick-Counter zurücksetzen
                    $debug_click_count = 0; // Timer als abgelaufen markieren
                    $('#header #caption img').css({transform: ''}); // Icon-Rotation zurücksetzen
                }, 3000);
            }
        }
    }





   /* Sendet alle noch nicht übermittelten Log-Einträge an den Server, damit dieser
   	* die Einträge in einer Log-Datei abspeichern kann.
   	*
   	* Version: 	2.0.1
   	* Stand: 	31. Dezember 2015
   	*
   	* Input:
   	* 	none
   	*
   	* Success:
   	* 	none
   	*
   	* Failure:
   	* 	none
   	*
   	* Errors:
   	*	passed 	-> ajax -> debug_save_dump()
   	*	0e006 	=> "Auf Grund von Verbindungsproblemen konnte der Log-Dump nicht übermittelt werden!"
   	*	0e007 	=> "Der Server hat eine ungültige Antwort auf den letzten Log-Dump gesendet!"
    */
    function dump_debug_log()
    {
    	if($UPDATING == undefined || $UPDATING == false)
    	{
	    	var $new_entries = {}; // leeres Objekt zum Zwischenspeichern aller zu sendenden Einträge speichern
	    	var $something_to_submit = false; // ob Einträge zum Senden gefunden wurden

	    	$('#debug #logs').children().each(function() // für jeden Bereich im Debug-Log
	    	{
	    		var $log_area = $(this).attr('id'); // ID des Bereichs ermitteln
	    		var $log_entries = $('#debug #logs #' + $log_area + ' .entry:not(:data(dumped)), #debug #logs #' + $log_area + ' .entry:not(:data(queued))'); // noch nicht gesendete Einträge aus diesem Bereich ermitteln

	    		if($log_entries.length > 0) // nur falls auch Einträge gefunden wurden, die noch nicht gesendet wurden
	    		{
	    			$new_entries[$log_area.split('_')[1]] = $log_entries; // alle jene Einträge im Objekt unter der ID (ohne Präfix) des Bereichs speichern
	    			$something_to_submit = true; // markieren, dass Einträge zum Senden gefunden wurden
	    		}
			});


			// ist die Anzahl der bereits an den Server gesendeten Einträge größer als die maximal
			// zu speichernde Anzahl an Einträgen, werden alle bereits gesendeten Einträge gelöscht
			if($('#debug .entry:data(dumped)').length >= $DEBUG_MAX_ENTRIES)
			{
				// Debug-Eintrag zusammenbauen
		        var $entry =
		        '<div class="entry">' +
		            '<p class="time">' + time() + '</p>' +
		            '<p class="type">info</p>' +
		            '<div class="text"><p class="readable" style="margin-left: 0px;">Debug log trimmed</p></div>' +
		        '</div>';

		        $('#debug #logs').children().each(function() // für jeden Bereich im Debug-Log
				{
					$(this).children('.entry:data(dumped)').remove(); // bereits gesendete Einträge dieses Bereichs löschen
					$(this).append($entry); // Debug-Eintrag im Log einfügen

			        var $area = $(this).attr('id').split('_')[1];
			        var $log_count = parseInt($('#debug #selection #sel_' + $area + ' span').html()); // versuchen die Anzahl der Einträge aus dem Debug-Menü auszulesen

			        if(!isNaN($log_count)) // nur falls die Anzahl für dieses Log bereits gesetzt wurde
			        {
			        	$('#debug #selection #sel_' + $area + ' span').html($('#debug #logs #log_'+ $area + ' .entry').length); // Anzahl der Einträge ermitteln und einfügen
			        }
			    });
			}


			if($something_to_submit == true) // wenn sich im Log neue unverarbeitete Einträge befinden
	    	{
	    		var $dump = {}; // speichert die Daten für alle zusendenden Einträge

	    		$.each($new_entries, function($index, $value) // für jeden Bereich im Zwischenspeicher
	    		{
	    			$dump[$index] = new Array(); // Bereich im Ajax-Array anlegen

	    			$value.each(function() // für jeden Eintrag dieses bereichs
	    			{
                        var $data = null;
		    			if($(this).find('.text .data').length) // sofern der Eintrag ein Datenelement besitzt
			    		{
			    			$data = $(this).find('.text .data').attr('title'); // Inhalt des Datenelements speichern
			    		}

			    		// Daten aus dem Eintrag auslesen und als Array in das Dump-Array schreiben
			    		$dump[$index].push([$(this).children('.time').html(), $(this).children('.type').html(), $(this).find('.text .readable').css('margin-left'), $(this).find('.text .readable').html(), $data]);
			    		$(this).data('queued', true); // Eintrag als Queued markieren
		    		});
	    		});


				$.ajax({
		            url: './javascript/ajax/debug.php',
		            data: {dump: JSON.stringify($dump)},
		            type: 'post',
		            success: function($ajaxReturn)
		            {
		                try
		                {
							var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln

							if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
							{
							    $('#debug .entry:data(queued)').data('dumped', true); // gesendete Einträge als Dumped markieren
							    $('#debug .entry:data(queued) .entry:data(dumped)').removeData('queued'); // Queued-Flag entfernen
							}
							else
							{
								$('#debug .entry:data(queued)').removeData('queued'); // Queued-Flag entfernen, damit die Einträge beim nächsten Aufruf wieder gesendet werden

								for(var $i in $data['errors']) // für jeden übermittelten Error
							    {
							        debug('properties', 0, 'error', $i + ' | ' + $return['errors'][$i]);
							    }
							}
		                }
		                catch(err) // falls die Umwandlung fehlschlägt
		                {
		                   debug('properties', 0, 'error', '0e007 | Der Server hat eine ung&uuml;ltige Antwort auf den letzten Log-Dump gesendet!');
		                   debug_append_data('runtime', $ajaxReturn);

		                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
		                }
		            },
		            error: function()
		            {
		            	debug('properties', 0, 'error', '0e006 | Auf Grund von Verbindungsproblemen konnte der Log-Dump nicht &uuml;bermittelt werden!');
		            }
		        });
	    	}
    	}
    }





   /* Ist lediglich dafür zuständig den gewünschten Bereich des Debug-Logs einzublenden und den
   	* jeweiligen Menüpunkt entsprechend zu markieren.
   	*
   	* Version: 	1.0.0
   	* Stand: 	24. September 2015
   	*
   	* Input:
   	* 	$selection 	: object 	= die Referenz auf den angeklickten Menüpunkt im Debug-Log
   	*
   	* Success:
   	* 	none
   	*
   	* Failure:
   	* 	none
   	*
   	* Errors:
   	* 	none
    */
    function debug_switch_log($selection)
    {
    	$('#debug #selection p').removeClass('selected'); // Aktivierung des aktuellen Menüpunkts entfernen
    	$($selection).addClass('selected'); // angeklickten Menüpunkt als aktiv markieren

    	$('#debug #logs div').removeClass('selected'); // aktuellen Bereich ausblenden
    	$('#debug #logs #log_' + $($selection).attr('id').split('_')[1]).addClass('selected'); // gewünschten Bereich einblenden
    }
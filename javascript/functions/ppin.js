
   /* ***** KLASSE *****
    * Dieses Objekt ist für den Ablauf von PPIN-Abfragen zuständig. Hierzu gehört die
    * Animation des Formulars und das überprüfen des PPIN.
    *
    * Version:  1.0.0
    * Stand:    17. Dezember 2015
    *
    * Input:
    *   none
    *
    * Function:
    *   void    : cancel()
    *   void    : submit()
    *   void    : delete()
    */
	function overlay_reqppin($object, callback)
	{
		var $this = this;

		$object.fadeIn(500); // zugehörigen Content einblenden



	   /* Bricht die Eingabe ab und versetzt das Formular sowie dieses Objekt in
	   	* seinen Ausgangszustand.
        *
        * Version:  1.0.0
        * Stand:    17. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
		this.cancel = function()
		{
			$object.find('input').removeAttr('disabled');
			$object.find('input').val('');
			$object.find('input:first').focus();
		}



	   /* Sendet den eingegebenen PPIN an den Server um diesen überprüfen zu lassen.
	   	* Je nach Antwort des Servers wird dann ggf. der angefragte Content freigegeben.
        *
        * Version:  1.0.0
        * Stand:    17. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   6e017 	= "Der Pächter-Pin konnte nicht überprüft werden, da der Server eine ungültige Antwort geliefert hat!"
        *   6e018	= "Auf Grund eines Verbindungsproblems konnten der Pächter PIN nicht überprüft werden!"
        *   6e030	= "Der Pächter-Pin konnte nicht überprüft werden!"
        *   passed	-> ajax
        */
		this.submit = function()
		{
			var $value = overlay_ppin_value($object.find('input')); // Value der Eingabe ermitteln

			if($value.length == 4) // sofern die zweite Eingabe mit der vorherigen übereinstimmt
			{
				// Formular deaktivieren
				$object.find('input, button').attr('disabled', 'disabled');

				$.ajax({
		            url: './javascript/ajax/ppin.php',
		            data: {data: JSON.stringify($value)},
		            type: 'post',
		            success: function($ajaxReturn)
		            {
		                try
		                {
		                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
		                }
		                catch(err) // falls die Umwandlung fehlschlägt
		                {
		                   $this.cancel(); // Formular in Ausgangszustand zurücksetzen
		                   handle_errors('overlay', {'success': false, 'errors': {'6e017': 'Der P&auml;chter-Pin konnte nicht &uuml;berpr&uuml;ft werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});
		                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
		                }

		                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
		                {
							$this.cancel();

							if($data['data'] == true)
							{
								$this.delete();
								if(typeof callback == 'function'){callback();}
							}
							else
							{
								overlay_hint('red', 'Der eingegebene PIN ist nicht korrekt!');
							}
		                }
		                else
		                {
							$this.cancel(); // Formular in Ausgangszustand zurücksetzen
							handle_errors('overlay', {'success': false, 'errors': {'6e030': 'Der P&auml;chter-Pin konnte nicht &uuml;berpr&uuml;ft werden!'}, 'data': $data});
							handle_errors('overlay', $data);
		                }
		            },
		            error: function() // falls Verbindungsprobleme aufgetreten sind
		            {
						$this.cancel(); // Formular in Ausgangszustand zurücksetzen
						handle_errors('overlay', {'success': false, 'errors': {'6e018': 'Auf Grund eines Verbindungsproblems konnten der P&auml;chter PIN nicht &uuml;berpr&uuml;ft werden!'}, 'data': null});
		            }
		        });
			}
			else // die Eingaben stimmen nicht überein
			{
				$this.cancel();
				overlay_hint('red', 'Der PIN muss aus vier Zeichen bestehen!');
			}
		}



		// Event-Handler: Input Felder
    	$object.find('input').keyup(function($key)
    	{
    		overlay_ppin_keyup(this, $key, function($data)
    		{
    			if($data['data'] == 4) // sofern wir beim letzten Feld angekommen sind und dies die ERSTE Eingabe ist
	    		{
	    			$object.find('button[name="submit"]').removeAttr('disabled'); // Senden Button aktivieren
	    		}
	    		else
	    		{
	    			$object.find('button[name="submit"]').attr('disabled', 'disabled'); // Senden Button deaktivieren, falls wieder ein Feld gelöscht wird
	    		}
    		});
    	});



    	// Event-Handler: Senden-Button
    	$object.find('button[name="submit"]').on('click', function()
    	{
    		$this.submit();
    	});



       /* Wird aufgerufen wenn der Content im Overlay gewechselt wird. Die Funktion
       	* entfernt dann alle ihre Event-Handler, versteckt ihren Content und löscht
       	* sich als View-Objekt.
        *
        * Version:  1.0.0
        * Stand:    17. November 2015
        *
        * Input:
        *   none
        *
        * Success:
        *   none
        *
        * Failure:
        *   none
        *
        * Errors:
        *   none
        */
    	this.delete = function()
		{
			$object.find('*').off(); // alle Event-Handler entfernen
			$object.fadeOut(500);
			$overlay_view = null;
		}
	}

    var $references = new Array(); // enthält die Verweise zu allen vorhandenen Referenz-Objekten



   /* Über diese Funktion werden neue Referenz-Objekte erzeugt, alte gelöscht
    * oder falls nötig aktualisiert. Hierzu wird an die Funktion das Data-Array
    * der update_price_data()-Funktion übergeben. Die übergeben Daten werden
    * zudem auf Spezial-Meldungen (wie z.B. die Geschlossen-Meldung) durchsucht
    * um dann dementsprechend darauf zu reagieren.
    *
    * Version:  1.0.2
    * Stand:    20. August 2015
    *
    * Input:
    *   $data   : array
    *   {
    *       [: integer] => array
    *       {
    *           ['id']      => : integer
    *           ['image']   => : string
    *           ['prices']  => : array
    *           {
    *               [: integer] => : integer
    *               ...
    *           }
    *       }
    *       ...
    *   }
    *   callback    : function
    *
    * Success:
    *   call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    =>  	: mixed		= falls nicht TRUE, wurde ein Magic-Error in den Preisdaten erkannt. Es wird ggf. die Zeit für einen Timeout zurückgegeben
    *   }
    *
    * Failure:
    *   ~Success
    *
    * Errors:
    *   none
    */
    function handle_price_data($data_array, callback)
    {
        var $data = $data_array['data'];
        var $alive = new Array(); // speichert die IDs der noch vorhandenen Referenzen

        var $special_state = false; // bei besonderen Zuständen (geschlossen, Rise-Timeout) != false
        var $state_meta = null; // enthält ggf. zusätzliche Daten zu einem Magic-Error

        debug('runtime', 3, 'info', 'Checking for Magic-Error 2n013 (station is closed)');

        if($data_array['errors'] != null && '2n013' in $data_array['errors']) // falls der Magic-Error 2n013 zurückgegeben wird (Station hat geschlossen)
        {
            $special_state = 'closed'; // Station als geschlossen markieren
            debug_append('runtime', ' => true');
        }
        else // der Magic-Error 2n013 wurde nicht vom Server zurückgegeben (Station hat geöffnet)
        {
            debug_append('runtime', ' => false');
            debug('runtime', 3, 'info', 'Checking for message 2n014 & 2n015 (price rise timeout)');

        	if($data_array['errors'] != null && ('2n014' in $data_array['errors'] || '2n015' in $data_array['errors'])) // falls die "Fehlermeldung" 2n014 oder 2n015 zurückgegeben wird (Timeout wegen Preiserhöhung)
        	{
        		$special_state = 'rise'; // Station als "im Timeout" markieren
        		$state_meta = $data_array['data']; // Zusatzdaten für din Magic-Error speichern
            	debug_append('runtime', ' => true');
        	}
        	else // es wurde keine Magic-Errors gefunden
        	{
		        if($data != null) // nur wenn Preis-Daten übergeben wurden
		        {
		            debug('runtime', 3, 'info', 'New prices available');

		            for(var $i = 0; $i < $data.length; $i++) // für jeden geladenen Referenz
		            {
		                debug('runtime', 4, 'info', 'Processing prices of reference with id #' + $data[$i]['id']);
		                debug('runtime', 5, 'info', 'Checking if reference is already displayed');

		                var $is_new = true; // markiert ob die Referenz neu ist

		                for(var $n = 0; $n < $references.length; $n++) // für jede bereits vorhandene Referenz
		                {
		                    if($references[$n].get_id() == $data[$i]['id']) // wenn die geladene Referenz mit vorhandener Referenz Übereinstimmt
		                    {
		                        debug_append('runtime', ' => true');
		                        debug('runtime', 6, 'info', 'Trigger update of reference data');

		                        $references[$n].update($data[$i]['image'], $data[$i]['identifier'], $data[$i]['prices']); // Update triggern
		                        $alive.push($data[$i]['id']); // Referenz als "immer noch vorhanden" markieren
		                        $is_new = false; // ist keine neue Referenz, da schon vorhanden
		                        break; // Schleife kann beendet werden, da eine Übereinstimmung gefunden wurde
		                    }
		                }

		                if($is_new == true) // nur wenn die Referenz neu ist
		                {
		                    debug_append('runtime', ' => false');
		                    debug('runtime', 6, 'info', 'Creating new reference object');

		                    $alive.push($data[$i]['id']); // Referenz ebenfalls als "immer noch vorhanden" markieren, da er sonst direkt wieder entfernt wird
		                    $references.push(new reference($data[$i]['id'], $data[$i]['image'], $data[$i]['identifier'], $data[$i]['prices'])); // Objekt erzeugen und abspeichern
		                }
		            }
		        }
		        else // es wurden keine Preis-Daten übergeben
		        {
		            debug('runtime', 3, 'info', 'No price data for handling passed');
		        }
	        }
        }


        /* Überprüft alle vorhandenen Referenzen, ob diese noch benötigt werden. Die Funktion
         * simuliert eine Schleife, an die jedoch eine Callback-Funktion übergeben werden kann.
         * Jeder Aufruf ist somit ein Loop der dann wiederrum einen neuen Loop aufruft, allerdings
         * mit inkrementiertem Index. Zum Ende wird die Callback-Funktion aufgerufen.
         */
        var callbackLoop_GarbageCollector = function($i, callback)
        {
            if($i < $references.length) // maximal bis das Ende der Arrays erreicht wurde
            {
                debug('runtime', 4, 'info', 'Checking reference with id #' + $references[$i].get_id() + ', at index #' + $i);

                var $dead = true; // markiert ob die Referenz gelöscht werden soll

                for(var $n = 0; $n < $alive.length; $n++) // für jede als "immer noch vorhanden" markierte Referenz
                {
                    if($references[$i].get_id() == $alive[$n]) // wenn die Referenz als "immer noch vorhanden" markiert wurde
                    {
                        debug_append('runtime', ' => in use');

                        $dead = false; // Markierung entfernen
                        $alive.splice($n, 1); // ID kann aus Array entfernt werden, da verarbeitet
                        break;
                    }
                }

                if($dead == true) // wenn die Referenz noch zum Löschen markiert ist
                {
                    debug_append('runtime', ' => unused');

                    $references[$i].remove(function() // Referenz löschen, da nicht mehr benötigt
                    {
                        debug('runtime', 5, 'info', 'Reference has been removed');

                        $references.splice($i, 1); // Verweis auf Objekt entfernen und damit das Objekt freigeben

                        callbackLoop_GarbageCollector($i, function() // rekursiver Aufruf, mit inkrementiertem Index
                        {
                            if(typeof callback == 'function'){callback();}
                        });
                    });
                }
                else // Referenz wird noch benötigt, also nächste Referenz überprüfen
                {
                    callbackLoop_GarbageCollector($i + 1, function() // rekursiver Aufruf, mit inkrementiertem Index
                    {
                        if(typeof callback == 'function'){callback();}
                    });
                }
            }
            else // letzte Referenz wurde erreicht, also Ende der "Schleife"
            {
                if(typeof callback == 'function'){callback();}
            }
        }


        debug('runtime', 3, 'info', 'Start of reference garbage collector');
        debug_append_data('runtime', $alive, 'IDs of used references');

        callbackLoop_GarbageCollector(0, function() // rekursive Schleife aufrufen um nicht benötigte Referenzen zu entfernen
        {
            prepare_reference_container(function() // Anzeigebereich des Clients entsprechend vorbereiten
            {
                var callbackLoop_ReferenceShow = function($i, callback)
                {
                    if($i < $references.length) // sofern nicht der letzte Index erreicht wurde
                    {
                        debug('runtime', 4, 'info', 'Reference with id #' + $references[$i].get_id() + ', at index #' + $i);

                        $references[$i].show(function() // aktuelle Referenz einblenden
                        {
                            debug_append('runtime', ' => done');

                            callbackLoop_ReferenceShow($i + 1, function() // rekursiver Aufruf mit inkrementiertem Index
                            {
                                if(typeof callback == 'function'){callback(true);}
                            });
                        });
                    }
                    else // der letzte Index wurde erreicht
                    {
                        if($i == 0) // falls der letzte Index null ist
                        {
                            if(typeof callback == 'function'){callback(false);} // False == keine Referenzen wurden eingeblendet
                        }
                        else
                        {
                            if(typeof callback == 'function'){callback(true);} // True == mindestens eine Referenz wurde eingeblendet
                        }
                    }
                }


                debug('runtime', 3, 'info', 'Fading in references, if any exist');

                callbackLoop_ReferenceShow(0, function($return) // vorhandene Referenzen einblenden (callbackLoop)
                {
                    if($return == true) // wenn Referenzen eingeblendet wurden
                    {
                        debug('runtime', 3, 'info', 'Playing notification sound');
                        var $played = 1; // zähler für die Anzahl der Wiedergaben

                        //debug('runtime', $debug_last_tab['runtime'] + 1, 'info', 'Playing notification sound');
                        play_notification_sound(0, function() // Benachrichtigungston abspielen
                        {
                            var $this = this; // Bezug auf das aktuelle Objekt speichern (die Callback-Funktion)

                            if($played < 3) // bis der Zähler das Maximum erreicht
                            {
                                $played++; // wird bei jedem Abspielen des Tons inkrementiert

                                setTimeout(function() // kurze Pause zwischen den Tönen
                                {
                                    play_notification_sound(0, $this); // rekursiver Aufruf um den Ton erneut abzuspielen
                                }, 500);
                            }
                        });
                    }
                    else // keine einzige Referenz wurde eingeblendet
                    {
                        debug('runtime', 4, 'info', 'No references available to fade in');
                    }


                    // hier ist das Ende der Funktion
                    if($special_state == false) // sofern kein Magic-Error gefunden wurde
                    {
                    	if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});} // Success-Daten zurückgeben
                    }
                    else // es wurde ein Magic-Error gefunden
                    {
                    	// sofern Sepzial-Meldungen gefunden werden, werden die Metadaten der Meldung zurückgegeben, die später ggf.
                    	// zum Setzen eines Timeouts verwendet werden
                    	if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': $state_meta});} // Success-Daten zurückgeben
                    }
                });
            }, $special_state, $state_meta);
        });
    }





   /* Wird automatisch beim Erzeugen eines Referenz-Objektes aufgerufen oder
    * zum Ende von handle_price_data(). Diese Funktion blendet die "Preise angeglichen"
    * Meldung ein, sofern keine Referenzen mehr im DOM vorhanden sind.
    *
    * Version:  1.1.1
    * Stand:    25. August 2015
    *
    * Input:
    *   callback    	: function
    *   $special_state  : mixed		= {"closed", "rise"} = gibt an, ob ein bestimmter Zustand angezeigt werden soll
    *   $state_meta		: mixed		= Metadaten für spezielle Zustände (z.B. für den Countdown)
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function prepare_reference_container(callback, $special_state, $state_meta)
    {
        var $return = null;
        debug('runtime', $debug_last_tab['runtime'] - 1, 'info', 'Preparing reference container');

        if($('.references').length == 0 || typeof $special_state == 'string') // nur wenn keine Referenz-Objekte mehr im DOM vorhanden sind
        {
            debug('runtime', $debug_last_tab['runtime'] + 1, 'info', 'Removing current placeholder message');

            $.when($('#references #placeholder').fadeOut(1000)).then(function() // Placeholder-Meldung ausblenden
            {
                debug_append('runtime', ' => done');
                $('#references #placeholder').remove(); // bisherigen Placeholder löschen
                $('#reference-bottom').fadeOut(500); // Bottom-Meldung ausblenden

                debug('runtime', $debug_last_tab['runtime'], 'info', 'Displaying new placeholder message');

                if($special_state == 'closed') // Station ist als geschlossen markiert
                {
                    debug_append('runtime', ' => station closed');

                    // Placeholder-Meldung in DOM einfügen
                    $('#references').html(
                        '<div id="placeholder" class="placeholder-closed" style="display: none;">' +
                            '<img src="./images/closed.png" />' +
                            '<div id="caption">' +
                                '<h3>Preisabfragen pausiert</h3>' +
                                '<p>Zurzeit werden keine Preisabfragen durchgef&uuml;hrt, da Ihre Station noch geschlossen hat oder sich kurz vor Ladenschluss befindet.</p>' +
                            '</div>' +
                        '</div>'
                    );

                    setScreenBrightness(1);

                    $('body').on('click.station_closed_screenbrightness', function()
                    {
                        setScreenBrightness(255);
                        debug('properties', 0, 'info', 'click');
                    });
                }
                else
                {
                    setScreenBrightness(255);
                    $('body').off('click.station_closed_screenbrightness');

                    if($special_state == 'rise') // es ist ein Rise-Timeout aktiv (Preiserhöhung)
                    {
                        debug_append('runtime', ' => rise');

                        // Placeholder-Meldung in DOM einfügen
                        $('#references').html(
                            '<div id="placeholder" class="placeholder-equalized" style="display: none;">' +
                                '<img src="./images/cloud_rise.png" />' +
                                '<div id="caption">' +
                                    '<h3>Preiserh&ouml;hung festgestellt!</h3>' +
                                    '<p>Zurzeit werden keine Preisabfragen durchgef&uuml;hrt, da Ihre Station eine Preiserh&ouml;hung durchgef&uuml;hrt hat. Die Preise der Wettbewerbspartner werden in etwa <span id="countdown-rise">##:##</span> Minuten wieder auf &Auml;nderungen &uuml;berpr&uuml;ft.</p>' +
                                '</div>' +
                            '</div>'
                        );

                        //*** Berechnung des Timeouts ***//
                        // Sollte der Client während eines Timeout neu geladen werden, muss der Countdown so gesetzt werden,
                        // dass er mit den RuntimeTimeouts Übereinstimmt und der Timeout abgelaufen ist, sobald der Countdown abgelaufen ist
                        //
                        // RestTimeout / ExecTimeout 	=[z.B]=> 	1288 / 300 = 4,29
                        // Ergebnis aufgerundet			=[z.B]=> 	5
                        // Ergebnis * ExecTimeout 		=[z.B]=>	1500

                        countdown(Math.ceil($state_meta / ($RUNTIME_EXEC_TIME / 1000)) * $RUNTIME_EXEC_TIME / 1000 , "countdown-rise", "m");
                    }
                    else if($special_state == 'loading') // wenn die Runtime das erste Mal ausgeführt wird (direkt nach Login)
                    {
                        debug_append('runtime', ' => loading');

                        $('#references').html(
                            '<div id="placeholder" class="placeholder-loading" style="display: none;">' +
                                '<img src="./images/cloud_load.png" />' +
                                '<div id="caption">' +
                                    '<h3>Daten werden geladen...</h3>' +
                                    '<p>Bitte haben Sie einen Moment Geduld, bis Ihre Stationsdaten geladen wurden und der erste Abgleich mit Ihren Wettbewerbspartnern durchgef&uuml;hrt wurde.</p>' +
                                '</div>' +
                            '</div>'
                        );
                    }
                    else if($special_state == 'malfunction') // wird während der Ausführung der Runtime ein Fehler festgestellt
                    {
                        debug_append('runtime', ' => malfunction');

                        $('#references').html(
                            '<div id="placeholder" class="placeholder-malfunction" style="display: none;">' +
                                '<img src="./images/cloud_error.png" />' +
                                '<div id="caption">' +
                                    '<h3>Etwas ist schief gelaufen</h3>' +
                                    '<p>Bei der Preisabfrage sind offenbar unvorhergesehene Probleme aufgetreten.<br>Die Abfrage wird jedoch in etwa <span id="countdown-malfunction">##:##</span> Minuten erneut durchgef&uuml;hrt.</p>' +
                                '</div>' +
                            '</div>'
                        );

                        countdown($RUNTIME_EXEC_TIME / 1000, "countdown-malfunction", "m"); // Countdown in der Bottom-Meldung setzen (bis zur nächsten Preisabfrage)
                    }
                    else if($special_state == 'offline')
                    {
                        debug_append('runtime', ' => offline');

                        $('#references').html(
                            '<div id="placeholder" class="placeholder-malfunction" style="display: none;">' +
                                '<img src="./images/offline.png" />' +
                                '<div id="caption">' +
                                    '<h3>Offline</h3>' +
                                    '<p>Es besteht offenbar keine Verbindung zum Internet!<br>In etwa <span id="countdown-offline">##:##</span> Minuten wird erneut versucht die Abfrage durchzuf&uuml;hren.</p>' +
                                '</div>' +
                            '</div>'
                        );

                        countdown($RUNTIME_EXEC_TIME / 1000, "countdown-offline", "m"); // Countdown in der Bottom-Meldung setzen (bis zur nächsten Preisabfrage)
                        $return = true;
                    }
                    else // wenn die Station noch geöffnet ist
                    {
                        debug_append('runtime', ' => equalized');

                        // Placeholder-Meldung in DOM einfügen
                        $('#references').html(
                            '<div id="placeholder" class="placeholder-equalized" style="display: none;">' +
                                '<img src="./images/cloud_okey.png" />' +
                                '<div id="caption">' +
                                    '<h3>Alle Preise sind angeglichen!</h3>' +
                                    '<p>Die Preise der Wettbewerbspartner werden in etwa <span id="countdown-equalized">##:##</span> Minuten erneut auf &Auml;nderungen &uuml;berpr&uuml;ft.</p>' +
                                '</div>' +
                            '</div>'
                        );

                        countdown($RUNTIME_EXEC_TIME / 1000, "countdown-equalized", "m"); // Countdown in der Bottom-Meldung setzen (bis zur nächsten Preisabfrage)
                    }
                }

                $.when($('#references #placeholder').fadeIn(1000)).then(function() // wenn die Placeholder-Meldung eingeblendet wurde
                {
                    if(typeof callback == 'function'){callback($return);}
                });
            });
        }
        else // es sind Referenzen im DOM vorhanden
        {
            setScreenBrightness(255);
        	$('body').off('click.station_closed_screenbrightness');

            debug('runtime', $debug_last_tab['runtime'] + 1, 'info', 'Removing placehoder message, if displayed');

            overlay_hide(); // Overlay-Content ausblenden, falls sichtbar
            $.when($('#references #placeholder').fadeOut(1000)).then(function() // Placeholder-Meldung ausblenden
            {
                debug_append('runtime', ' => done');

                $('#references #placeholder').remove(); // löscht die Placeholder-Meldung aus dem DOM
                $('#reference-bottom').fadeIn(500); // Bottom-Meldung ausblenden
                countdown($RUNTIME_TIMEOUT, 'countdown-references', 'm'); // Countdown in der Bottom-Meldung setzen (bis zur nächsten Preisabfrage)

                if(typeof callback == 'function'){callback($return);}
            });
        }
    }





   /* Über diese Funktion werden neue Referenz-Objekte erzeugt, alte gelöscht
    * oder falls nötig aktualisiert. Hierzu wird an die Funktion das Data-Array
    * der update_price_data()-Funktion übergeben.
    *
    * Version:  1.0.0
    * Stand:    17. März 2015
    *
    * Input:
    *   callback    : function
    *
    * Success:
    *   call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null / array
    *       {
    *           [: string]  => : string
    *           ...
    *       }
    *       ['data']    => array
    *       {
    *           [: integer] => array
    *           {
    *               ['id']      => : integer
    *               ['image']   => : string
    *               ['prices']  => : array
    *               {
    *                   [: integer] => : integer
    *                   ...
    *               }
    *           }
    *           ...
    *       }
    *   }
    *
    * Failure:
    *   call->callback(array
    *   {
    *       ['success'] => false    : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  => : string
    *           ...
    *       }
    *       ['data']    => null     : null
    *   }
    *
    * Errors:
    *   passed  ->  ajax -> price_validation()
    *   2e001   =   "Die Preise konnten nicht Überprüft werden, da der Server eine ungültige Antwort geliefert hat!"
    *   2e002   =   "Auf Grund eines Verbindungsproblems konnten die Preise nicht Überprüft werden!"
    */
    function update_price_data(callback)
    {
        $.ajax({
            url: './javascript/ajax/prices.php',
            type: 'post',
            success: function($ajaxReturn)
            {
                try
                {
                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
                }
                catch(err) // falls die Umwandlung fehlschlägt
                {
                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'2e001': 'Die Preise konnten nicht &uuml;berpr&uuml;ft werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});}
                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
                }

                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
                {
                    if(typeof callback == 'function'){callback({'success': true, 'errors': $data['errors'], 'data': $data['data']});} // Success-Daten zurückgeben
                }
                else
                {
                    if(typeof callback == 'function'){callback({'success': false, 'errors': $data['errors'], 'data': null});} // Failure-Daten zurückgeben
                }
            },
            error: function() // es sind Verbindungsprobleme aufgetreten
            {
                if(typeof callback == 'function'){callback({'success': false, 'errors': {'2e002': 'Auf Grund eines Verbindungsproblems konnten die Preise nicht &uuml;berpr&uuml;ft werden!'}, 'data': null});} // Failure-Daten zurückgeben
            }
        });
    }





   /* ***** KLASSE *****
    * Jedes Referenz-Objekt steht für eine Referenz im DOM und liefert
    * die zugehörigen Funktionen um das Objekt ein- bzw. auszublenden.
    * Hinzu kommt die Möglichkeit die Referenz zu aktualisieren usw.!
    *
    * Version:  1.0.2
    * Stand:    05. Juni 2016
    *
    * Input:
    *   $id         : integer   = die ID der Referenz
    *   $image      : string    = das Logo der Referenz-Marke
    *   $identifier : integer 	= ein nummerisches Erkennungsmerkmal zur Identifizierung an der Kasse
    *   $prices     : array     = der Anzeige-Typ der Meldung
    *   {
    *       [: integer] => array
    *       {
    *           ['value']   => : integer
    *           ['name']    => : string
    *       }
    *   }
    *
    * Function:
    *   void    : show(callback : function)
    *   void    : hide(callback : function)
    *   void    : remove(callback : function)
    *   void    : update($image : string, $type : string, $content : string)
    *   integer : get_id()
    */
    function reference($id, $image, $identifier, $prices)
    {
        if(typeof $prices == 'object')
        {
            var $price_html = '';

            for(var $i in $prices)
            {
                $price_html += '<li><p>' + $prices[$i]['name'] + '</p><span>' + (($prices[$i]['value']) / 100).toFixed(2) + '<sup>9</sup></span></li>';
            }


            if($identifier == undefined || $identifier == null)
            {
            	$identifier = '';
            }
            else
            {
            	$identifier = '#' + $identifier;
            }



            var $object = $(
            '<div class="references" style="display: none">' +
            	'<div class="brand">' +
                	'<img src="./images/brands/' + $image + '" />' +
                	'<p>' + $identifier + '</p>' +
            	'</div>' +
                '<div class="prices">' +
                    '<ul>' + $price_html + '</ul>' +
                '</div>' +
            '</div>');

            $('#content #references').append($object); // Objekt im DOM einfügen



           /* Blendet das Broadcast-Objekt ein und spielt einen Benachrichtigungston ab.
            * Durch eine Art rekursive Funktion wird der Ton mehrmals abgespielt.
            *
            * Version:  1.0.0
            * Stand:    17. März 2015
            *
            * Input:
            *   callback    : function  = wird zum Ende der Funktion aufgerufen
            *
            * Success:
            *   call -> callback();
            *
            * Failure:
            *   ~Success
            *
            * Errors:
            *   none
            */
            this.show = function(callback)
            {
                $.when($object.fadeIn(1000)).then(function() // wenn die Referenz eingeblendet wurde
                {
                    if(typeof callback == 'function'){callback();}
                });
            }



           /* Blendet das Broadcast-Objekt wieder aus.
            *
            * Version:  1.0.0
            * Stand:    16. März 2015
            *
            * Input:
            *   callback    : function  = wird zum Ende der Funktion aufgerufen
            *
            * Success:
            *   call -> callback();
            *
            * Failure:
            *   ~Success
            *
            * Errors:
            *   none
            */
            this.hide = function(callback)
            {
                $.when($object.fadeOut(1000)).then(function() // wenn die Referenz ausgeblendet wurde
                {
                    if(typeof callback == 'function'){callback();}
                });
            }



           /* Entfernt das Broadcast-Objekt aus dem DOM und leert alle Variabeln.
            * Damit ist dieses Objekt "gelöscht" und jede Referenz zu ihm sollte
            * aufgehoben werden.
            *
            * Version:  1.0.0
            * Stand:    16. März 2015
            *
            * Input:
            *   callback    : function  = wird zum Ende der Funktion aufgerufen
            *
            * Success:
            *   call -> callback();
            *
            * Failure:
            *   ~Success
            *
            * Errors:
            *   none
            */
            this.remove = function(callback)
            {
                this.hide(function() // wenn die Meldung ausgeblendet wurde
                {
                    $object.remove(); // Objekt aus dem DOM entfernen
                    $id = $image = $prices = $object = undefined; // Variabeln löschen

                    if(typeof callback == 'function'){callback();}
                });
            }



           /* Aktualisiert, falls nötig, die wichtigsten Elemente derBroadcast-Meldung.
            * Sofern der Titel, Type oder Content verändert wurde, wird das Objekt
            * ausgeblendet, aktualisiert und wieder eingeblendet.
            *
            * Version:  1.0.0
            * Stand:    05. Juni 2016
            *
            * Input:
            *   callback    : function  = wird zum Ende der Funktion aufgerufen
            *
            * Success:
            *   call -> callback();
            *
            * Failure:
            *   ~Success
            *
            * Errors:
            *   none
            */
            this.update = function($newImage, $newIdentifier, $newPrices, callback)
            {
                if($image != $newImage || $prices != $newPrices || $identifier != $newIdentifier) // falls sich ein Wert verändert hat
                {
                    var $this = this; // Referenz auf dieses Objekt speichern

                    this.hide(function() // wenn die Referenz ausgeblendet wurde
                    {
                        var $newPriceHTML = '';

                        for(var $i in $newPrices)
                        {
                            $newPriceHTML += '<li><p>' + $newPrices[$i]['name'] + '</p><span>' + (($newPrices[$i]['value']) / 100).toFixed(2) + '<sup>9</sup></span></li>';
                        }


                        if($newIdentifier == undefined || $newIdentifier == null)
			            {
			            	$newIdentifier = '';
			            }
			            else
			            {
			            	$newIdentifier = '#' + $newIdentifier;
			            }



                        $object.find('.prices').children('ul').html($newPriceHTML); // Content erneuern
                        $object.find('.brand img').attr('src', './images/brands/' + $newImage); // Image der Meldung erneuern
                        $object.find('.brand p').html($newIdentifier);

                        // Variabeln neu befüllen
                        $image = $newImage;
                        $identifier = $newIdentifier;
                        $prices = $newPrices;

                        setTimeout(function() // kurze kosmetische Pause
                        {
                            $this.show(function() // wenn die Meldung eingeblendet wurde
                            {
                                if(typeof callback == 'function'){callback();}
                            });
                        }, 1500);
                    });
                }
            }



            // ***** ***** Getter Funktionen ***** *****

            this.get_id = function() // ID der Referenz zurückgeben
            {
                return $id;
            }
        }
        else // es wurde kein Preis-Array übergeben
        {
            //**TODO** Fehlerbehandlung einfügen
        }
    }
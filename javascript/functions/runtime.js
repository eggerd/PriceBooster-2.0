
    var $price_update_timeout = 0; // neue Updates sind erst nach überschreiten dieses Timestamps möglich (gespeichert in "Sekunden")



   /* Startet das Runtime Enviroment, welches für Preisabfragen usw.
    * zuständig ist. Ruft alle nötigen Unterfunktionen auf und startet
    * die benötigten Routinen.
    *
    * Version:  1.2.3
    * Stand:    11. Dezember 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function runtime()
    {
    	dump_debug_log(); // Log-Einträge des letzten Durchlaufs an den Server senden

        debug('runtime', 0, 'info', 'Runtime execution');
        debug('runtime', 1, 'info', 'Hiding notifications of area 4 (Properties), if existing');

        remove_notifications('area', 5, function() // Fehlermeldungen der Datenbank entfernen
        {
            remove_notifications('area', 4, function() // Fehlermeldungen der Properties entfernen
            {
                debug_append('runtime', ' => done');
                debug('runtime', 1, 'info', 'Checking for new client version');

                validate_version(function($return) // Versionen vergleichen
                {
                    debug_append_data('runtime', $return);

                    if($return['success'] == true) // wenn die Abfrage ohne Fehler verlief
                    {
                        if($return['data'] == false) // wenn die Versionen nicht mehr übereinstimmen
                        {
                            debug('runtime', 2, 'info', 'New client version available');
                            debug('runtime', 2, 'info', 'Reloading client to update to a newer version');
                            location.reload(); // Seite neu laden um auf neue Version zu aktualisieren
                        }
                        else
                        {
                            debug_append('runtime', ' => client is up to date');

                            // +++++ +++++ +++++ +++++ +++++
                            // Prices Runtime
                            // +++++ +++++ +++++ +++++ +++++

                            debug('runtime', 1, 'info', 'Start of price validation');
                            debug('runtime', 2, 'info', 'Validation of client authentication');

                            authenticated(function($return) // Gültigkeit des Logins überprüfen
                            {
                                debug_append_data('runtime', $return);

                                if($return['success'] == true && $return['data'] == true) // nur wenn keine Fehler aufgetreten sind und der Login gültig ist
                                {
                                    debug_append('runtime', ' => valid');

                                    if($price_update_timeout <= Math.floor(Date.now() / 1000)) // Update nur durchführen wenn der Timeout abgelaufen ist
                                    {
                                        debug('runtime', 2, 'info', 'Hiding notifications of area 2 (Runtime), if existing');

                                        remove_notifications('area', 2, function() // Fehlermeldungen der Runtime entfernen
                                        {
                                            debug_append('runtime', ' => done');
                                            debug('runtime', 2, 'info', 'Fetching price data from server');

                                            update_price_data(function($return) // Preis-Daten vom Server laden
                                            {
                                                debug_append_data('runtime', $return);

                                                if($return['success'] == true) // wenn die Daten erfolgreich geladen wurden
                                                {
                                                    handle_price_data($return, function($handler_data) // zurückgegebene Preis-Daten auswerten (z.B. Referenz-Objekte erzeugen, -Container vorbereiten usw.)
                                                    {
                                                        if($handler_data['data'] == true) // sofern die Preis-Daten OK sind
                                                        {
	                                                        if($return['data'] != null) // sofern vom Server auch Preis-Daten zurückgegeben wurden
	                                                        {
	                                                            $price_update_timeout = (Math.floor((Date.now() / 1000) / 60) * 60) + $RUNTIME_TIMEOUT; // neuen Timeout setzen (in Sekunden - aber auf Minuten gerundet)
	                                                            debug('runtime', 3, 'info', 'New timeout set - end at ' + date($price_update_timeout * 1000));
	                                                            debug_append_data('runtime', $price_update_timeout, 'Timestamp');
	                                                        }
	                                                        else // die Abfrage war erfolgreich, aber keine neuen Preise wurden übermittelt
	                                                        {
	                                                            $price_update_timeout = 0; // Timeout für die Preisabfragen zurücksetzen
	                                                            debug('runtime', 3, 'info', 'Timeout reset');
	                                                        }
	                                                    }
	                                                    else // die Preis-Daten wurden abgelehnt, vermutlich da Magic-Error gefunden wurden
	                                                    {
	                                                    	if($handler_data['data'] != null && !isNaN($handler_data['data'])) // ist in den Daten eine Nummer, so wird diese für den Timeout verwendet
	                                                    	{
	                                                    		$price_update_timeout = (Math.floor((Date.now() / 1000) / 60) * 60) + $handler_data['data']; // neuen Timeout setzen (in Sekunden - aber auf Minuten gerundet)
	                                                            debug('runtime', 3, 'info', 'New timeout set - end at ' + date($price_update_timeout * 1000));
	                                                            debug_append_data('runtime', $price_update_timeout, 'Timestamp');
	                                                    	}
	                                                    	else // die Abfrage war erfolgreich, aber keine neuen Preise wurden übermittelt
	                                                        {
	                                                            $price_update_timeout = 0; // Timeout für die Preisabfragen zurücksetzen
	                                                            debug('runtime', 3, 'info', 'Timeout reset');
	                                                        }
	                                                    }
                                                    });
                                                }
                                                else // Preis-Daten konnten nicht vom Server geladen werden
                                                {
                                                    for(var $i in $return['errors']) // für jeden übermittelten Error
                                                    {
                                                        debug('runtime', 3, 'error', $i + ' | ' + $return['errors'][$i]);
                                                        //debug_append_data('runtime', $return['data']);

                                                        create_notification($i, $return['errors'][$i]); // Fehlermeldung einblenden
                                                    }

                                                    prepare_reference_container(null, 'malfunction'); // Malfunction-Placeholder anzeigen
                                                }
                                            });
                                        });
                                    }
                                    else // Timeout für die Preisabfragen ist noch nicht abgelaufen
                                    {
                                        debug('runtime', 2, 'info', 'Active timeout until ' + date($price_update_timeout * 1000));
                                        debug('runtime', 2, 'info', 'End of price validation');
                                    }
                                }
                                else if($return['errors'] != null) // Login ist entweder False oder konnte nicht überprüft werden
                                {
                                    debug('runtime', 3, 'warning', 'Client side logout due to a error during the authentication validation');
                                    logout(); // Logout auch von JS-Seite aus triggern (nur zur Sicherheit)
                                    finish_logout(); // Loginformular wieder einblenden usw.

                                    remove_notifications('area', 1, function() // Fehlermeldungen des Logins entfernen
                                    {
                                        for(var $i in $return['errors']) // für jede vorhandene Fehlermeldung
                                        {
                                            debug('runtime', 4, 'error', $i + ' | ' + $return['errors'][$i]);
                                            create_notification($i, $return['errors'][$i]); // Fehlermeldung einblenden
                                        }
                                    });
                                }
                                else // wenn der Client einfach nicht angemeldet ist
                                {
                                    debug('runtime', 3, 'info', 'Client is not authenticated - end of price validation');
                                    logout();
                                }
                            });



                            // +++++ +++++ +++++ +++++ +++++
                            // Broadcast Runtime
                            // +++++ +++++ +++++ +++++ +++++

                            remove_notifications('area', 3, function() // Fehlermeldungen der Broadcasts entfernen
                            {
                                fetch_broadcasts(function($return) // lädt alle vorhandenen Broadcasts (global und für die Station)
                                {
                                    if($return['success'] == true) // wenn das Laden erfolgreich war
                                    {
                                        handle_broadcasts($return['data']); // geladene Broadcast-Daten verarbeiten
                                    }
                                    else
                                    {
                                        for (var $code in $return['errors']) // für alle übermittelten Fehlermeldungen
                                        {
                                            create_notification($code, $return['errors'][$code]); // Fehlermeldung einblenden
                                        }
                                    }
                                });
                            });
                        }
                    }
                    else // die Version konnte nicht überprüft werden
                    {
                    	if($return['data'] != 'update') // nur wenn es sich nicht um ein Update handelt
                    	{
		                    for(var $i in $return['errors']) // für jede vorhandene Fehlermeldung
		                    {
		                        debug('runtime', 2, 'error', $i + ' | ' + $return['errors'][$i]);
		                        create_notification($i, $return['errors'][$i]); // Fehlermeldung einblenden
		                    }

		                    prepare_reference_container(null, 'malfunction'); // Malfunction-Placeholder anzeigen
		                }
                    }
                });
            });
        });



        // +++++ +++++ +++++ +++++ +++++
        // recursive Runtime Execution
        // +++++ +++++ +++++ +++++ +++++

        $runtime_timeout = setTimeout(function() // rekursiver Aufruf nach Timeout
        {
            runtime(); // Runtime rekursiv aufrufen
        }, $RUNTIME_EXEC_TIME);
    }

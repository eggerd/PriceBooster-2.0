
	var $UPDATING = false; // gibt an ob gerade eine Softwareaktualisierung durchgeführt wird



   /* Liest die Value des angegebenen Key-Paars für die aktuelle Station aus der
    * Datenbank aus, wodurch Einstellunden und sonstige Daten spezifisch für jede
    * Station geladen werden können.
    *
    * Version:  1.0.1
    * Stand:    13. September 2015
    *
    * Input:
    *     $key1     : string    = der benötigte Haupt-Key
    *     $key2     : string    = der optionale zweite Key
    *     callback  : function  = wird zum Ende der Funktion aufgerufen
    *
    * Success:
    *   call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => passed -> ajax -> fetch_station_properties()
    *   }
    *
    * Failure:
    *   call->callback(array
    *   {
    *       ['success'] => false : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => null
    *   }
    *
    * Errors:
    *   passed  -> ajax -> fetch_station_properties()
    *   passed  -> ajax
    *
    *   4e001   => "Einstellungen konnte nicht geladen werden, da der Server eine ungültige Antwort geliefert hat!"
    *   4e002   => "Auf Grund eines Verbindungsproblems konnten benötigte Einstellungen nicht geladen werden!"
    */
    function fetch_station_properties($key1, $key2, callback)
    {
        if(typeof $key2 == 'undefined') // falls kein zweiter Kex benötigt wird
        {
            $key2 = 'null'; // Null als zweiten Key eintragen (wie in der Datenbank)
        }

        $.ajax({
            url: './javascript/ajax/properties.php',
            data: {type: 'properties', key1: JSON.stringify($key1), key2: JSON.stringify($key2)}, // fetch_station_properties() aufrufen
            type: 'post',
            success: function($ajaxReturn)
            {
                try
                {
                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
                }
                catch(err) // falls die Umwandlung fehlschlägt
                {
                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'4e001': 'Einstellungen konnte nicht geladen werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});}
                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
                }

                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
                {
                    if(typeof callback == 'function'){callback({'success': true, 'errors': $data['errors'], 'data': $data['data']});} // Success-Daten zurückgeben
                }
                else
                {
                    if(typeof callback == 'function'){callback({'success': false, 'errors': $data['errors'], 'data': null});} // Failure-Daten zurückgeben
                }
            },
            error: function() // falls Verbindungsprobleme aufgetreten sind
            {
                if(typeof callback == 'function'){callback({'success': false, 'errors': {'4e002': 'Auf Grund eines Verbindungsproblems konnten ben&ouml;tigte Einstellungen nicht geladen werden!'}, 'data': null});} // Failure-Data zurückgeben
            }
        });
    }





   /* Über diese Funktion werden grundlegende Daten der Station aus der Datenbank geladen.
    * Hierzu gehören z.B. die Adresse oder den Stationsnamen.
    *
    * Version:  1.0.1
    * Stand:    13. September 2015
    *
    * Input:
    *   callback  : function  = wird zum Ende der Funktion aufgerufen
    *
    * Success:
    *   call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => passed -> ajax -> fetch_station_info()
    *   }
    *
    * Failure:
    *   call->callback(array
    *   {
    *       ['success'] => false : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => null
    *   }
    *
    * Errors:
    *   passed  -> ajax -> fetch_station_info()
    *   passed  -> ajax
    *
    *   4e007   => "Informationen konnte nicht geladen werden, da der Server eine ungültige Antwort geliefert hat!"
    *   4e008   => "Auf Grund eines Verbindungsproblems konnten benötigte Informationen nicht geladen werden!"
    */
    function fetch_station_info(callback)
    {
        $.ajax({
            url: './javascript/ajax/properties.php',
            data: {type: 'info'}, // fetch_station_info() aufrufen
            type: 'post',
            success: function($ajaxReturn)
            {
                try
                {
                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
                }
                catch(err) // falls die Umwandlung fehlschlägt
                {
                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'4e007': 'Informationen konnte nicht geladen werden, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});}
                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
                }

                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
                {
                    if(typeof callback == 'function'){callback({'success': true, 'errors': $data['errors'], 'data': $data['data']});} // Success-Datan zurückgeben
                }
                else
                {
                    if(typeof callback == 'function'){callback({'success': false, 'errors': $data['errors'], 'data': null});} // Failure-Daten zurückgeben
                }
            },
            error: function() // falls Verbindungsprobleme aufgetreten sind
            {
                if(typeof callback == 'function'){callback({'success': false, 'errors': {'4e008': 'Auf Grund eines Verbindungsproblems konnten ben&ouml;tigte Informationen nicht geladen werden!'}, 'data': null});} // Failure-Daten zurückgeben
            }
        });
    }





   /* Fragt die aktuelle Version vom Server ab und vergleicht diese mit der
    * der aktuellen Client-Version, und gibt anschließend das Ergebnis zurück.
    *
    * Version:  1.1.1
    * Stand:    26. Juli 2016
    *
    * Input:
    *   callback    : function  = wird zum Ende der Funktion aufgerufen
    *
    * Success:
    *   call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null
    *       ['data']    => 		: boolean
    *   }
    *
    * Failure:
    *   call->callback(array
    *   {
    *       ['success'] => false : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => null
    *   }
    *
    * Errors:
    *   4e009   => "Die Service-Version konnte auf Grund von Verbindungsproblemen nicht überprüft werden!"
    *   4e010   => "ungültige Antwort des Servers beim Ermitteln der Service-Version!"
    */
    function validate_version(callback)
    {
    	$.ajax({
            url: './version.php',
            type: 'post',
            success: function($ajaxReturn)
            {
                try
                {
                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
                }
                catch(err) // falls die Umwandlung fehlschlägt
                {
                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'4e010': 'Ung&uuml;ltige Antwort des Servers beim Ermitteln der Service-Version!'}, 'data': $ajaxReturn});} // Failure-Daten zurückgeben
                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
                }

                if($data['data']['version'] != undefined) // sofern eine Versionsnummer zurückgegeben wurde
                {
	                if($data['data']['version'] == $SERVICE_VERSION) // zurückgegebene Version mit der Client Version vergleichen
	                {
	                    visualise_update(false, null, function()
						{
							if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});} // Success-Daten zurückgeben (Client ist aktuell)
						});
	                }
	                else
	                {
	                    if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': false});} // Success-Daten zurückgeben (Client ist outdated)
	                }
                }
                else // sofern eine Update-Versionsnummer zurückgegeben wurde
                {
                	visualise_update(true, $data['data']['update'], function() // Hinweis einblenden
					{
						if(typeof callback == 'function'){callback({'success': false, 'errors': null, 'data': 'update'});} // Failure-Daten, allerdings ohne Error, zurückgeben - verhinder Ausführung anderer Funktionen
					});
                }
            },
            error: function(xhr, status, text) // auch die Update-Info ist nicht erreichbar - Verbindungsproblem oder Server vollständig unerreichbar
            {
            	$debug_last_tab['runtime'] = 2;
            	Offline.check(); // prüfen ob eine Internetverbindung besteht; triggert Fehlermeldung in /javascript/functions/connection.js
            }
    	});
    }





   /* Wird normal von validate_version() aufgerufen, wenn eine Aktualisierung des Servers
   	* erkannt wird. Diese Funktion ist dabei für das ein- und ausblenden der Update
   	* Placeholder-Nachricht zuständig und dem ausblenden nicht benötigter Elemente.
   	*
   	* Version: 	1.0.3
   	* Stand: 	10. Juni 2016
   	*
   	* Input:
   	* 	$display 		: boolean 	= gibt an ob die Meldung ein- (true) oder ausgeblendet (!=true) werden soll
   	* 	$new_version 	: string 	= enthält die neue Versionsnummer, die in der Placeholder-Message erwähnt wird
   	* 	callback 		: function	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
    *   call->callback(array
    *   {
    *       ['success'] => true	: boolean
    *       ['errors']  => null
    *       ['data']    => 		: boolean 	= true: eingeblendet, false: ausgeblendet
    *   }
    *
    * Failure:
    *   ~Success
    *
   	* Errors:
   	* 	none
    */
    function visualise_update($display, $new_version, callback)
    {
    	if($display == undefined || $display == true) // nur wenn die Meldung eindeutig eingeblendet werden soll
    	{
    		$UPDATING = true;

    		$.when($('#navi, #login, #references, #reference-bottom').fadeOut(1000)).then(function() // alle betroffenen Elemente ausblenden
			{
				$references = new Array(); // Referenz-Array resetten, da alle Referenzen gleich durch die neue Update-Meldung Überschrieben werden
            	$price_update_timeout = 0; //Runtime-Timeout für Preisabfragen zurücksetzen

				overlay_hide(function() // Overlay minimieren, falls angezeigt
				{
					// Meldung in DOM einfügen
					$('#references').html(
	                    '<div id="placeholder" class="placeholder-update">' +
	                        '<img src="./images/cloud_update.png" />' +
	                        '<div id="caption">' +
	                            '<h3>Softwareaktualisierung</h3>' +
	                            '<p>Der Dienst wird zurzeit auf Version ' + $new_version + ' aktualisiert. In dieser Zeit sind leider keine Preisabfragen m&ouml;glich. Die Aktualisierung sollte jedoch in K&uuml;rze abgeschlossen sein und der normale Betrieb wieder aufgenommen werden!</p>' +
	                        '</div>' +
	                    '</div>'
	                );

	                $.when($('#references').fadeIn(1000)).then(function() // Meldung einblenden
					{
						if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
					});
				});
			});
		}
		else // Meldung soll wieder ausgeblendet werden
		{
			$UPDATING = false;

			if($('#references .placeholder-update').length > 0) // nur wenn die Meldung Überhaupt vorhanden ist
			{
				$.when($('#references').fadeOut(1000)).then(function() // Meldung ausblenden
				{
					$('#references').html(null); // Meldung aus DOM entfernen

					potentially_authenticated(function($data) // abfragen ob der Client zuvor eingeloggt war - wird nur anhand von Cookies überprüft
					{
						if($data['data'] == true) // Client scheint eingeloggt (kann zurzeit nicht genauer überprüft werden...)
						{
							$.when($('#navi, #references').fadeIn(1000)).then(function() // benötigte Elemente wieder einblenden
							{
								if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': false});}
							});
						}
						else // Client ist offenbar nicht eingeloggt
						{
							$.when($('#login, #references').fadeIn(1000)).then(function() // benötigte Elemente (Login) wieder einblenden
							{
								if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': false});}
							});
						}
					});
				});
			}
			else
			{
				if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': false});}
			}
		}
    }





   /* Ermittelt den Wert eines URL-Parameters und gibt diesen zurück. Sollte der Parameter
   	* nicht vorhanden sein, wird "null" zurückgegeben.
   	*
   	* Version: 	1.0.0
   	* Stand: 	31. Dezember 2015
   	*
   	* Input:
   	* 	$parameter 		: string 	= der Name des gesuchten Parameters
   	*
   	* Success:
    *   string
    *   null
    *
    * Failure:
    *   none
    *
   	* Errors:
   	* 	none
    */
    function request($parameter)
    {
		var $results = new RegExp('[\?&]' + $parameter + '=([^&#]*)').exec(window.location.href);

		if($results == null)
		{
			return null;
		}
		else
		{
			return decodeURI($results[1]) || 0;
		}
	}





   /* überprüft mit Hilfe der Kiosk-Librarie, ob ein Client beim Remote Management registriert
    * ist. Falls nicht, wird die Option angeboten die Registration nun durchzuführen. Hierzu
    * wird der Registrations-Key verwendet, der in der Config definiert wurde.
    *
    * Version: 	1.0.0
    * Stand: 	10. August 2016
    *
    * Input:
    * 	none
    *
    * Success:
    * 	none
    *
    * Failure:
    * 	none
    *
    * Errors:
    * 	none
    */
	function check_kiosk_registration()
	{
		var $status = isRemotelyRegistered();

		if($status != undefined)
		{
			if($status == true)
			{
				debug('properties', 0, 'info', 'Device is remotely registered');
			}
			else
			{
				debug('properties', 0, 'info', 'Device has not been remotely registered <p class="data" title="key: ' + $KIOSK_REMOTE_REGISTRATION_KEY + '" onclick="registerRemoteKey(\'' + $KIOSK_REMOTE_REGISTRATION_KEY + '\');">[<span>register now</span>]</p>');
			}
		}
		else
		{
			debug('properties', 0, 'info', 'Device can not be remotely registered');
		}

	}
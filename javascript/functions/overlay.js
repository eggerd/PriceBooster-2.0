
	var $overlay_state = 0; // ist null wenn das Overlay geschlossen wurde, oder die Content-Datei falls das Overlay nur minimiert wurde
	var $overlay_cancel = false; // wird von overlay_cancel() gesetzt um ggf. das Laden des Overlays zu unterbrechen

	var $overlay_destructor = undefined; // sofern benötigt wird hier die Destructor Funktion gespeichert
	var $overlay_hidehandler = undefined; // sofern benötigt wird hier die Hide-Handler Funktion gespeichert



   /* Blendet den normalen Anzeigebereich aus und dafür den Overlay-Content
   	* ein. Die Callback-Funktion wird aufgerufen, sobald das Overlay
   	* vollständig eingeblendet wurde.
   	*
   	* Version: 	1.0.0
   	* Stand: 	11. August 2015
   	*
   	* Input:
   	* 	callback 	: function 	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
   	* 	call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => true : boolean	= gibt an, dass das Overlay eingeblendet wurde
    *   }
    *
    * Failure:
    * 	~Success
    *
    * Errors:
    * 	none
    */
	function overlay_show(callback)
	{
		$('#content').fadeOut(500); // Anzeigebereich ausblenden
		$.when($('#overlay').slideDown(1000)).then(function() // Overlay einblenden
		{
			if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
		});
	}





   /* Blendet den normalen Anzeigebereich ein und dafür den Overlay-Content
   	* aus. Die Callback-Funktion wird aufgerufen, sobald das der normale
   	* Anzeigebereich vollständig eingeblendet ist.
   	*
   	* Version: 	1.1.0
   	* Stand: 	13. November 2015
   	*
   	* Input:
   	* 	callback 	: function 	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
   	* 	call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => false : boolean	= gibt an, dass das Overlay ausgeblendet wurde
    *   }
    *
    * Failure:
    * 	~Success
    *
    * Errors:
    * 	none
    */
	function overlay_hide(callback)
	{
		$.when($('#overlay').slideUp(500)).then(function() // Overlay ausblenden
		{
            if(typeof $overlay_hidehandler == 'function') // sofern eine Close-Callback gesetzt wurde
			{
				$overlay_hidehandler();
			}
        });

		$.when($('#content').fadeIn(1000)).then(function() // Anzeigeberiech wieder einblenden
        {
            if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': false});}
        });
	}





   /* Blendet das Overlay ein oder aus, je nach aktuellem Zustand des Overlays.
    *
    * Version: 	1.0.0
    * Stand: 	11. August 2015
    *
    * Input:
    * 	callback 	: function 	= wird zum Ende der Funktion aufgerufen
    *
    * Success:
    * 	passed	-> overlay_show()
    * 	passed	-> overlay_hide()
    *
    * Failure:
    * 	~Success
    *
    * Errors:
    * 	passed	-> overlay_show()
    * 	passed	-> overlay_hide()
    */
	function overlay_toggle(callback)
	{
		if($('#overlay').is(':visible'))
		{
			overlay_hide(function($return)
			{
				if(typeof callback == 'function'){callback($return);}
			});
		}
		else
		{
			overlay_show(function($return)
			{
				if(typeof callback == 'function'){callback($return);}
			});
		}
	}





   /* Lädt den Inhalt, für das angeforderte Overlay, über Ajax und bereitet den
   	* Overlay-Content für die Anzeige vor.
   	*
   	* Version: 	1.0.6
   	* Stand: 	18. Dezember 2015
   	*
   	* Input:
   	* 	$title 		: string	= der Titel für das Overlay
   	* 	$content 	: string	= Name der Source-Datei, in welcher der Inhalt für das Overlay zu finden ist
   	* 	$show 		: boolean 	= falls gesetzt und nicht "true", wird das Overlay nicht automatisch eingeblendet
   	* 	callback 	: function	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
   	* 	call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => false : boolean
    *   }
   	*
   	* Failure:
   	* 	call->callback(array
    *   {
    *       ['success'] => false : boolean
    *       ['errors']  => array
    *       {
    *           [: string]  =>  : string
    *       }
    *       ['data']    => null
    *   }
   	*
   	* Errors:
   	* 	passed 	-> ajax
   	*	6e001 	=> "Auf Grund einer ungültigen Antwort des Servers, konnte der Content nicht geöffnet werden!"
   	*	6e002 	=> "Der Content konnte auf Grund von Verbindungsproblemen nicht geöffnet werden!"
   	*	6e032 	=> "Auf Grund einer ungültigen Antwort des Servers, konnte der Content nicht geöffnet werden!"
    */
	function overlay($title, $content, $show, callback)
	{
		$overlay_cancel = false; // Overlay-Abbruch-Flag resetten
		remove_notifications(undefined, undefined, function()
		{
			if($overlay_state == 0 || $overlay_state != $content) // sofern das Overlay zuvor geschlossen wurde, oder zumindest ein anderer Content angefordert wird
			{
				overlay_close(function() // Overlay ausblenden, falls gerade angezeigt
				{
					$.ajax({
			            url: './javascript/ajax/overlay.php', // PHP Overlay Daten abrufen
			            data: {content: $content},
			            type: 'post',
			            success: function($ajaxReturn)
			            {
			                try
			                {
			                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
			                }
			                catch(err) // falls die Umwandlung fehlschlägt
			                {
			                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'6e001': 'Auf Grund einer ung&uuml;ltigen Antwort des Servers, konnte der Content nicht ge&ouml;ffnet werden!'}, 'data': $ajaxReturn});} // Failure-Daten zurückgeben
			                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
			                }

			                if($data['success'] == true) // wenn der Content erfolgreich geladen werden konnte
			                {
			                	if($data['data'] != null)
			                	{
			                		$overlay_state = $content; // aktuellen Content als geöffnet abspeichern

				                    $('#overlay #headline #title').html($title); // Titel des Overlays setzen
				                    $('#overlay #context').html($data['data']); // Content des Overlays setzen
				                    $('#overlay #context').perfectScrollbar({suppressScrollX: true}); // jQuery Scrollbar einblenden

				                    if($overlay_cancel == false && (typeof $show == 'undefined' || $show == true))
				                    {
					                    overlay_show(function() // Overlay einblenden
					                    {
					            			if(typeof callback == 'function'){callback({'success': true, 'errors': $data['errors'], 'data': true});} // Success-Daten zurückgeben
					                    });
				                    }
				                }
				                else
				                {
				                	if(typeof callback == 'function'){callback({'success': false, 'errors': {'6e032': 'Auf Grund einer ung&uuml;ltigen Antwort des Servers, konnte der Content nicht ge&ouml;ffnet werden!'}, 'data': $ajaxReturn});} // Failure-Daten zurückgeben
				                }
			                }
			                else // der Content konnte nicht geladen werden
			                {
			                    if(typeof callback == 'function'){callback({'success': false, 'errors': $data['errors'], 'data': null});} // Failure-Daten zurückgeben
			                }
			            },
			            error: function() // es sind Verbindungsprobleme aufgetreten
			            {
			                if(typeof callback == 'function'){callback({'success': false, 'errors': {'6e002': 'Der Content konnte auf Grund von Verbindungsproblemen nicht ge&ouml;ffnet werden!'}, 'data': null});} // Failure-Daten zurückgeben
			            }
			        });
				});
			}
			else
			{
				overlay_show();
			}
		});
	}





   /* Schließt das Oberlay wobei das Overlay ausgeblendet wird und der Content wieder
   	* geleert wird, um es in einem neutralen Zustand zu hinterlassen.
   	*
   	* Version: 	2.1.0
   	* Stand: 	26. November 2015
   	*
   	* Input:
   	* 	callback 	: function 	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
   	* 	call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => true : boolean
    *   }
    *
    * Failure:
    * 	~Success
    *
    * Errors:
    * 	none
    */
	function overlay_close(callback)
	{
		var $timeout = undefined; // speichert die Referenz auf den Timeout
		var $timeout_timer = 0; // die Zeit in Millisekunden, die der Timeout andauern soll - standard Null; wird nur gesetzt wenn Close-Callback gesetzt wurde

		overlay_hide(function() // Overlay ausblenden
		{
			$overlay_state = 0; // Overlay als geschlossen markieren

			var $finish = function() // Part der Close-Funktion, der erst nach dem Destructor aufgeführt werden darf
			{
				$timeout_timer = 0; // Timer wieder auf Null setzen (markiert die Ausführung der Funktion)
				$overlay_destructor = undefined; // Close-Callback kann nun gelöscht werden
				$overlay_hidehandler = undefined; // Close-Callback kann nun gelöscht werden

				$('#overlay #context').perfectScrollbar('destroy'); // jQuery Scrollbar entfernen
				$('#overlay #headline #title').html(''); // Titel des Overlays entfernen
				$('#overlay #context').html(''); // Content des Overlays entfernen

				if(typeof callback == 'function'){callback();}
			}


			if(typeof $overlay_destructor == 'function') // sofern ein Destructor gesetzt wurde
			{
				$timeout_timer = 1000; // Timeout für die Ausführung der Run-Funktion setzen - meldet sich der Destructor nicht innerhalb dieser Zeit, wird die Run-Funktion einfach ausgeführt

				$overlay_destructor(function() // Destructor ausführen
				{
					if($timeout_timer > 0) // nur falls die Run-Funktion nicht bereits über den Timeout aufgerufen wurde
					{
						clearTimeout($timeout); // Timeout abbrechen
						$finish(); // Run-Funktion ausführen
					}
				});
			}

			$timeout = setTimeout($finish, $timeout_timer); // führt die Run-Funktion aus, sollte sich der Destructor nicht zurückmelden
		});
	}





   /* Die übergebene Funktion wird in $overlay_destructor gespeichert und beim
   	* Aufruf von overlay_close() ausgeführt. Somit ist es Overlay-Funktionen möglich
   	* auf das Schließen des Overlays zu reagieren, um z.B. noch Sicherungen durch-
   	* zuführen oder derartiges. Es kann immer nur ein Overlay-Destructor gesetzt werden!
   	*
   	* Am Ende der Destructor-Funktion muss die Callback-Funktion (Parameter 1) aufgerufen
   	* werden, um den Destructor zu beenden.
   	*
   	* Version: 	1.0.0
   	* Stand: 	13. November 2015
   	*
   	* Input:
   	* 	$function	: function 	= die Funktion die von overlay_close() ausgeführt werden soll
   	* 	callback 	: function 	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
   	* 	call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => true : boolean
    *   }
    *
    * Failure:
    * 	~Success
    *
    * Errors:
    * 	6e005	=> "Der Overlay-Destructor wurde zurückgewiesen, da dieser bereits definiert wurde!"
    */
	function overlay_destructor($function, callback)
	{
		if(typeof $function == 'function') // nur wenn auch eine Funktion übergeben wurde
		{
			if($overlay_destructor == undefined) // sofern noch keine Overlay-Destructor definiert wurde
			{
				$overlay_destructor = $function;

				if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
			}
			else
			{
				if(typeof callback == 'function'){callback({'success': false, 'errors': {6e005: 'Der Overlay-Destructor wurde zur&uuml;ckgewiesen, da dieser bereits definiert wurde!'}, 'data': false});}
			}
		}
		else
		{
			if(typeof callback == 'function'){callback({'success': false, 'errors': null, 'data': false});}
		}
	}





   /* Die übergebene Funktion wird in $overlay_hidehandler gespeichert und beim
   	* Aufruf von overlay_hide() ausgeführt. Somit ist es Overlay-Funktionen möglich
   	* auf das Ausblenden des Overlays zu reagieren, um z.B. noch Sicherungen durch-
   	* zuführen oder derartiges. Es kann immer nur ein Hidehandler gesetzt werden!
   	* Wird noch vor dem Destructor aufgerufen!
   	*
   	* Version: 	1.0.0
   	* Stand: 	13. November 2015
   	*
   	* Input:
   	* 	$function	: function 	= die Funktion die von overlay_close() ausgeführt werden soll
   	* 	callback 	: function 	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
   	* 	call->callback(array
    *   {
    *       ['success'] => true : boolean
    *       ['errors']  => null : null
    *       ['data']    => true : boolean
    *   }
    *
    * Failure:
    * 	~Success
    *
    * Errors:
    * 	6e006	=> "Der Overlay-Hidehandler wurde zurückgewiesen, da dieser bereits definiert wurde!"
    */
	function overlay_hidehandler($function, callback)
	{
		if(typeof $function == 'function') // nur wenn auch eine Funktion übergeben wurde
		{
			if($overlay_hidehandler == undefined) // sofern noch keine Overlay-Hidehandler definiert wurde
			{
				$overlay_hidehandler = $function;

				if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
			}
			else
			{
				if(typeof callback == 'function'){callback({'success': false, 'errors': {6e006: 'Der Overlay-Hidehandler wurde zur&uuml;ckgewiesen, da dieser bereits definiert wurde!'}, 'data': false});}
			}
		}
		else
		{
			if(typeof callback == 'function'){callback({'success': false, 'errors': null, 'data': false});}
		}
	}





   /* Blendet eine Hinweismeldung in der Kopfzeile des Overlays ein. Die Farbe der
   	* Meldung kann dabei frei definiert werden um Success / Failure / Note zu
   	* signalisieren.
   	* Es ist jedoch zu beachten, dass nur ein relativ kleiner Platz für die
   	* Nachricht zur Verfügung steht! Alles was nicht passt wird einfach abgeschnissten!
   	*
   	* Version: 	1.0.0
   	* Stand: 	26. November 2015
   	*
   	* Input:
   	* 	$color 		: string 	= die gewünschte Farbe der Meldung (CSS Farbe)
   	* 	$message	: string 	= die einzublendende Nachricht
   	*
   	* Success:
   	* 	none
   	*
   	* Failure:
   	* 	none
   	*
   	* Errors:
   	* 	none
    */
	function overlay_hint($color, $message)
	{
		var $object = $('#overlay #headline #hint'); // Bereich für die Meldungen

		$.when($object.fadeOut(250)).then(function() // ggf. bereits vorhandene Meldung zunächst ausblenden
		{
			$object.css('color', $color); // Farbe für die Meldung setzen
			$object.html($message); // Nachricht einfügen

			$.when($object.fadeIn(500)).then(function() // Nachricht einblenden
			{
				setTimeout(function() // Zeit um die Nachricht zu lesen
				{
					$.when($object.fadeOut(500)).then(function() // Nachricht wieder ausblenden
					{
						$object.html(''); // Bereich für Meldungen wieder leeren
					});
				}, 2000);
			});
		});
	}





   /* Über diese Funktion kann (versucht) werden das Anzeigen des Overlays
   	* zu verhindern, sofern dieses beim Laden automatisch eingeblendet werden
   	* sollte. Zudem kann Daten-Array übergeben werden um ggf. eine Fehlermeldung
   	* anzuzeigen.
   	*
   	* Version: 	1.0.0
   	* Stand: 	26. November 2015
   	*
   	* Input:
   	* 	$meta 	: array
   	* 	{
   	* 		['success']	=> : boolean
   	* 		['errors']	=> array
   	* 		{
   	* 			[: string] 	=> : string
   	* 		}
   	* 		['data']	=> : mixed
   	* 	}
   	*
   	* Success:
   	* 	none
   	*
   	* Failure:
   	* 	none
   	*
   	* Errors:
   	* 	none
   	*/
	function overlay_cancel($meta)
	{
		$overlay_cancel = true;

		overlay_close(function()
		{
			$overlay_state = 0; // sicherheitshalber noch mal resetten (falls Timeing zuvor nicht gepasst hat)
		});

		handle_errors("overlay", $meta); // Fehlermeldungen einblenden
	}





   /* Wird in einem Overlay die Struktur eines PPIN-Formulars verwendet, kann über diese
   	* Funktion die Value dieses Formulars ermittelt werden. Hierfür werden die Values
   	* der einzelnen Felder zusammengefügt und dann zurückgegeben.
   	*
   	* Version: 	1.0.0
   	* Stand: 	26. November 2015
   	*
   	* Input:
   	* 	$object 	: jQuery-Object 	= das Objekt für alle betroffenen Input-Felder
   	*
   	* Success:
   	* 	: string
   	*
   	* Failure:
   	* 	~Success
   	*
   	* Errors:
   	* 	none
    */
	function overlay_ppin_value($object)
	{
		var $value = $object.map(function() // für alle Input Felder
		{
			return $(this).val(); // Value zurückgeben
		})
		.get().join(''); // Values zusammenfügen

		return $value;
	}





   /* Wird in einem Overlay die Struktur eines PPIN-Formulars verwendet, kann diese
   	* Funktion in einem Keyup-Event-Handler verwendet werden um dem Formular seine
   	* gewohnte Funktionalität (vor-/zurückspringen beim Eintippen) verleihen.
   	*
   	* Version: 	1.0.1
   	* Stand: 	08. Dezember 2015
   	*
   	* Input:
   	* 	$this 		: jQuery-Objekt 	= das vom Event-Handler übergebene this-Objekt
   	* 	$key 		: integer			= die ID der gedrückten Taste, die vom Event-Handler übergeben wurde
   	* 	callback 	: function 			= wird zum Ende aufgerufen, wobei der Index des neu selektierten Feldes übergeben wird
   	*
   	* Success:
   	* 	array
   	* 	{
   	* 		['success']	=> true : boolean
   	* 		['errors']	=> null : null
   	* 		['data']	=> 		: integer	= der Index des nun fokusierten Feldes
   	* 	}
   	*
   	* Failure:
   	* 	~Success
   	*
   	* Errors:
   	* 	none
    */
	function overlay_ppin_keyup($this, $key, callback)
	{
		var $index = $($this).index(); // Position des aktuellen Feldes ermitteln

		if($key.which == 8) // wird die Backspace Taste gedrückt
		{
			$($this).parent().children('input[type="password"]').eq((($index - 1) < 0 ? 0 : $index - 1)).focus(); // vorheriges Feld fokusieren
			//$index--;
		}
		else
		{
			// wenn eine der folgenden Tasten gedrückt wird:
			// 48-57   = 0-9
			// 65-90   = a-z
			// 96-111  = numpad
			// 186-222 = comma, period, back slash etc.
			if(($key.which >= 48 && $key.which <= 57) || ($key.which >= 65 && $key.which <= 90) || ($key.which >= 96 && $key.which <= 111) || ($key.which >= 186 && $key.which <= 222))
    		{
    			$($this).parent().children('input[type="password"]').eq($index + 1).focus(); // nächstes Feld fokusieren
    			$index++;
    		}
    		else
    		{
    			$index++;
    		}
		}

		if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': $index});} // Index des nun fokusierten Feldes übergeben
	}
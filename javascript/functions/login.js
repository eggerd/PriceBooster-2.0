
   /* Übergibt die vom Benutzer eingegebenen Login-Daten an PHP, welches
    * den eigentlichen Login ausführt. Je nach Antwort von PHP wird dann
    * entsprechend verfahren.
    *
    * Version:  1.0.2
    * Stand: 	12. September 2015
    *
    * Input:
    *   $station    : string    = die eingegebene Stationsnummer
    *   $password   : string    = das eingegebene Passwort
    *   callback    : callback
    *
    * Success:
    *   call->callback(array
    *   (
    *       ['success'] = true  : boolean
    *       ['errors']  = array
    *       {
    *           [error_code]    => :string
    *       }
    *       ['data']    = :boolean
    *   ));
    *
    * Failure:
    *   call->callback(array
    *   (
    *       ['success'] = false  : boolean
    *       ['errors']  = array
    *       {
    *           [error_code]    => :string
    *       }
    *       ['data']    = true  : boolean
    *   ));
    *
    * Errors:
    *   passed  ->  login() (PHP Function)
    *   1e007   =   "Die Anfrage konnte auf Grund von Verbindungsproblemen nicht bearbeitet werden!"
    *   1e009	=	"Der Login ist fehlgeschlagen, da der Server eine ungültige Antwort geliefert hat!"
    */
    function login($station, $password, callback)
    {
        $.ajax({
            url: './javascript/ajax/login.php', // PHP Login Funktion aufrufen
            data: {station: $station, password: $password}, // Passwort wird verschlüssel übertragen
            type: 'post',
            success: function($ajaxReturn)
            {
                try
                {
                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
                }
                catch(err) // falls die Umwandlung fehlschlägt
                {
                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'1e009': 'Der Login ist fehlgeschlagen, da der Server eine ung&uuml;ltige Antwort geliefert hat!'}, 'data': $ajaxReturn});}
                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
                }

                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
                {
                    if(typeof callback == 'function'){callback({'success': true, 'errors': $data['errors'], 'data': $data['data']});}
                }
                else
                {
                    if(typeof callback == 'function'){callback({'success': false, 'errors': $data['errors'], 'data': null});}
                }
            },
            error: function()
            {
                if(typeof callback == 'function'){callback({'success': false, 'errors': {'1e007': 'Die Anfrage konnte auf Grund von Verbindungsproblemen nicht bearbeitet werden!'}, 'data': null});}
            }
        });
    }





   /* Wird beim Klick auf den Login-Button aufgerufen und kümmert sich
    * um die Sperrung des Formulars, sowie um die Auswertung des Logins.
    * Somit kümmert sich diese Funktion auch um die Ausgabe von Fehler-
    * meldungen bzw. startet das Beenden des Logins auf Client-Seite.
    *
    * Version:  1.0.0
    * Stand:    02. Januar 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function trigger_login()
    {
        remove_notifications('area', 1, function() // erst alle alten Meldungen des Logins ausblenden
        {
            $('#login input').fadeTo(250, 0.5, function() // Formular verblassen
            {
                $('#login input').attr('disabled', 'disabled'); // Formular deaktivieren
            });

            login($('#login input').eq(0).val(), $('#login input').eq(1).val(), function($return) // Login durchführen
            {
                if($return['success'] == true && $return['data'] == true) // wenn der Login erfolgreich war
                {
                    var $support = ($return['errors'] != null && '1n016' in $return['errors']) ? true : false; // falls der Magic-Error 1n016 zurückgegeben wurde == Support-Login
                    finish_login($support); // nun macht diese Funktion weiter
                }
                else
                {
                    for (var $code in $return['errors']) // für alle übermittelten Fehlermeldungen
                    {
                        create_notification($code, $return['errors'][$code], function() // Meldungen erstellen
                        {
                            // Formular wieder freigeben
                            $('#login input').removeAttr('disabled');
                            $('#login input').fadeTo(250, 1);
                        });
                    }
                }
            });
        });
    }





   /* Wird aufgerufen wenn der Login bestötigt wurde und kümmert
    * sich um die nötigen Änderungen im DOM um den Login auch
    * auf Client-Seite zu beenden. Anschließend wird die
    * Service-Routine gestartet.
    *
    * Version:  1.0.4
    * Stand:    17. Dezember 2015
    *
    * Input:
    *   $support 	: boolean 	= gibt an, ob es sich um einen Support-Login handelt
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function finish_login($support)
    {
        $('#login').fadeOut(1000, function() // Login-Formular ausblenden
        {
            prepare_reference_container(null, 'loading'); // Loading-Placeholder einblenden

            var $station = $('#login input').first().val().toUpperCase(); // Stationsnummer auslesen und UpperCase anwenden
            $('#login input[type!=submit]').val(''); // Login-Formular leeren

            if($support == undefined || $support == false) // sofern es sich nicht um einen Support-Login handelt
            {
            	$('#header #navi h3').html($station); // Stationsname ausgeben
        	}
        	else // es handelt sich um einen Support-Login
        	{
        		$('#header #navi h3').html('<span id="support">SUPPORT</span> <span id="spacer">/</span> ' + $station); // Stationsnummer mit spezieller Markierung ausgeben
        		$('#debug').show(); // Log einblenden
        	}

            $('#header #navi').fadeIn(1000, function() // Navi einblenden
            {
                clearTimeout($runtime_timeout); // aktuelle Runtime beenden
                runtime(); // Runtime wieder starten - Login ist vollständig abgeschlossen!

            });
        });
    }





   /* Erfragt beim Server ob der aktuelle Login gültig ist. Der Server antwortet
    * hierauf einfach mit TRUE oder FALSE.
    *
    * Version:  1.0.1
    * Stand:    13. September 2015
    *
    * Input:
    *   callback    : callback
    *
    * Success:
    *   call->callback(array
    *   (
    *       ['success'] = true  : boolean
    *       ['errors']  = array
    *       {
    *           [error_code]    => :string
    *       }
    *       ['data']    = :boolean
    *   ));
    *
    * Failure:
    *   call->callback(array
    *   (
    *       ['success'] = false  : boolean
    *       ['errors']  = array
    *       {
    *           [error_code]    => :string
    *       }
    *       ['data']    = true  : boolean
    *   ));
    *
    * Errors:
    *   passed  ->  authenticated() (PHP Function)
    *   1e010   =   "Die Gültigkeit der Login-Session konnte auf Grund einer ungültigen Server Antwort nicht überprüft werden!"
    *   1e011   =   "Die Gültigkeit der Login-Session konnte auf Grund von Verbindungsproblemen nicht überprüft werden!"
    */
    function authenticated(callback)
    {
        $.ajax({
            url: './javascript/ajax/login.php', // PHP Login Funktion aufrufen
            data: {authenticated: null}, // Login-Status abfragen
            type: 'post',
            success: function($ajaxReturn)
            {
                try
                {
                   var $data = jQuery.parseJSON($ajaxReturn); // PHP Antwort in Array umwandeln
                }
                catch(err) // falls die Umwandlung fehlschlägt
                {
                   if(typeof callback == 'function'){callback({'success': false, 'errors': {'1e010': 'Die G&uuml;ltigkeit der Login-Session konnte auf Grund einer ung&uuml;ltigen Server Antwort nicht &uuml;berpr&uuml;ft werden!'}, 'data': $ajaxReturn});}
                   return; // Funktion beenden um Fehlermeldungen zu vermeiden!
                }

                if($data['success'] == true) // wenn die Verarbeitung der Daten erfolgreich war
                {
                    if(typeof callback == 'function'){callback({'success': true, 'errors': $data['errors'], 'data': $data['data']});}
                }
                else
                {
                    if(typeof callback == 'function'){callback({'success': false, 'errors': $data['errors'], 'data': null});}
                }
            },
            error: function()
            {
                if(typeof callback == 'function'){callback({'success': false, 'errors': {'1e011': 'Die G&uuml;ltigkeit der Login-Session konnte auf Grund von Verbindungsproblemen nicht &uuml;berpr&uuml;ft werden!'}, 'data': null});}
            }
        });
    }





   /* Wird aufgerufen wenn der Logout beendet wurde. Kümmert sich um
    * die benötigten Anderungen im DOM um den Logout auch dort
    * zu visualisieren.
    *
    * Version:  1.0.2
    * Stand:    10. Juni 2016
    *
    * Input:
    *   none
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function finish_logout()
    {
        overlay_close();

        $('#menu').fadeOut(500); // Menü ausblenden
        $('#header #navi').fadeOut(1000, function() // Navi ausblenden
        {
            $('#header #navi h3').html(''); // Stationsnummer aus der Navi löschen
        });

        $('#reference-bottom').fadeOut(500);
        $.when($('#references').children().fadeOut(1000)).then(function() // Referenzen ausblenden
        {
            $('#references').children().remove(); // ausgeblendete Referenzen löschen
            $references = new Array(); // Referenz-Array resetten, da alle Referenzen gelöscht wurden
            $price_update_timeout = 0; //Runtime-Timeout für Preisabfragen zurücksetzen

            $('#login input').css('opacity', ''); // Verblassung aufheben
            $('#login input').removeAttr('disabled', 'disabled'); // Formular aktivieren

            $('#login').fadeIn(1000);
        });
    }





   /* Beendet die Login-Session des Clients auf Seite des Servers als auch auf
    * der Seite des Clients.
    *
    * Version:  1.1.1
    * Stand:    12. September 2015
    *
    * Input:
    *   none
    *
    * Success:
    *   none
    *
    * Failure:
    *   none
    *
    * Errors:
    *   none
    */
    function logout()
    {
        finish_logout();

        $.ajax({
            url: './javascript/ajax/login.php', // PHP Login Funktion aufrufen
            data: {logout: null}, // Logout auf Seite des Servers durchführen (nicht zwingend notwendig!)
            type: 'post',
            success: function($ajaxReturn)
            {
        		// nothing to do here, so far
            },
            error: function()
            {
            	// nothing to do here, so far
            },
            complete: function()
            {
            	$.removeCookie('station_id', { path: '/'});
        		$.removeCookie('session_id', { path: '/'});
        		$.removeCookie('support', { path: '/'}); // falls überhaupt gesetzt, einfach mit löschen
            }
        });
    }





   /* Prüft nur anhand des Session-Cookies, ob der Client eingeloggt sein könnte. Wird
   	* z.B. benötigt, falls keine Verbindung zum Server besteht, aber dennoch anhand
   	* des Login-Status entschieden werden muss. In diesem Fall wird anhand des Session-
   	* Cookies von einem gültigen Login ausgegangen.
   	*
   	* Version: 	1.0.0
   	* Stand: 	08. Oktober 2015
   	*
   	* Input:
   	* 	callback 	: function 	= wird zum Ende der Funktion aufgerufen
   	*
   	* Success:
   	* 	call->callback(array
    *   (
    *       ['success'] = true  : boolean
    *       ['errors']  = null
    *       ['data']    = 		: boolean 	= true: scheint eingeloggt, false: nicht eingeloggt
    *   ));
    *
    * Failure:
    * 	~Success
   	*
   	* Errors:
   	* 	none
    */
    function potentially_authenticated(callback)
    {
    	if($.cookie('session_id') != undefined) // sofern das Session-Cookie gesetzt ist
    	{
			if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});} // von gültigem Login ausgehen
    	}
    	else
    	{
    		if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': false});} // scheint nicht angemeldet zu sein
    	}
    }
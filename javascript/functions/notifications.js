
   /* Erzeugt eine Fehlermeldung / Warnung / Infomeldung und gibt diese in
    * dem dafür vorgesehenen Bereich der Seite aus. Anhand des übergebenen
    * Error-Codes wird ermittelt ob es sich bei der Meldung um einen Error,
    * Warning oder Note handelt.
    *
    * Version:  1.1.0
    * Stand:    28. Juli 2015
    *
    * Input:
    *   $code       : string    = der Fehlercode (z.B. 1e001 / 1w002 / 1n003)
    *   $message    : string    = die Nachricht der Meldung
    *   callback    : callback
    *
    * Success:
    *   call->callback(array
    *   (
    *       ['success'] = true      : boolean
    *       ['errors']  = null      : null
    *       ['data']    = $object   : object
    *   ));
    *
    * Failure:
    *   ~Success
    *
    * Errors:
    *   none
    */
    function create_notification($code, $message, callback)
    {
        var $distinct = true; // gibt an ob die Meldung einzigartig angezeigt wird

        // remove_notifications() kann nicht verwendet werden, da in der Runtime ggf. paralell diese Funktion
        // aufgerufen wird und es somit dennoch zu multiplen Anzeigen kommen kann. Da bei der nachfolgenden
        // Methode die Meldung zu keinem Zeitpunkt völlig entfernt wird, ist sichergestellt das jeder Aufruf
        // zum Ergebnis kommt, dass die Meldung bereits vorhanden ist
        $('#notifications .notification').each(function() // für alle vorhandenen Meldungen
        {
            var $this = $(this);

            if($this.data('code') == $code) // wenn das aktuelle Objekt die gleiche Meldung anzeigt
            {
                $distinct = false; // Meldung als nicht mehr einzigartig markieren

                if($this.is(':visible'))
                {
                    // erzeugt den Effejkt, dass die Meldung "aktualisiert" wird
                    $this.slideUp(500, function() // Meldung ausblenden
                    {
                        setTimeout(function() // kurz warten
                        {
                            $this.slideDown(500); // Meldung einfach wieder einblenden
                        }, 500);
                    });
                }
            }
        });


        if($distinct == true) // Notification nur erzeugen, wenn sie bisher nicht vorhanden ist
        {
            var $type;
            switch($code.match(/e|w|n|s/)[0]) // Typ der Nachricht, anhand des Buchstabend im Fehlercode, ermitteln
            {
                case "e": $type = 'error'; break;
                case "w": $type = 'warning'; break;
                case "n": $type = 'note'; break;
                case "s": $type = 'success'; break;
            }

            // Objekt für die Fehlermeldung erzeugen
            var $object = $(
            '<div class="notification ' + $type + '" style="display: none">' +
                '<img src="./images/' + $type + '.png" />' +
                '<div>' +
                    '<h5>Meldung ' + $code + '</h5>' +
                    '<p>' + $message + '</p>' +
                '</div>' +
            '</div>');

            // der Fehlermeldung alle ggf. benötigten Informationen zuweisen
            $object.data('code', $code);
            $object.data('type', $type);
            $object.data('area', $code.match(/[1-9]{1,2}/)); // die vorderen Zahlen im Fehlercode ermitteln

            $('#content #notifications').append($object); // Meldung in DOM einfügen
            $('#content .notification').last().slideDown(500, function() // Meldung einblenden
            {
                if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': $object});}
            });
        }
    }





   /* Entfernt entweder alle oder eine bestimmte Fehlermeldung bzw. eine
    * bestimmte Gruppe von Fehlermeldungen. Als Kriterium diehnen hierzu
    * der Fehlercode (z.B. 1e001), der Bereich (z.B. 1 bei Code 1e003)
    * oder die Art der Meldung (z.B. error). Werden keine Parameter
    * übergeben, oder diese mit 'null' definiert, werden einfach alle
    * vorhandenen Meldungen entfernt.
    *
    * Version:  1.1.0
    * Stand:    05. Januar 2016
    *
    * Input:
    *   $where      : string    = gibt die Art des Kriteriums an { code, type, area }
    *   $equals     : mixed     = gibt den Wert des Kriteriums an (z.B. ein Fehlercode; undefined == alle)
    *   callback    : callback
    *
    * Success:
    *   call->callback(array                // wird für jede gelöschte Meldung aufgerufen!
    *   (
    *       ['success'] = true  : boolean
    *       ['errors']  = null  : null
    *       ['data']    = true  : boolean
    *   ));
    *
    * Failure:
    *   ~Success
    *
    * Errors:
    *   none
    */
    function remove_notifications($where, $equals, callback)
    {
        if(typeof $where == 'undefined' || $where == null) // wenn $where nicht definiert wurde einfach alle löschen
        {
            $.when($('#notifications .notification:data(type)').slideUp(500)).then(function() // wenn alle Meldungen ausgeblendet wurden (auch wenn keine vorhanden sind!)
            {
                $('#notifications .notification:data(type)').remove(); // alle Meldungen löschen

                if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
            });
        }
        else
        {
            var $max_objects = $('#notifications .notification:data('+$where+')').length; // Anzahl der vorhandenen Meldungen
            var $done = 0; // Anzahl der verarbeiteten Meldungen

            // Abfrage wird benötigt, da die callback sonst nicht aufgerufen wird,
            // sollten keine Meldungen vorhanden sein - da die Foreach nicht läuft!
            if($max_objects > 0) // sofern überhaupt Meldungen vorhanden sind
            {
                $('#notifications .notification:data(' + $where + ')').each(function() // für alle vorhandenen Meldungen
                {
                    if($equals == undefined || $(this).data($where) == $equals) // falls die Meldung dem Kriterium entspricht
                    {
                        $.when($(this).slideUp(500)).then(function() // wenn die Meldung ausgeblendet wurde
                        {
                            $(this).remove(); // und Meldung löschen
                            $done++;

                            if($done == $max_objects) // falls gerade das Ende des Loops erreicht wurde
                            {
                                if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
                            }
                        });
                    }
                    else
                    {
                        $done++;

                        if($done == $max_objects) // falls gerade das Ende des Loops erreicht wurde
                        {
                            if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
                        }
                    }
                });
            }
            else
            {
                if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
            }
        }
    }





   /* Über diese Funktion wird die Wiedergabe von Notification-Sounds gesteuert
    * und stellt somit einen zentralen Regulierungspunkt dar. Der Funktion kann
    * durch einen nummerischen Wert mitgeteilt werden, welcher Sound abgespielt
    * werden soll. Die Funktion beachtet unter allen Umständen die Einstellung
    * $PLAY_NOTIFICATION_SOUNDS.
    *
    * Version:  1.0.0
    * Stand:    04. August 2015
    *
    * Input:
    *     $sound    : integer   = der Index des abzuspielenden Sounds
    *     callback  : callback
    *
    * Success-Output:
    *     call -> callback(array
    *     (
    *         ['success']   = true
    *         ['errors']    = null
    *         ['data']      = true
    *      ));
    * Failure-Output:
    *     call -> callback(array
    *     (
    *         ['success']   = false
    *         ['errors']    = null
    *         ['data']      = false
    *      ));
    *
    */
    function play_notification_sound($sound, callback)
    {
        if($PLAY_NOTIFICATION_SOUNDS == true)
        {
            //ion.sound.play('stone'); // <== wird benötigt, falls die Audiowiedergabe mit <audio> nicht unterstützt wird
            $('#audio')[$sound].play(); // Ton abspielen

            $('#audio').on( "ended", function() // wenn der Ton fertig abgespielt wurde (rekursive "Funktion")
            {
                if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': true});}
            });
        }
        else
        {
            if(typeof callback == 'function'){callback({'success': true, 'errors': null, 'data': false});}
        }
    }





   /* Über diese Funktion sollten Fehlermeldungen erzeugt werden. Sie verarbeitet alle
   	* übergebenen Fehlermeldungen, erzeugt die entsprechenden Log-Einträge, blendet das
   	* Overlay aus und schließlich die Fehlermeldungen ein.
    *
    * Version: 	1.0.1
    * Stand: 	18. Dezember 2015
    *
    * Input:
    * 	$area 	: string 	= der Bereich in dem die Errors aufgetreten sind
    * 	$data 	: array
    * 	{
    * 		['success'] : boolean
    * 		['errors']	: array
    * 		{
    * 			[: string]	= : string
    * 		}
    * 		['data']	: mixed
    * 	}
    *
    * Success:
    * 	none
    *
    * Failure:
    * 	none
    *
    * Errors:
    * 	none
    */
    function handle_errors($area, $data)
    {
    	if($.type($data) == 'object') // nur wenn auch ein Array / Object übergeben wurde
    	{
	    	if($data['errors'] != null)
	    	{
		    	overlay_hide(); // Overlay ausblenden, falls zurzeit angezeigt

		    	for(var $i in $data['errors']) // für jeden übermittelten Error
		        {
                    var $type;
		        	switch($i.match(/e|w|n|s/)[0]) // Typ der Nachricht, anhand des Buchstabend im Fehlercode, ermitteln
		            {
		                case "e": $type = 'error'; break;
		                case "w": $type = 'warning'; break;
		                case "n": $type = 'note'; break;
		                case "s": $type = 'success'; break;
		            }

		            // Debug Einträge anlegen
		            debug($area, $debug_last_tab[$area] + 1, $type, $i + ' | ' + $data['errors'][$i]);

		            if(typeof $data['data'] != 'undefined')
		            {
		            	debug_append_data($area, $data['data']);
	            	}

		            create_notification($i, $data['errors'][$i]); // Fehlermeldung einblenden
		        }
	        }
        }
    }
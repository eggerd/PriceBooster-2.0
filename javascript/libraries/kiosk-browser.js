
	// Stand: 10. August 2016
	// Kompatibler App-Build: 155 (2.0.38)

	function showAndroidToast(toast) 
	{
		if(typeof Android != "undefined")
		{
			Android.showToast(toast);
		}
	}

	//OPENS THE BUILT IN BARCODE SCANNER
	function openAndroidBarcodeScanner() 
	{
		if(typeof Android != "undefined")
		{
			Android.openBarcodeScanner();
		}
	}
	
	//OPENS THE BUILT IN BARCODE SCANNER WITH DATA
	function openAndroidBarcodeScannerData(data) 
	{
		if(typeof Android != "undefined")
		{
			Android.openBarcodeScanner(data);
		}
	}

	//OPENS THE BUILT IN BARCODE SCANNER WITH INPUT FIELD PARAMETER
	function openAndroidBarcodeScannerInput(input) 
	{
		if(typeof Android != "undefined")
		{
			Android.openBarcodeScannerInput(input);
		}
	}
	
	//OPENS THE SETTINGS ACTIVITY (ALWAYS PROMPTS FOR PASSWORD)
	function openSettings()	
	{
		if(typeof Android != "undefined")
		{
			Android.openSettings();
		}
	}

	//EXITS KIOSK BROWSER, WHEN REQUIREPASSWORD IS SET TO FALSE THE PASSWORD DIALOG WILL NOT BE DISPLAYED
	function exitKiosk(requirepassword) 
	{
		if(typeof Android != "undefined")
		{
			Android.exitKiosk(requirepassword);
		}
	}
	
	//VIBRATE FOR SPECIFIED MILLSECONDS
	function vibrate(milliseconds) 
	{
		if(typeof Android != "undefined")
		{
			Android.vibrate(milliseconds);
		}
	}
	
	//PLAY DEFAULT NOTIFICATION SOUND
	function playNotificationSound(milliseconds) 
	{
		if(typeof Android != "undefined")
		{
			Android.playNotificationSound();
		}
	}
	
	//SHOW KEYBOARD
	function showKeyboard() 
	{
		if(typeof Android != "undefined")
		{
			Android.showKeyboard();
		}
	}
	
	//HIDE KEYBOARD
	function hideKeyboard() 
	{
		if(typeof Android != "undefined")
		{
			Android.hideKeyboard();
		}
	}
	
	//TOGGLE WIFI
	function setWiFiEnabled(enabled) 
	{
		if(typeof Android != "undefined")
		{
			Android.setWiFiEnabled(enabled);
		}
	}
	
	function createLogEntry(url) 
	{
		if(typeof Android != "undefined")
		{
			Android.createLogEntry(url);
		}
	}
	
	//RETRIEVES CURRENT BATTERY LEVEL & DISPLAYS IN TOAST
	function getDeviceBatteryLevel() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getBatteryLevel();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET MAC ADDRESS
	function getMacAddress() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getMacAddress();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET IMEI
	function getIMEI() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getIMEI();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET SERIAL NUMBER
	function getSerialNumber() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getSerialNumber();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET BRAND
	function getBrand() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getBrand();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET MODEL
	function getModel() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getModel();
		}
		else
		{
			return undefined;
		}
	}
	
	//SET SCREEN BRIGHTNESS
	function setScreenBrightness(level) 
	{
		if(typeof Android != "undefined")
		{
			Android.setScreenBrightness(level);
		}
	}
	
	//GET SCREEN BRIGHTNESS
	function getScreenBrightness() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getScreenBrightness();
		}
		else
		{
			return undefined;
		}
	}
	
	//OPENS WIFI SETTINGS, WHEN REQUIREPASSWORD IS SET TO FALSE THE PASSWORD DIALOG WILL NOT BE DISPLAYED
	function openWIFISettings(requirepassword) 
	{
		if(typeof Android != "undefined")
		{
			Android.openWIFISettings(requirepassword);
		}
	}
	
	//GET KIOSK HOME PAGE (RETURNS STRING)
	function getHomePage() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getHomePage();
		}
		else
		{
			return undefined;
		}
	}
	
	//LOAD KIOSK HOME PAGE - delete cache, history etc if enabled
	function loadHomePage() 
	{
		if(typeof Android != "undefined")
		{
			Android.loadHomePage();
		}
	}
	
	//SET KIOSK HOME PAGE
	function setHomePage(url) 
	{
		if(typeof Android != "undefined")
		{
			Android.setHomePage(url);
		}
	}
	
	//GET WIFI SSID
	function getWIFISSID() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getWIFISSID();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET IP ADDRESS
	function getIPAddress() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getIPAddress();
		}
		else
		{
			return undefined;
		}
	}
	
	//OPEN APPLICATION
	function openApplication(packagename) 
	{
		if(typeof Android != "undefined")
		{
			Android.openApplication(packagename);
		}
	}
	
	//OPEN APPLICATION, GIVE KIOSK BROWSER FOCUS AFTER X INTERVAL
	function openApplication(packagename, regainFocusInterval) 
	{
		if(typeof Android != "undefined")
		{
			Android.openApplication(packagename, regainFocusInterval);
		}
	}
	
	//TURN SCREEN OFF
	function turnScreenOff() 
	{
		if(typeof Android != "undefined")
		{
			Android.turnScreenOff();
		}
	}
	
	//PRINT PAGE VIA GOOGLE CLOUD PRINT
	function printPage() 
	{
		if(typeof Android != "undefined")
		{
			Android.printPage();
		}
	}
	
	//BROADCAST INTENT
	function broadcastIntent(intent, allowForeground) 
	{
		if(typeof Android != "undefined")
		{
			Android.broadcastIntent(intent, allowForeground);
		}
	}
	
	function broadcastIntentExtras(intent, allowForeground) 
	{
		if(typeof Android != "undefined")
		{
			var extras = [["keyname1","value1"],["keyname2","value2"]];
			Android.broadcastIntent(intent, allowForeground, extras.toString());
		}
	}
	
	function startActivityWithIntent(intent, allowForeground, packageName) 
	{
		if(typeof Android != "undefined")
		{
			Android.startActivityWithIntent(intent, allowForeground, packageName);
		}
	}
	
	function startService(intent) 
	{
		if(typeof Android != "undefined")
		{
			var extras = [["keyname1","value1"]];
			Android.startService(intent, extras.toString());
		}
	}
	
	//IS KIOSK BROWSER IN FOCUS?
	function isAppInFocus() 
	{
		if(typeof Android != "undefined")
		{
			return Android.isAppInFocus();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET UNIQUE IDENTIFIER
	function getRemoteIdentifier() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getRemoteIdentifier();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET UNIQUE IDENTIFIER
	function getUniqueIdentifier() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getUniqueIdentifier();
		}
		else
		{
			return undefined;
		}
	}
	
	//GET KIOSK TITLE
	function getKioskTitle() 
	{
		if(typeof Android != "undefined")
		{
			return Android.getKioskTitle();
		}
		else
		{
			return undefined;
		}
	}
	
	//SET KIOSK TITLE
	function setKioskTitle(title) 
	{
		if(typeof Android != "undefined")
		{
			Android.setKioskTitle(title);
		}
	}
	
	//GET CHARGING STATE
	function isCharging() 
	{
		if(typeof Android != "undefined")
		{
			return Android.isCharging();
		}
		else
		{
			return undefined;
		}
	}
	
	//RESTART APPLICATION
	function restartApplication() 
	{
		if(typeof Android != "undefined")
		{
			Android.restartApplication();
		}
	}
	
	//GO BACK
	function back() 
	{
		if(typeof Android != "undefined")
		{
			Android.back();
		}
	}
	
	//GO FORWARD
	function forward() 
	{
		if(typeof Android != "undefined")
		{
			Android.forward();
		}
	}
	
	function setScreenOrientation(orientation) 
	{
		if(typeof Android != "undefined")
		{
			Android.setScreenOrientation(orientation)
		}
	}
	
	function registerRemote() 
	{
		if(typeof Android != "undefined")
		{
			Android.registerRemote();
		}
	}
	
	function registerRemoteKey(key) 
	{
		if(typeof Android != "undefined")
		{
			Android.registerRemoteKey(key);
		}
	}
	
	//GET REMOTE REGISTRATION STATUS
	function isRemotelyRegistered() 
	{
		if(typeof Android != "undefined")
		{
			return Android.isRemotelyRegistered();
		}
		else
		{
			return undefined;
		}
	}	
	
	//REGAIN FOCUS
	function regainFocus() 
	{
		if(typeof Android != "undefined")
		{
			return Android.regainFocus();
		}
		else
		{
			return undefined;
		}
	}
	
	//DELAY SCREENSAVER
	function delayScreensaver() 
	{
		if(typeof Android != "undefined")
		{
			return Android.delayScreensaver();
		}
		else
		{
			return undefined;
		}
	}
	
	//STOP SCREENSAVER
	function stopScreensaver() 
	{
		if(typeof Android != "undefined")
		{
			return Android.stopScreensaver();
		}
		else
		{
			return undefined;
		}
	}
	
	function getVolumeLevel(type) 
	{
		if(typeof Android != "undefined")
		{
			return Android.getVolumeLevel(type);
		}
		else
		{
			return undefined;
		}
	}
	
	//GET MAX VOLUME LEVEL FOR SPECIFIED TYPE
	function getMaxVolumeLevel(type) 
	{
		if(typeof Android != "undefined")
		{
			return Android.getMaxVolumeLevel(type);
		}
		else
		{
			return undefined;
		}
	}
	
	//SET VOLUME LEVEL
	function setVolumeLevel() 
	{
		if(typeof Android != "undefined")
		{
			//NOTE IF THE DEVICE IS SET TO DO NOT DISTURB THE VOLUME CANNOT BE CHANGED
			//REQUIRES WHOLE NUMBER, VOLUME RANGE WILL DEPEND ON THE TYPE BEING SET
			//50% VOLUME 
			var middle = Math.round(Android.getMaxVolumeLevel(STREAM_NOTIFICATION)/2);
			showAndroidToast(middle);
			return Android.setVolumeLevel(STREAM_NOTIFICATION, middle);
		}
		else
		{
			return undefined;
		}
	}
	
	//SHOW BOOKMARKS BAR
	function showBookmarks() 
	{
		if(typeof Android != "undefined")
		{
			Android.showBookmarks();
		}
	}
	
	//HIDE BOOKMARKS BAR
	function hideBookmarks() 
	{
		if(typeof Android != "undefined")
		{
			Android.hideBookmarks();
		}
	}
	
	//REFRESH REMOTE MANAGEMENT PROFILE
	function refreshProfile() 
	{
		if(typeof Android != "undefined")
		{
			Android.refreshProfile();
		}
	}
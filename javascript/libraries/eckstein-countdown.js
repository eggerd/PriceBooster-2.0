	// In diesem Array wird für jeden erzeugten Countdown ein Eintrag hinterlegt, in dem
	// die Zeit, das Format und auch die Referenz auf das zugehörige setTimeout()
	// gespeichert wird. Die Daten werden jeweil unter der Target-ID des Countdowns
	// abgelegt, so dass diese beim Erzeugen eines weiteren Countdowns herangezogen werden
	// können.
	var $countdown_controler = new Array();



   /* Diese Funktion ist für die Erzeugung und Vernichtung von Countdowns zuständig.
   	* Soll ein neuer Countdown erzeugt werden, werden die zugehörigen Daten im
   	* Controler-Array gespeichert und an die Helper-Funktion übergeben, wodurch der
   	* Countdown erzeugt wird.
   	* Sollte für das selbe Ziel-Element ein zweiter Countdown gestartet werden, wird
   	* der erste Countdown verworfen und durch den neuen ersetzt.
   	*
   	* Version: 	1.0.1
   	* Stand: 	26. Juli 2016
   	*
   	* Input:
   	* 	$time 	: integer 	= die Zeit in Sekunden, die der Countdown runterzählen soll
   	* 	$id 	: string 	= die ID des Ziel-Elementes, in dem der Countdown eingefügt werden soll (ohne führende Raute)
   	* 	$format : string 	= über diesen Wert kann das Ausgabe Format des Countdowns verändert werden
   	* 						  {h = 'hh:mm:ss', m = 'mm:ss', s = 'ss', default = 'd hh:mm:ss'}
   	*
   	* Success:
   	* 	none
   	*
   	* Failure:
   	* 	none
   	*
   	* Errors:
   	* 	none
	*/
	function countdown($time, $id, $format)
	{
		if($countdown_controler[$id] == undefined) // sofern im Controler-Array noch kein Eintrag vorhanden ist
		{
			$countdown_controler[$id] = {time: $time, format: $format, remaining: $time}; // Eintrag anlegen
			countdown_handler($time, $id, $format); // Countdown erzeugen
		}
		else // Eintrag ist bereits vorhanden - scheint schon einen Countdown zu geben
		{
			window.clearTimeout($countdown_controler[$id]['timeout']); // vorhandenen Countdown unterbrechen
			$countdown_controler[$id] = {time: $time, format: $format, remaining: $time}; // Daten überschreiben
			countdown_handler($time, $id, $format); // neuen Countdown erzeugen
		}
	}





/*
Author: Benjamin Eckstein
http://www.umingo.de/

You can use this code in any manner so long as the author's
name, Web address and this disclaimer is kept intact.
********************************************************
Usage Sample:
<div id="cID">Init<script>countdown(100000,'cID');</script></div>
*/

function countdown_handler(time,id,format)
{
    //time brauchen wir später noch
    var t = time;

    //Tage berechnen
    var d = Math.floor(t/(60*60*24)) % 24;

    // Stunden berechnen
    var h = Math.floor(t/(60*60)) % 24;


    // Minuten berechnen
    // Sekunden durch 60 ergibt Minuten
    // Minuten gehen von 0-59
    //also Modulo 60 rechnen
    var m = Math.floor(t/60) %60;

    // Sekunden berechnen
    var s = t %60;

    //Zeiten formatieren
    d = (d >  0) ? d+"d ":"";
    h = (h < 10) ? "0"+h : h;
    m = (m < 10) ? "0"+m : m;
    s = (s < 10) ? "0"+s : s;

    // Ausgabestring generieren
    var strZeit;
    switch(format) // Hinweis: Switch wurde zusätzlich eingefügt - nicht vom Autor!
    {
        case "h":
            strZeit = h + ":" + m + ":" + s;
            break;

        case "m":
            strZeit = m + ":" + s;
            break;

        case "s":
            strZeit = s;
            break;

        default:
            strZeit = d + h + ":" + m + ":" + s;
            break;
    }

    // Falls der Countdown noch nicht zurückgezählt ist
    if(time > 0)
    {
    	if($countdown_controler[id] != undefined) // Hinweis: IF wurde zusätzlich eingefügt - nicht vom Autor!
    	{
	        //Countdown-Funktion erneut aufrufen
	        //diesmal mit einer Sekunde weniger
	        $countdown_controler[id]['timeout'] = window.setTimeout('countdown_handler('+ --time+',\''+id+'\',\''+format+'\')',1000);
	        $countdown_controler[id]['remaining'] = time;
        }
    }
    else
    {
        //führe eine funktion aus oder refresh die seite
        //dieser Teil hier wird genau einmal ausgeführt und zwar
        //wenn die Zeit um ist.
        switch(format) // Hinweis: Switch wurde zusätzlich eingefügt - nicht vom Autor!
        {
            case "m":
                strZeit = "00:00";
                break;

            case "s":
                strZeit = "00";
                break;

            default:
                strZeit = "00:00:00";
                break;
        }

        $countdown_controler[id] = undefined; // Hinweis: Wurde zusätzlich eingefügt - nicht vom Autor!
    }

    // Ausgabestring in Tag mit id="id" schreiben
    var element = document.getElementById(id);

    if(element != null) // Hinweis: IF wurde zusätzlich eingefügt - nicht vom Autor!
    {
    	element.innerHTML = strZeit;
    }
    else
    {
    	if(typeof $countdown_controler[id] != 'undefined')
    	{
    		window.clearTimeout($countdown_controler[id]['timeout']); // Hinweis: clearTimeout wurde zusätzlich eingefügt - nicht vom Autor!
		}
    }
}

//Helfer Funktion erlaubt Counter auch ohne Timestamp
//countdown2(Tage,Stunden,Minuten,Sekunden,ID)
function countdown2(d,h,m,s,id)
{
    countdown(d*60*60*24+h*60*60+m*60+s,id);
}


    window.onload = function() // wird nach dem Laden der Seite ausgeführt
    {
        var $hide_menu_timer; // speichert den setTimeout() bis zum automatischen Schließen des Menüs


        debug('properties', 0, 'info', 'Version: ' + $SERVICE_VERSION);
        setScreenBrightness(255);
        check_kiosk_registration();


        $.ajaxSetup( // global für alle Ajax-Requests definieren
    	{
			data: {"tracking_id": request('tracking_id')} // TrackingID des Clients übermitteln
		});
		debug('properties', 0, 'info', 'Tracking ID: ' + request('tracking_id'));


        authenticated(function($return) // Session-Status abfragen
        {
        	if($return['success'] == true && $return['data'] == true) // wenn die Station angemeldet ist
        	{
        		$debug_last_tab['runtime'] = 1;
        		prepare_reference_container(null, 'loading'); // Loading-Placeholder einblenden
        	}

        	runtime(); // startet die Runtime
        });


        $('#login input[type=submit]').on('click', function() // wenn Anmelden angeklickt wird
        {
            trigger_login(); // Login-Vorgang auslösen
        });


        $('#header #caption img').on('click', function() // wenn das Icon angeklickt wird
        {
            toggle_debug_visibility(); // Funktion zum Ein-/Ausblenden des Debug-Logs
        });


        $('#debug #selection p').on('click', function() // wenn einer der Tabs im Debug-Log angeklickt wird
        {
            debug_switch_log(this); // Ansicht des Debug-Logs umschalten
        });


        $('#header #navi img').on('click', function() // wenn das Menü-Icon angeklickt wird
        {
            if($('#menu').is(':visible')) // wenn das Menü bereits sichtbar ist
            {
                $('#menu').fadeOut(500); // wieder ausblenden
            }
            else
            {
                $('#menu').fadeIn(500); // Menü einblenden
            }

            $hide_menu_timer = setTimeout(function() // Timer bis zum automatischen Ausblenden starten
            {
                $('#menu').fadeOut(500); // Menü nach Ablauf des Timers wieder ausblenden
            }, $HIDE_MENU_TIME);
        });


        $('#menu img').on('click', function() // wenn auf einen Menüpunkt geklickt wird
        {
            clearTimeout($hide_menu_timer); // Timer für das Ausblenden abbrechen, da dies jetzt getan wird (verhindert ungewolltes Ausblenden bei erneutem öffnen des Menüs)
            $('#menu').fadeOut(200); // Menü wieder ausblenden

            switch ($(this).index()) // je nach angeklicktem Button eine zugehörige Aktion ausführen
            {
                case 1:
                    overlay('Einstellungen', 'settings.php', false, function($data){handle_errors('overlay', $data)}); // Overlay für Settings erzeugen
                    break;

                case 2:
                    overlay('Kontakt', 'contact.php', true, function($data){handle_errors('overlay', $data)}); // Overlay für Kontaktdaten erzeugen
                    break;

                case 3:
                    overlay('Changelog', 'changelog.php', true, function($data){handle_errors('overlay', $data)}); // Overlay für Changelog erzeugen
                    break;

                case 4:
                    overlay('Abmelden', 'logout.php', false, function($data){handle_errors('overlay', $data)}); // Overlay für Changelog erzeugen
                    break;
            }
        });


        $('#overlay #headline #close').on('click', function() // wenn der Close-Button des Overlays angeklickt wird
        {
            overlay_close(); // Overlay schließen
        });
    };